import { Observable } from 'rxjs/Observable';
import {
  AddKidReviewQuery,
  ExamineLast1Query,
  ExamineLastQuery,
  getDoctorByIdQuery,
  getReviewById0Query,
  getReviewByIdQuery,
  kidReviewByMedrec0Query,
  kidReviewByMedrecQuery,
  PatientByMedrecQuery,
  QueryExamineLast,
  QueryExamineLast1,
  QueryGetDoctorById,
  QueryKidReviewByMedrec,
  QueryKidReviewByMedrec0,
  QueryPatientByMedrec,
  QueryReviewById,
  QueryReviewById0,
  QueueFlagPendingQuery,
  QueueFlagQuery,
  setExaminationFlagQuery,
  setQueueFlagQuery,
  setReviewStatusQuery,
  updateKidReviewQuery,
  updateReviewQuery,
  QueueQuerySub,
  QueueAllQuery,
  QueueExchangeQuery,
  ExamineExchangeQuery,
  EmployeeByNikQuery,
  QueryEmployeeByNik,
  MidwifeReviewQuery,
  QueryMWReviewById0,
  getMWReviewById0Query,
  updateMWReviewQuery,
} from './../gqlquery/gqlquery';
import {
  addExaminationQuery,
  AdultReviewQuery,
  DeleteQueueQuery,
  ExamineByIdQuery,
  GroupByDoctorQuery,
  GroupByPoliQuery,
  QueryDoctorById,
  QueryExamineById,
  QueryPoliById,
  QueryQueue,
  QueryQueueById,
  QueueByIdQuery,
  QueueQuery,
  setQueueStatusQuery,
} from '../gqlquery/gqlquery';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AntrianService {

  private API = "api/sms/send/";


  constructor(private apollo: Apollo, private http: Http) { }

  public sendSMS(nohp, message) {
    if (nohp === null || message === null) {
      return Observable.throw('Pelase Insert Credentials');
    } else {
      return this.http.get(this.API + nohp + "&" + message)
        .map(res => res.json())
        .subscribe(
        data => console.log(data),
        error => console.log(error)
        );
    }

  }


  public groupByPoli() {
    return this.apollo.watchQuery<QueryPoliById>({
      query: GroupByPoliQuery
    });
  }

  public groupByDoctor(poli: string) {
    return this.apollo.watchQuery<QueryDoctorById>({
      query: GroupByDoctorQuery,
      variables: {
        poli: poli
      }
    });
  }

  public getDoctorById(nodok: string) {
    return this.apollo.watchQuery<QueryGetDoctorById>({
      query: getDoctorByIdQuery,
      variables: {
        nodok: nodok
      }
    });
  }

  public loadQueue(poli: string, dokter: string) {
    return this.apollo.watchQuery<QueueAllQuery>({
      query: QueueAllQuery,
      variables: {
        poli: poli,
        nama_dok: dokter
      },
      fetchPolicy: 'network-only'
    });
  }

  public loadQueueSubscription(nodok: string, flag: number, poli: string, dokter: string) {
    return this.apollo.subscribe({
      query: QueueExchangeQuery,
      variables: {
        nodok: nodok,
        flag: flag
      }
    });
  }

  public loadExamineSubscription(nodok: string, flag: number, poli: string, dokter: string) {
    return this.apollo.subscribe({
      query: ExamineExchangeQuery,
      variables: {
        nodok: nodok,
        flag: flag
      }
    });
  }

  public loadQueuePatient(poli: string, dokter: string, flag: number) {
    return this.apollo.watchQuery<QueryQueue>({
      query: QueueQuery,
      variables: {
        poli: poli,
        nama_dok: dokter,
        flag: flag
      }
    });
  }

  public loadPatientExamination(nodok: string) {
    return this.apollo.watchQuery<QueryExamineLast>({
      query: ExamineLastQuery,
      variables: {
        nodok: nodok
      },
      fetchPolicy: 'network-only'
    });
  }

  public loadPatientExamination1(nodok: string) {
    return this.apollo.watchQuery<QueryExamineLast1>({
      query: ExamineLast1Query,
      variables: {
        nodok: nodok
      },
      fetchPolicy: 'network-only'
    });
  }

  public getPatientById(no_medrec: string) {
    return this.apollo.watchQuery<QueryQueueById>({
      query: QueueByIdQuery,
      variables: {
        no_medrec: no_medrec
      },
    });
  }

  public getDataPatientByMedrec(no_medrec: string) {
    return this.apollo.watchQuery<QueryPatientByMedrec>({
      query: PatientByMedrecQuery,
      variables: {
        no_medrec: no_medrec
      },
    });
  }

  public getPatientByIdReview(no_medrec: string) {
    return this.apollo.watchQuery<QueryQueueById>({
      query: QueueByIdQuery,
      variables: {
        no_medrec: no_medrec
      },

    })
  }

  public getReviewById(no_medrec: string) {
    return this.apollo.watchQuery<QueryReviewById>({
      query: getReviewByIdQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getReviewByMedrec(no_medrec: string) {
    return this.apollo.watchQuery<QueryReviewById>({
      query: getReviewByIdQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getReviewByMedrecZero(no_medrec: string) {
    return this.apollo.watchQuery<QueryReviewById0>({
      query: getReviewById0Query,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getKidReviewByMedrec(no_medrec: string) {
    return this.apollo.watchQuery<QueryKidReviewByMedrec>({
      query: kidReviewByMedrecQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getKidReviewByMedrecZero(no_medrec: string) {
    return this.apollo.watchQuery<QueryKidReviewByMedrec0>({
      query: kidReviewByMedrec0Query,
      variables: {
        no_medrec: no_medrec
      }
    });
  }


  public setFlagPatient(no_medrec: string, flag: number, poli: string, dokter: string) {
    return this.apollo.mutate({
      mutation: setQueueFlagQuery,
      variables: {
        no_medrec: no_medrec,
        flag: flag
      },
      refetchQueries: [{
        query: QueueAllQuery,
        variables: {
          poli: poli,
          nama_dok: dokter
        }
      }]
    });
  }

  public setStatusPatient(no_medrec: string, status_pasien: string, flag: number, poli: string, dokter: string) {
    return this.apollo.mutate({
      mutation: setQueueStatusQuery,
      variables: {
        no_medrec: no_medrec,
        status_pasien: status_pasien,
        flag: flag
      },
      refetchQueries: [{
        query: QueueAllQuery,
        variables: {
          poli: poli,
          nama_dok: dokter
        }
      }]
    });
  }

  public deletePatient(no_medrec: string) {
    return this.apollo.mutate({
      mutation: DeleteQueueQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public addDataPatient(dataarray: any, kunjungan: string, specialis: string, nodok: string) {
    var salt = Math.floor(Math.random() * 10000);
    var no_periksa = salt + dataarray.no_medrec;

    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "-"
      + (currentdate.getMonth() + 1) + "-"
      + currentdate.getDate() + " "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes() + ":"
      + currentdate.getSeconds();
    console.log(datetime + " 000000000000 " + kunjungan);

    return this.apollo.mutate({
      mutation: addExaminationQuery,
      variables: {
        no_periksa: no_periksa,
        kode_unit: dataarray.kode_unit,
        poli: dataarray.poli,
        tgl: datetime,
        nama_dok: dataarray.nama_dok,
        specialis: specialis,
        nodok: dataarray.nodok,
        no_registrasi: dataarray.no_registrasi,
        no_medrec: dataarray.no_medrec,
        nama_pasien: dataarray.nama_pasien,
        diagnosa: '',
        no_antrian: dataarray.no_antrian,
        kasus: '',
        jaminan: dataarray.jaminan,
        kunjungan: kunjungan,
        rujuk: '',
        umur: dataarray.umur,
        tgl_lahir: dataarray.tgl_lahir,
        kelamin: dataarray.kelamin,
        status_pasien: dataarray.status_pasien,
        kaji: dataarray.kaji,
        flag: dataarray.flag
      },
      refetchQueries: [{
        query: ExamineLastQuery,
        variables: {
          nodok: nodok
        }
      }]
    });
  }

  public getDataPatientById(no_medrec: string) {
    return this.apollo.watchQuery<QueryExamineById>({
      query: ExamineByIdQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public saveAdultReview(dataarray: any) {
    let td = dataarray.td1 + "/" + dataarray.td2;
    let nadi = dataarray.nadi1 + "/" + dataarray.nadi2;
    let pernafasan = dataarray.pernafasan1 + "/" + dataarray.pernafasan2;
    return this.apollo.mutate({
      mutation: AdultReviewQuery,
      variables: {
        no_kaji: dataarray.no_kaji,
        no_medrec: dataarray.no_medrec,
        keluhan: dataarray.keluhan,
        resikoA1: dataarray.resikoA1,
        resikoA2: dataarray.resikoA2,
        resikoB1: dataarray.resikoB1,
        resikoHasil1: dataarray.resikoHasil1,
        resikoHasil2: dataarray.resikoHasil2,
        resikoHasil3: dataarray.resikoHasil3,
        resikoTindakan1action: dataarray.resikoTindakan1action,
        resikoTindakan2action: dataarray.resikoTindakan2action,
        resikoTindakan3action: dataarray.resikoTindakan3action,
        resikoTindakan1: dataarray.resikoTindakan1,
        resikoTindakan2: dataarray.resikoTindakan2,
        resikoTindakan3: dataarray.resikoTindakan3,
        td: td,
        td1: dataarray.td1,
        td2: dataarray.td2,
        nadi: nadi,
        nadi1: dataarray.nadi1,
        nadi2: dataarray.nadi2,
        pernafasan: pernafasan,
        pernafasan1: dataarray.pernafasan1,
        pernafasan2: dataarray.pernafasan2,
        suhu: dataarray.suhu,
        tb: dataarray.tb,
        bb: dataarray.bb,
        nyeri: dataarray.nyeri,
        resiko_jatuh_a: dataarray.resiko_jatuh_a,
        resiko_jatuh_b: dataarray.resiko_jatuh_b,
        gizi1: dataarray.gizi1,
        gizi2: dataarray.gizi2,
        gizi3: dataarray.gizi3,
        gizi3_lainnya: dataarray.gizi3_lainnya,
        status_fungsional: dataarray.status_fungsional,
        status_fungsional_ket: dataarray.status_fungsional_ket,
        tt_perawat: dataarray.tt_perawat,
        flagKaji: 0
      },
      refetchQueries: [{
        query: getReviewById0Query,
        variables: {
          no_medrec: dataarray.no_medrec
        }
      }]
    });
  }

  public updateAdultReview(dataarray: any) {
    let td = dataarray.td1 + "/" + dataarray.td2;
    let nadi = dataarray.nadi1 + "/" + dataarray.nadi2;
    let pernafasan = dataarray.pernafasan1 + "/" + dataarray.pernafasan2;
    return this.apollo.mutate({
      mutation: updateReviewQuery,
      variables: {
        no_kaji: dataarray.no_kaji,
        no_medrec: dataarray.no_medrec,
        keluhan: dataarray.keluhan,
        resikoA1: dataarray.resikoA1,
        resikoA2: dataarray.resikoA2,
        resikoB1: dataarray.resikoB1,
        resikoHasil1: dataarray.resikoHasil1,
        resikoHasil2: dataarray.resikoHasil2,
        resikoHasil3: dataarray.resikoHasil3,
        resikoTindakan1action: dataarray.resikoTindakan1action,
        resikoTindakan2action: dataarray.resikoTindakan2action,
        resikoTindakan3action: dataarray.resikoTindakan3action,
        resikoTindakan1: dataarray.resikoTindakan1,
        resikoTindakan2: dataarray.resikoTindakan2,
        resikoTindakan3: dataarray.resikoTindakan3,
        td: td,
        td1: dataarray.td1,
        td2: dataarray.td2,
        nadi: nadi,
        nadi1: dataarray.nadi1,
        nadi2: dataarray.nadi2,
        pernafasan: pernafasan,
        pernafasan1: dataarray.pernafasan1,
        pernafasan2: dataarray.pernafasan2,
        suhu: dataarray.suhu,
        tb: dataarray.tb,
        bb: dataarray.bb,
        nyeri: dataarray.nyeri,
        resiko_jatuh_a: dataarray.resiko_jatuh_a,
        resiko_jatuh_b: dataarray.resiko_jatuh_b,
        gizi1: dataarray.gizi1,
        gizi2: dataarray.gizi2,
        gizi3: dataarray.gizi3,
        gizi3_lainnya: dataarray.gizi3_lainnya,
        status_fungsional: dataarray.status_fungsional,
        status_fungsional_ket: dataarray.status_fungsional_ket,
        tt_perawat: dataarray.tt_perawat
      },
      refetchQueries: [{
        query: getReviewById0Query,
        variables: {
          no_medrec: dataarray.no_medrec
        }
      }]
    });
  }

  public setStatusReview(no_medrec: string, kaji: string, poli: string, dokter: string) {
    return this.apollo.mutate({
      mutation: setReviewStatusQuery,
      variables: {
        no_medrec: no_medrec,
        kaji: kaji
      },
      refetchQueries: [{
        query: QueueAllQuery,
        variables: {
          poli: poli,
          nama_dok: dokter
        }
      }]
    });
  }

  public saveKidReview(dataarray: any) {
    let td = dataarray.td1 + "/" + dataarray.td2;
    let nadi = dataarray.nadi1 + "/" + dataarray.nadi2;
    let pernafasan = dataarray.pernafasan1 + "/" + dataarray.pernafasan2;
    return this.apollo.mutate({
      mutation: AddKidReviewQuery,
      variables: {
        no_kaji: dataarray.no_kaji,
        no_medrec: dataarray.no_medrec,
        keluhan: dataarray.keluhan,
        td: td,
        td1: dataarray.td1,
        td2: dataarray.td2,
        nadi: nadi,
        nadi1: dataarray.nadi1,
        nadi2: dataarray.nadi2,
        pernafasan: pernafasan,
        pernafasan1: dataarray.pernafasan2,
        pernafasan2: dataarray.pernafasan1,
        suhu: dataarray.suhu,
        tb: dataarray.tb,
        bb: dataarray.bb,
        riwayatAlergiObat: dataarray.riwayatAlergiObatKet,
        riwayatAlergiObatKet: dataarray.riwayatAlergiObatKet,
        riwayatAlergiMakanan: dataarray.riwayatAlergiMakanan,
        riwayatAlergiMakananKet: dataarray.riwayatAlergiMakananKet,
        riwayatAlergiPantangan: dataarray.riwayatAlergiPantangan,
        nilaiIMT: dataarray.nilaiIMT,
        keluhanNyeri: dataarray.keluhanNyeri,
        nips: dataarray.nips,
        nipsEkspresiWajah: dataarray.nipsEkspresiWajah,
        nipsEkspresiWajahHasil: dataarray.nipsEkspresiWajahHasil,
        nipsMenangis: dataarray.nipsMenangis,
        nipsMenangisHasil: dataarray.nipsMenangisHasil,
        nipsPernafasan: dataarray.nipsPernafasan,
        nipsPernafasanHasil: dataarray.nipsPernafasanHasil,
        nipsPergelanganTangan: dataarray.nipsPergelanganTangan,
        nipsPergelanganTanganHasil: dataarray.nipsPergelanganTanganHasil,
        nipsKaki: dataarray.nipsKaki,
        nipsKakiHasil: dataarray.nipsKakiHasil,
        nipsKesadaran: dataarray.nipsKesadaran,
        nipsKesadaranHasil: dataarray.nipsKesadaranHasil,
        flacc: dataarray.flacc,
        flaccWajah: dataarray.flaccWajah,
        flaccWajahHasil: dataarray.flaccWajahHasil,
        flaccKaki: dataarray.flaccKaki,
        flaccKakiHasil: dataarray.flaccKakiHasil,
        flaccAktifitas: dataarray.flaccAktifitas,
        flaccAktifitasHasil: dataarray.flaccAktifitasHasil,
        flaccMenangis: dataarray.flaccMenangis,
        flaccMenangisHasil: dataarray.flaccMenangisHasil,
        flaccBersuara: dataarray.flaccBersuara,
        flaccBersuaraHasil: dataarray.flaccBersuaraHasil,
        faces: dataarray.faces,
        nyeri: dataarray.nyeri,
        skalaNyeri: dataarray.skalaNyeri,
        penyebabNyeri: dataarray.penyebabNyeri,
        mulaiNyeri: dataarray.mulaiNyeri,
        tipeNyeri: dataarray.tipeNyeri,
        karakteristikNyeri: dataarray.karakteristikNyeri,
        karakteristikNyeriLainnya: dataarray.karakteristikNyeriLainnya,
        nyeriMenyebar: dataarray.nyeriMenyebar,
        nyeriMenyebarHasil: dataarray.nyeriMenyebarHasil,
        frekuensiNyeri: dataarray.frekuensiNyeri,
        pengobatanNyeri: dataarray.pengobatanNyeri,
        pengobatanNyeriAkibat: dataarray.pengobatanNyeriAkibat,
        pengobatanNyeriAkibatLainnya: dataarray.pengobatanNyeriAkibatLainnya,
        kemampuanAktifitas: dataarray.kemampuanAktifitas,
        statusPsikologis: dataarray.statusPsikologis,
        statusPsikologisLainnya: dataarray.statusPsikologisLainnya,
        statusMental: dataarray.statusMental,
        statusMentalDisorientasi: dataarray.statusMentalDisorientasi,
        resikoA1: dataarray.resikoA1,
        resikoA2: dataarray.resikoA2,
        resikoB1: dataarray.resikoB1,
        resiko1Ket: dataarray.resiko1Ket,
        resiko2Ket: dataarray.resiko2Ket,
        resiko3Ket: dataarray.resiko3Ket,
        gizi1: dataarray.gizi1,
        gizi2: dataarray.gizi2,
        gizi3: dataarray.gizi3,
        gizi4: dataarray.gizi4,
        flagKaji: "0"
      }
    });
  }

  public updateKidReview(dataarray: any) {
    let td = dataarray.td1 + "/" + dataarray.td2;
    let nadi = dataarray.nadi1 + "/" + dataarray.nadi2;
    let pernafasan = dataarray.pernafasan1 + "/" + dataarray.pernafasan2;
    return this.apollo.mutate({
      mutation: updateKidReviewQuery,
      variables: {
        no_kaji: dataarray.no_kaji,
        no_medrec: dataarray.no_medrec,
        keluhan: dataarray.keluhan,
        td: td,
        td1: dataarray.td1,
        td2: dataarray.td2,
        nadi: nadi,
        nadi1: dataarray.nadi1,
        nadi2: dataarray.nadi2,
        pernafasan: pernafasan,
        pernafasan1: dataarray.pernafasan2,
        pernafasan2: dataarray.pernafasan1,
        suhu: dataarray.suhu,
        tb: dataarray.tb,
        bb: dataarray.bb,
        riwayatAlergiObat: dataarray.riwayatAlergiObatKet,
        riwayatAlergiObatKet: dataarray.riwayatAlergiObatKet,
        riwayatAlergiMakanan: dataarray.riwayatAlergiMakanan,
        riwayatAlergiMakananKet: dataarray.riwayatAlergiMakananKet,
        riwayatAlergiPantangan: dataarray.riwayatAlergiPantangan,
        nilaiIMT: dataarray.nilaiIMT,
        keluhanNyeri: dataarray.keluhanNyeri,
        nips: dataarray.nips,
        nipsEkspresiWajah: dataarray.nipsEkspresiWajah,
        nipsEkspresiWajahHasil: dataarray.nipsEkspresiWajahHasil,
        nipsMenangis: dataarray.nipsMenangis,
        nipsMenangisHasil: dataarray.nipsMenangisHasil,
        nipsPernafasan: dataarray.nipsPernafasan,
        nipsPernafasanHasil: dataarray.nipsPernafasanHasil,
        nipsPergelanganTangan: dataarray.nipsPergelanganTangan,
        nipsPergelanganTanganHasil: dataarray.nipsPergelanganTanganHasil,
        nipsKaki: dataarray.nipsKaki,
        nipsKakiHasil: dataarray.nipsKakiHasil,
        nipsKesadaran: dataarray.nipsKesadaran,
        nipsKesadaranHasil: dataarray.nipsKesadaranHasil,
        flacc: dataarray.flacc,
        flaccWajah: dataarray.flaccWajah,
        flaccWajahHasil: dataarray.flaccWajahHasil,
        flaccKaki: dataarray.flaccKaki,
        flaccKakiHasil: dataarray.flaccKakiHasil,
        flaccAktifitas: dataarray.flaccAktifitas,
        flaccAktifitasHasil: dataarray.flaccAktifitasHasil,
        flaccMenangis: dataarray.flaccMenangis,
        flaccMenangisHasil: dataarray.flaccMenangisHasil,
        flaccBersuara: dataarray.flaccBersuara,
        flaccBersuaraHasil: dataarray.flaccBersuaraHasil,
        faces: dataarray.faces,
        nyeri: dataarray.nyeri,
        skalaNyeri: dataarray.skalaNyeri,
        penyebabNyeri: dataarray.penyebabNyeri,
        mulaiNyeri: dataarray.mulaiNyeri,
        tipeNyeri: dataarray.tipeNyeri,
        karakteristikNyeri: dataarray.karakteristikNyeri,
        karakteristikNyeriLainnya: dataarray.karakteristikNyeriLainnya,
        nyeriMenyebar: dataarray.nyeriMenyebar,
        nyeriMenyebarHasil: dataarray.nyeriMenyebarHasil,
        frekuensiNyeri: dataarray.frekuensiNyeri,
        pengobatanNyeri: dataarray.pengobatanNyeri,
        pengobatanNyeriAkibat: dataarray.pengobatanNyeriAkibat,
        pengobatanNyeriAkibatLainnya: dataarray.pengobatanNyeriAkibatLainnya,
        kemampuanAktifitas: dataarray.kemampuanAktifitas,
        statusPsikologis: dataarray.statusPsikologis,
        statusPsikologisLainnya: dataarray.statusPsikologisLainnya,
        statusMental: dataarray.statusMental,
        statusMentalDisorientasi: dataarray.statusMentalDisorientasi,
        resikoA1: dataarray.resikoA1,
        resikoA2: dataarray.resikoA2,
        resikoB1: dataarray.resikoB1,
        resiko1Ket: dataarray.resiko1Ket,
        resiko2Ket: dataarray.resiko2Ket,
        resiko3Ket: dataarray.resiko3Ket,
        gizi1: dataarray.gizi1,
        gizi2: dataarray.gizi2,
        gizi3: dataarray.gizi3,
        gizi4: dataarray.gizi4,
      }
    });
  }

  public setFlagExamination(no_medrec: string, flag: number, nodok: string) {
    return this.apollo.mutate({
      mutation: setExaminationFlagQuery,
      variables: {
        no_medrec: no_medrec,
        flag: flag
      },
      refetchQueries: [{
        query: ExamineLast1Query,
        variables: {
          nodok: nodok
        }
      }]
    })
  }

  public getEmployeeByNik(nik: string) {
    return this.apollo.watchQuery<QueryEmployeeByNik>({
      query: EmployeeByNikQuery,
      variables: {
        nik: nik
      },
    });
  }

  public getMWReviewByMedrecZero(no_medrec: string) {
    return this.apollo.watchQuery<QueryMWReviewById0>({
      query: getMWReviewById0Query,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public saveMidwifeReview(dataarray: any) {
    return this.apollo.mutate({
      mutation: MidwifeReviewQuery,
      variables: {
        no_kaji: dataarray.no_kaji,
        no_medrec: dataarray.no_medrec,
        tanggal: dataarray.tanggal,
        jam: dataarray.jam,
        sumberData: dataarray.sumberData,
        rujukan: dataarray.rujukan,
        rujukan_ket: dataarray.rujukan_ket,
        diagnosa_rujukan: dataarray.diagnosa_rujukan,
        keluhan: dataarray.keluhan,
        riwayatPenyakit: dataarray.riwayatPenyakit,
        riwayatPenyakit_ket: dataarray.riwayatPenyakit_ket,
        dirawat: dataarray.dirawat,
        dirawat_diagnosa: dataarray.dirawat_diagnosa,
        dirawat_kapan: dataarray.dirawat_kapan,
        dirawat_di: dataarray.dirawat_di,
        dioprasi: dataarray.dioprasi,
        dioprasi_jenis: dataarray.dioprasi_jenis,
        dioprasi_kapan: dataarray.dioprasi_kapan,
        pengobatan: dataarray.pengobatan,
        pengobatan_obat: dataarray.pengobatan_obat,
        penyakitKeluarga: dataarray.penyakitKeluarga,
        ketergantunganZat: dataarray.ketergantunganZat,
        pekerjaan: dataarray.pekerjaan,
        pekerjaan_ket: dataarray.pekerjaan_ket,
        alergi: dataarray.alergi,
        alergi_obat: dataarray.alergi_obat,
        alergi_obat_reaksi: dataarray.alergi_obat_reaksi,
        alergi_makanan: dataarray.alergi_makanan,
        alergi_makanan_reaksi: dataarray.alergi_makanan_reaksi,
        alergi_lainnya: dataarray.alergi_lainnya,
        alergi_lainnya_reaksi: dataarray.alergi_lainnya_reaksi,
        kontrasepsi: dataarray.kontrasepsi,
        kontrasepsi_jenis: dataarray.kontrasepsi_jenis,
        kontrasepsi_lama: dataarray.kontrasepsi_lama,
        kontrasepsi_keluhan: dataarray.kontrasepsi_keluhan,
        pernikahan: dataarray.pernikahan,
        menikah_kali: dataarray.menikah_kali,
        menikah_umur: dataarray.menikah_umur,
        menikah1: dataarray.menikah1,
        menikah2: dataarray.menikah2,
        menarche: dataarray.menarche,
        siklus: dataarray.siklus,
        siklus_teratur: dataarray.siklus_teratur,
        siklus_teratur_ket: dataarray.siklus_teratur_ket,
        haid_volume: dataarray.haid_volume,
        haid_keluhan: dataarray.haid_keluhan,
        haid_hpht: dataarray.haid_hpht,
        haid_taksiran: dataarray.haid_taksiran,
        ginekologi: dataarray.ginekologi,
        riwayat_kehamilan: dataarray.riwayat_kehamilan,
        status_ekosos: dataarray.status_ekosos,
        bicara: dataarray.bicara,
        bicara_ket: dataarray.bicara_ket,
        penerjemah: dataarray.penerjemah,
        penerjemah_ket: dataarray.penerjemah_ket,
        isyarat: dataarray.isyarat,
        hambatan: dataarray.hambatan,
        hambatan_ket: dataarray.hambatan_ket,
        resiko_a: dataarray.resiko_a,
        resiko_b: dataarray.resiko_b,
        resiko_hasil: dataarray.resiko_hasil,
        beritahuDokter: dataarray.beritahuDokter,
        beritahuDokter_pukul: dataarray.beritahuDokter_pukul,
        aktivitas: dataarray.aktivitas,
        aktivitas_ket: dataarray.aktivitas_ket,
        alatBantu: dataarray.alatBantu,
        kronis_lokasi: dataarray.kronis_lokasi,
        kronis_frek: dataarray.kronis_frek,
        kronis_durasi: dataarray.kronis_durasi,
        akut_lokasi: dataarray.akut_lokasi,
        akut_frek: dataarray.akut_frek,
        akut_durasi: dataarray.akut_durasi,
        skor_nyeri: dataarray.skor_nyeri,
        nyeriHilang: dataarray.nyeriHilang,
        flagKaji: 0
      },
      refetchQueries: [{
        query: getMWReviewById0Query,
        variables: {
          no_medrec: dataarray.no_medrec
        }
      }]
    });
  }

  public updateMidwifeReview(dataarray: any) {
    return this.apollo.mutate({
      mutation: updateMWReviewQuery,
      variables: {
        no_kaji: dataarray.no_kaji,
        no_medrec: dataarray.no_medrec,
        tanggal: dataarray.tanggal,
        jam: dataarray.jam,
        sumberData: dataarray.sumberData,
        rujukan: dataarray.rujukan,
        rujukan_ket: dataarray.rujukan_ket,
        diagnosa_rujukan: dataarray.diagnosa_rujukan,
        keluhan: dataarray.keluhan,
        riwayatPenyakit: dataarray.riwayatPenyakit,
        riwayatPenyakit_ket: dataarray.riwayatPenyakit_ket,
        dirawat: dataarray.dirawat,
        dirawat_diagnosa: dataarray.dirawat_diagnosa,
        dirawat_kapan: dataarray.dirawat_kapan,
        dirawat_di: dataarray.dirawat_di,
        dioprasi: dataarray.dioprasi,
        dioprasi_jenis: dataarray.dioprasi_jenis,
        dioprasi_kapan: dataarray.dioprasi_kapan,
        pengobatan: dataarray.pengobatan,
        pengobatan_obat: dataarray.pengobatan_obat,
        penyakitKeluarga: dataarray.penyakitKeluarga,
        ketergantunganZat: dataarray.ketergantunganZat,
        pekerjaan: dataarray.pekerjaan,
        pekerjaan_ket: dataarray.pekerjaan_ket,
        alergi: dataarray.alergi,
        alergi_obat: dataarray.alergi_obat,
        alergi_obat_reaksi: dataarray.alergi_obat_reaksi,
        alergi_makanan: dataarray.alergi_makanan,
        alergi_makanan_reaksi: dataarray.alergi_makanan_reaksi,
        alergi_lainnya: dataarray.alergi_lainnya,
        alergi_lainnya_reaksi: dataarray.alergi_lainnya_reaksi,
        kontrasepsi: dataarray.kontrasepsi,
        kontrasepsi_jenis: dataarray.kontrasepsi_jenis,
        kontrasepsi_lama: dataarray.kontrasepsi_lama,
        kontrasepsi_keluhan: dataarray.kontrasepsi_keluhan,
        pernikahan: dataarray.pernikahan,
        menikah_kali: dataarray.menikah_kali,
        menikah_umur: dataarray.menikah_umur,
        menikah1: dataarray.menikah1,
        menikah2: dataarray.menikah2,
        menarche: dataarray.menarche,
        siklus: dataarray.siklus,
        siklus_teratur: dataarray.siklus_teratur,
        siklus_teratur_ket: dataarray.siklus_teratur_ket,
        haid_volume: dataarray.haid_volume,
        haid_keluhan: dataarray.haid_keluhan,
        haid_hpht: dataarray.haid_hpht,
        haid_taksiran: dataarray.haid_taksiran,
        ginekologi: dataarray.ginekologi,
        riwayat_kehamilan: dataarray.riwayat_kehamilan,
        status_ekosos: dataarray.status_ekosos,
        bicara: dataarray.bicara,
        bicara_ket: dataarray.bicara_ket,
        penerjemah: dataarray.penerjemah,
        penerjemah_ket: dataarray.penerjemah_ket,
        isyarat: dataarray.isyarat,
        hambatan: dataarray.hambatan,
        hambatan_ket: dataarray.hambatan_ket,
        resiko_a: dataarray.resiko_a,
        resiko_b: dataarray.resiko_b,
        resiko_hasil: dataarray.resiko_hasil,
        beritahuDokter: dataarray.beritahuDokter,
        beritahuDokter_pukul: dataarray.beritahuDokter_pukul,
        aktivitas: dataarray.aktivitas,
        aktivitas_ket: dataarray.aktivitas_ket,
        alatBantu: dataarray.alatBantu,
        kronis_lokasi: dataarray.kronis_lokasi,
        kronis_frek: dataarray.kronis_frek,
        kronis_durasi: dataarray.kronis_durasi,
        akut_lokasi: dataarray.akut_lokasi,
        akut_frek: dataarray.akut_frek,
        akut_durasi: dataarray.akut_durasi,
        skor_nyeri: dataarray.skor_nyeri,
        nyeriHilang: dataarray.nyeriHilang
      },
      refetchQueries: [{
        query: getMWReviewById0Query,
        variables: {
          no_medrec: dataarray.no_medrec
        }
      }]
    });
  }


}
