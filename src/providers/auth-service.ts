import { AlertController, Loading, LoadingController } from 'ionic-angular';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import { Storage } from "@ionic/storage";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class AuthService {
  loading: Loading;

  private API = "auth/authentication";
  private APIDATA = "auth/data";

  contentHeader = new Headers({ "Content-Type": "application/json" });
  error: string;
  jwtHelper = new JwtHelper();
  user: string;

  constructor(private http: Http, private storage: Storage, private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    storage.ready().then(() => {
      storage.get('email').then(email => {
        this.user = JSON.parse(email);
      }).catch(console.log);
    });
  }

  login(credentials) {
    this.showLoading();
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw('Pelase Insert Credentials');
    } else {
      return this.http.post(this.API, JSON.stringify(credentials), { headers: this.contentHeader }).map(res => res.json());
    }
  }

  forgetPass(credentials) {
    this.showLoading();
    if (credentials.email === null) {
      return Observable.throw('Pelase Insert Credentials');
    } else {
      return this.http.get(this.API + "/forget/" + credentials.email, { headers: this.contentHeader })
        .map(res => res.json());
    }

  }

  getDataNursery(credentials): Promise<any> {
    if (credentials.email === null || credentials.password === null) {
      throw ('Pelase Insert Credentials');
    } else {
      return this.http.get(this.APIDATA + "/" + credentials.email, { headers: this.contentHeader }).map(res => res.json()).toPromise();
    }
  }

  saveToken(data, credentials): Observable<any> {
    this.user = this.jwtHelper.decodeToken(data.token).email;
    return Observable.fromPromise(
      this.storage.set('token', data.token)
        .then(() => {
          return this.storage.get('token');})
          .then((data) => {
          if (data) {
            return this.getDataNursery(credentials);
          }
          else {
            throw ("No Token");
          }
        }).then((data) => {
          return this.dataNurse(data);
        })
    );
  }

  dataNurse(data): Promise<any> {
    if (data[0].role == "perawat") {
      return Promise.all([
        this.storage.set('role', data[0].role),
        this.storage.set('nik', data[0].nik)
      ]).then(([data]) => { return data })
    }
    throw ("Anda bukan perawat");
  }

  getToken() {
    if (this.saveToken == null) {
      return null;
    } else {
      this.storage.get('token');
    }
  }

  public static authenticated() {
    return tokenNotExpired('/_ionickv/token');
  }

  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw('Pelase Insert Credentials');
    } else {
      return Observable.create(observer => {
        //save to API
        observer.next(true);
        observer.complete();
      });
    }
  }

  public logout() {
    return Observable.create(observer => {
      var removeToken = this.storage.remove('token');
      this.storage.remove('poli');
      this.storage.remove('nama_dokter');
      this.storage.remove('nodok');
      this.storage.remove('role');
      observer.next(removeToken);
      observer.complete();
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Mohon tunggu . . . ',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(title, error) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: error,
      buttons: ['OK']
    });
    alert.present();
  }

}
