import gql from 'graphql-tag';

export interface QueryReviewById {
  reviewByMedrec
}

export interface QueryReviewById0 {
  reviewByMedrec0
}

export interface QueryPoliById {
  groupByPoli
}

export interface QueryDoctorById {
  groupByDoctor
}

export interface QueryQueue {
  queue
}

export interface QueueAllQuery {
  available
  waiting
  pending

}

export interface QueueExchangeQuery {
  queueDataExchange
}

export interface ExamineExchangeQuery {
  examineDataExchange
}

export interface QueryExamineLast {
  examineLast
}

export interface QueryExamineLast1 {
  examineLast1
}

export interface QueryQueueById {
  queueById
}

export interface QueryExamineById {
  examineById
}

export interface QueryKidReviewByMedrec {
  kidReviewByMedrec
}

export interface QueryKidReviewByMedrec0 {
  kidReviewByMedrec0
}


export interface QueryPatientByMedrec {
  patientByNorm
}

export interface QueryGetDoctorById {
  doctorByNodok;
}

export interface QueryEmployeeByNik {
  EmployeeByNikQuery;
}

export interface QueryMWReviewById0 {
  MWreviewByMedrec0
}


export const getDoctorByIdQuery = gql`
query getDoctorByIdQuery($nodok: String!) {
  doctorByNodok(nodok: $nodok) {
    nama_dok,
    specialis
  }
}
`;

export const PatientByMedrecQuery = gql`
query PatientByMedrecQuery($no_medrec:String) {
  patientByNorm(no_medrec:$no_medrec) {
    no_medrec,
    nama,
    telp
  }
}
`;

export const GroupByPoliQuery = gql`
query GroupByPoliQuery {
  groupByPoli {
    poli
  }
}
`;

export const GroupByDoctorQuery = gql`
query GroupByDoctorQuery($poli:String!) {
  groupByDoctor(poli:$poli) {
    nama_dok,
    nodok
  }
}
`;

export const QueueAllQuery = gql`
query QueueAllQuery($poli: String!, $nama_dok: String!) {
  available: queue(poli: $poli, nama_dok: $nama_dok, flag: 0) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
  waiting: queue(poli: $poli, nama_dok: $nama_dok, flag: 1) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
  pending: queue(poli: $poli, nama_dok: $nama_dok, flag: 3) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const QueueExchangeQuery = gql`
subscription QueueExchangeQuery($nodok:String!, $flag:Int!) {
  queueDataExchange(nodok: $nodok, flag: $flag){
    no_medrec,
    nodok,
    flag
  }  
}
`;

export const ExamineExchangeQuery = gql`
subscription ExamineExchangeQuery($nodok:String!, $flag:Int!) {
  examineDataExchange(nodok: $nodok, flag: $flag){
    nodok,
    no_medrec,
    flag
  }  
}
`;

export const QueueQuery = gql`
query QueueQuery($poli: String!, $nama_dok: String!, $flag:Int!) {
  queue(poli: $poli, nama_dok: $nama_dok, flag: $flag) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const QueueQuerySub = gql`
query QueueQuerySub($poli: String!, $nama_dok: String!, $flag:Int!) {
  queue(poli: $poli, nama_dok: $nama_dok, flag: $flag) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const QueueFlagQuery = gql`
query QueueFlagQuery($poli: String!, $nama_dok: String!) {
  queue(poli: $poli, nama_dok: $nama_dok, flag: 1) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const QueueFlagPendingQuery = gql`
query QueueFlagPendingQuery($poli: String!, $nama_dok: String!) {
  queue(poli: $poli, nama_dok: $nama_dok, flag: 3) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const QueueQueries = gql`
query QueueQueries($poli: String!, $nama_dok: String!, $flag:Int!) {
  queue(poli: $poli, nama_dok: $nama_dok, flag: $flag) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  },
  queue(poli: $poli, nama_dok: $nama_dok, flag: 1) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  },
  queue(poli: $poli, nama_dok: $nama_dok, flag: 3) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const ExamineLastQuery = gql`
query ExamineLastQuery($nodok:String!){
  examineLast(nodok:$nodok){
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const ExamineLast1Query = gql`
query ExamineLast1Query($nodok:String!){
  examineLast1(nodok:$nodok){
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const QueueByIdQuery = gql`
query QueueByIdQuery($no_medrec: String!) {
  queueById(no_medrec:$no_medrec) {
    kode_unit,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const setQueueStatusQuery = gql`
mutation setQueueStatusQuery($no_medrec: String!, $status_pasien: String!, $flag: Int!) {
setQueueStatus(no_medrec: $no_medrec, status_pasien: $status_pasien, flag: $flag) {
  kode_unit,
  poli,
  nodok,
  nama_dok,
  no_registrasi,
  no_medrec,
  nama_pasien,
  no_antrian,
  jaminan,
  umur,
  tgl_lahir,
  kelamin,
  status_pasien,
  kaji,
  flag
}
}
`;

export const setQueueFlagQuery = gql`
mutation setQueueFlagQuery($no_medrec: String!, $flag: Int!) {
setQueueFlag(no_medrec: $no_medrec, flag: $flag) {
  kode_unit,
  poli,
  nodok,
  nama_dok,
  no_registrasi,
  no_medrec,
  nama_pasien,
  no_antrian,
  jaminan,
  umur,
  tgl_lahir,
  kelamin,
  status_pasien,
  kaji,
  flag
}
}
`;

export const AdultReviewQuery = gql`
mutation AdultReviewQuery(
  $no_kaji: String,
  $no_medrec: String,
  $keluhan: String,
  $resikoA1: String,
  $resikoA2: String,
  $resikoB1: String,
  $resikoHasil1: String,
  $resikoHasil2: String,
  $resikoHasil3:String,
  $resikoTindakan1action: String,
  $resikoTindakan2action: String,
  $resikoTindakan3action: String,
  $resikoTindakan1: String,
  $resikoTindakan2: String,
  $resikoTindakan3: String,
  $td: String,
  $td1: Int,
  $td2: Int,
  $nadi: String,
  $nadi1: Int,
  $nadi2: Int,
  $pernafasan: String,
  $pernafasan1: Int,
  $pernafasan2: Int,
  $suhu: Float,
  $tb: Int,
  $bb: Int,
  $nyeri: String,
  $resiko_jatuh_a: String,
  $resiko_jatuh_b: String,
  $gizi1: String,
  $gizi2: String,
  $gizi3: String,
  $gizi3_lainnya: String,
  $status_fungsional: String,
  $status_fungsional_ket: String,
  $tt_perawat: String,
  $flagKaji: Int) {

  adultReview(
    no_kaji: $no_kaji,
    no_medrec: $no_medrec,
    keluhan: $keluhan, 
    resikoA1: $resikoA1,
    resikoA2: $resikoA2,
    resikoB1: $resikoB1,
    resikoHasil1: $resikoHasil1,
    resikoHasil2: $resikoHasil2,
    resikoHasil3: $resikoHasil3,
    resikoTindakan1action: $resikoTindakan1action,
    resikoTindakan2action: $resikoTindakan2action,
    resikoTindakan3action: $resikoTindakan3action,
    resikoTindakan1: $resikoTindakan1,
    resikoTindakan2: $resikoTindakan2,
    resikoTindakan3: $resikoTindakan3,
    td: $td,
    td1: $td1,
    td2: $td2,
    nadi: $nadi,
    nadi1: $nadi1,
    nadi2: $nadi2,
    pernafasan: $pernafasan,
    pernafasan1: $pernafasan1,
    pernafasan2: $pernafasan2,
    suhu: $suhu,
    tb: $tb,
    bb: $bb,
    nyeri: $nyeri
    resiko_jatuh_a: $resiko_jatuh_a,
    resiko_jatuh_b: $resiko_jatuh_b,
    gizi1: $gizi1,
    gizi2: $gizi2,
    gizi3: $gizi3,
    gizi3_lainnya: $gizi3_lainnya,
    status_fungsional: $status_fungsional,
    status_fungsional_ket: $status_fungsional_ket,
    tt_perawat: $tt_perawat,
    flagKaji: $flagKaji){

      no_kaji,
      no_medrec,
      keluhan,
      resikoA1,
      resikoA2,
      resikoB1,
      resikoHasil1,
      resikoHasil2,
      resikoHasil3,
      resikoTindakan1action,
      resikoTindakan2action,
      resikoTindakan3action,
      resikoTindakan1,
      resikoTindakan2,
      resikoTindakan3,
      td,
      td1,
      td2,
      nadi,
      nadi1,
      nadi2,
      pernafasan,
      pernafasan1,
      pernafasan2,
      suhu,
      tb,
      bb,
      nyeri,
      resiko_jatuh_a,
      resiko_jatuh_b,
      gizi1,
      gizi2,
      gizi3,
      gizi3_lainnya,
      status_fungsional,
      status_fungsional_ket,
      tt_perawat
    }
}
`;


export const updateReviewQuery = gql`
mutation updateReviewQuery(
  $no_kaji: String,
  $no_medrec: String,
  $keluhan: String,
  $resikoA1: String,
  $resikoA2: String,
  $resikoB1: String,
  $resikoHasil1: String,
  $resikoHasil2: String,
  $resikoHasil3:String,
  $resikoTindakan1action: String,
  $resikoTindakan2action: String,
  $resikoTindakan3action: String,
  $resikoTindakan1: String,
  $resikoTindakan2: String,
  $resikoTindakan3: String,
  $td: String,
  $td1: Int,
  $td2: Int,
  $nadi: String,
  $nadi1: Int,
  $nadi2: Int,
  $pernafasan: String,
  $pernafasan1: Int,
  $pernafasan2: Int,
  $suhu: Float,
  $tb: Int,
  $bb: Int,
  $nyeri: String,
  $resiko_jatuh_a: String,
  $resiko_jatuh_b: String,
  $gizi1: String,
  $gizi2: String,
  $gizi3: String,
  $gizi3_lainnya: String,
  $status_fungsional: String,
  $status_fungsional_ket: String
  $tt_perawat: String) {

  updateReview(
    no_kaji: $no_kaji,
    no_medrec: $no_medrec,
    keluhan: $keluhan, 
    resikoA1: $resikoA1,
    resikoA2: $resikoA2,
    resikoB1: $resikoB1,
    resikoHasil1: $resikoHasil1,
    resikoHasil2: $resikoHasil2,
    resikoHasil3: $resikoHasil3,
    resikoTindakan1action: $resikoTindakan1action,
    resikoTindakan2action: $resikoTindakan2action,
    resikoTindakan3action: $resikoTindakan3action,
    resikoTindakan1: $resikoTindakan1,
    resikoTindakan2: $resikoTindakan2,
    resikoTindakan3: $resikoTindakan3,
    td: $td,
    td1: $td1,
    td2: $td2,
    nadi: $nadi,
    nadi1: $nadi1,
    nadi2: $nadi2,
    pernafasan: $pernafasan,
    pernafasan1: $pernafasan1,
    pernafasan2: $pernafasan2,
    suhu: $suhu,
    tb: $tb,
    bb: $bb,
    nyeri: $nyeri
    resiko_jatuh_a: $resiko_jatuh_a,
    resiko_jatuh_b: $resiko_jatuh_b,
    gizi1: $gizi1,
    gizi2: $gizi2,
    gizi3: $gizi3,
    gizi3_lainnya: $gizi3_lainnya,
    status_fungsional: $status_fungsional,
    status_fungsional_ket: $status_fungsional_ket,
    tt_perawat: $tt_perawat){

      no_kaji,
      no_medrec,
      keluhan,
      resikoA1,
      resikoA2,
      resikoB1,
      resikoHasil1,
      resikoHasil2,
      resikoHasil3,
      resikoTindakan1action,
      resikoTindakan2action,
      resikoTindakan3action,
      resikoTindakan1,
      resikoTindakan2,
      resikoTindakan3,
      td,
      td1,
      td2,
      nadi,
      nadi1,
      nadi2,
      pernafasan,
      pernafasan1,
      pernafasan2,
      suhu,
      tb,
      bb,
      nyeri,
      resiko_jatuh_a,
      resiko_jatuh_b,
      gizi1,
      gizi2,
      gizi3,
      gizi3_lainnya,
      status_fungsional,
      status_fungsional_ket,
      tt_perawat
    }
}
`;


export const DeleteQueueQuery = gql`
mutation DeleteQueueQuery($no_medrec: String!) {
  deleteQueue(no_medrec: $no_medrec) {
    no_medrec
}
}
`;

export const addExaminationQuery = gql`
mutation addExaminationQuery(
  $no_periksa: String!
  $kode_unit: String!
  $poli: String!
  $tgl: String!
  $nodok: String!
  $nama_dok: String
  $specialis: String
  $no_registrasi: String!
  $no_medrec: String!
  $nama_pasien: String
  $diagnosa: String
  $no_antrian: Int
  $kasus: String
  $jaminan: String
  $kunjungan: String
  $rujuk: String
  $umur: Int
  $tgl_lahir: String
  $kelamin: String
  $status_pasien: String
  $kaji: String
  $flag: Int) {   
  addExamination (
    no_periksa: $no_periksa
    kode_unit: $kode_unit
    poli: $poli
    tgl: $tgl
    nodok: $nodok
    nama_dok: $nama_dok
    specialis: $specialis
    no_registrasi: $no_registrasi
    no_medrec: $no_medrec
    nama_pasien: $nama_pasien
    diagnosa: $diagnosa
    no_antrian: $no_antrian
    kasus: $kasus
    jaminan: $jaminan
    kunjungan: $kunjungan
    rujuk: $rujuk
    umur: $umur
    tgl_lahir: $tgl_lahir
    kelamin: $kelamin
    status_pasien: $status_pasien
    kaji: $kaji
    flag: $flag
  ){
    poli,
    nama_pasien,
    no_medrec,
    nodok,
    no_antrian,
    jaminan,
    umur,
    kelamin,
    status_pasien,
    kaji,
    flag,
    kunjungan
  }
}
`;

export const ExamineByIdQuery = gql`
query ExamineByIdQuery($no_medrec:String!) {
  examineById(no_medrec: $no_medrec) {
    kode_unit,
    poli,
    nama_dok,
    specialis,
    nodok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const setReviewStatusQuery = gql`
mutation setReviewStatusQuery($no_medrec: String!, $kaji: String!) {
setReviewStatus(no_medrec: $no_medrec, kaji: $kaji) {
  kode_unit,
  poli,
  nama_dok,
  no_registrasi,
  no_medrec,
  nama_pasien,
  no_antrian,
  jaminan,
  umur,
  tgl_lahir,
  kelamin,
  status_pasien,
  kaji,
  flag
}
}
`;

export const getReviewByIdQuery = gql`
query getReviewByIdQuery($no_medrec: String!) {
  reviewByMedrec(no_medrec: $no_medrec) {
    no_kaji,
    no_medrec,
    keluhan,
    resikoA1,
    resikoA2,
    resikoB1,
    resikoHasil1,
    resikoHasil2,
    resikoHasil3,
    resikoTindakan1action,
    resikoTindakan2action,
    resikoTindakan3action,
    resikoTindakan1,
    resikoTindakan2,
    resikoTindakan3,
    td,
    td1,
    td2,
    nadi,
    nadi1,
    nadi2,
    pernafasan,
    pernafasan1,
    pernafasan2,
    suhu,
    tb,
    bb,
    nyeri,
    resiko_jatuh_a,
    resiko_jatuh_b,
    gizi1,
    gizi2,
    gizi3,
    gizi3_lainnya,
    status_fungsional,
    status_fungsional_ket,
    anamnesa_keluhan,
    anamnesa_riwayat_penyakit,
    fisik_kepala,
    fisik_leher,
    fisik_mulut,
    fisik_paru,
    fisik_genitalia,
    fisik_mata,
    fisik_tht,
    fisik_jantung,
    fisik_abdomen,
    fisik_ekstremitas,
    status_gizi,
    pemeriksaan_gigi,
    pemeriksaan_gigi_ket,
    status_lokalis,
    pemeriksaan_penunjang,
    diagnosis,
    rencana,
    dirujuk,
    dirujuk_lainnya
  }
}
`;

export const getReviewById0Query = gql`
query getReviewById0Query($no_medrec: String!) {
  reviewByMedrec0(no_medrec: $no_medrec) {
    no_kaji,
    no_medrec,
    keluhan,
    resikoA1,
    resikoA2,
    resikoB1,
    resikoHasil1,
    resikoHasil2,
    resikoHasil3,
    resikoTindakan1action,
    resikoTindakan2action,
    resikoTindakan3action,
    resikoTindakan1,
    resikoTindakan2,
    resikoTindakan3,
    td,
    td1,
    td2,
    nadi,
    nadi1,
    nadi2,
    pernafasan,
    pernafasan1,
    pernafasan2,
    suhu,
    tb,
    bb,
    nyeri,
    resiko_jatuh_a,
    resiko_jatuh_b,
    gizi1,
    gizi2,
    gizi3,
    gizi3_lainnya,
    status_fungsional,
    status_fungsional_ket,
    anamnesa_keluhan,
    anamnesa_riwayat_penyakit,
    fisik_kepala,
    fisik_leher,
    fisik_mulut,
    fisik_paru,
    fisik_genitalia,
    fisik_mata,
    fisik_tht,
    fisik_jantung,
    fisik_abdomen,
    fisik_ekstremitas,
    status_gizi,
    pemeriksaan_gigi,
    pemeriksaan_gigi_ket,
    status_lokalis,
    pemeriksaan_penunjang,
    diagnosis,
    rencana,
    dirujuk,
    dirujuk_lainnya
  }
}
`;

export const AddKidReviewQuery = gql`
mutation AddKidReviewQuery(
  $no_kaji: String,
  $no_medrec: String,
  $keluhan: String,
  $td: String,
  $td1: Int,
  $td2: Int,
  $nadi: String,
  $nadi1: Int,
  $nadi2: Int,
  $pernafasan: String,
  $pernafasan1: Int,
  $pernafasan2: Int,
  $suhu: Int,
  $tb: Int,
  $bb: Int,
  $riwayatAlergiObat: String,
  $riwayatAlergiObatKet: String,
  $riwayatAlergiMakanan: String,
  $riwayatAlergiMakananKet: String,
  $riwayatAlergiPantangan: String,
  $nilaiIMT: String,
  $keluhanNyeri: String,
  $nips: String,
  $nipsEkspresiWajah: String,
  $nipsEkspresiWajahHasil: String,
  $nipsMenangis: String,
  $nipsMenangisHasil: String,
  $nipsPernafasan: String,
  $nipsPernafasanHasil: String,
  $nipsPergelanganTangan: String,
  $nipsPergelanganTanganHasil: String,
  $nipsKaki: String,
  $nipsKakiHasil: String,
  $nipsKesadaran: String,
  $nipsKesadaranHasil: String,
  $flacc: String,
  $flaccWajah: String,
  $flaccWajahHasil: String,
  $flaccKaki: String,
  $flaccKakiHasil: String,
  $flaccAktifitas: String,
  $flaccAktifitasHasil: String,
  $flaccMenangis: String,
  $flaccMenangisHasil: String,
  $flaccBersuara: String,
  $flaccBersuaraHasil: String,
  $faces: String,
  $nyeri: String,
  $skalaNyeri: String,
  $penyebabNyeri: String,
  $mulaiNyeri: String,
  $tipeNyeri: String,
  $karakteristikNyeri: String,
  $karakteristikNyeriLainnya: String,
  $nyeriMenyebar: String,
  $nyeriMenyebarHasil: String,
  $frekuensiNyeri: String,
  $pengobatanNyeri: String,
  $pengobatanNyeriAkibat: String,
  $pengobatanNyeriAkibatLainnya: String,
  $kemampuanAktifitas: String,
  $statusPsikologis: String,
  $statusPsikologisLainnya: String,
  $statusMental: String,
  $statusMentalDisorientasi: String,
  $resikoA1: String,
  $resikoA2: String,
  $resikoB1: String,
  $resiko1Ket: String,
  $resiko2Ket: String,
  $resiko3Ket: String,
  $gizi1: String,
  $gizi2: String,
  $gizi3: String,
  $gizi4: String,
  $flagKaji: String) {

  addKidReview(
    no_kaji: $no_kaji,
    no_medrec: $no_medrec,
    keluhan: $keluhan, 
    td: $td,
    td1: $td1,
    td2: $td2,
    nadi: $nadi,
    pernafasan: $pernafasan,
    pernafasan1: $pernafasan1,
    pernafasan2: $pernafasan2,
    suhu: $suhu,
    tb: $tb,
    bb: $bb,
    riwayatAlergiObat: $riwayatAlergiObat,
    riwayatAlergiObatKet: $riwayatAlergiObatKet,
    riwayatAlergiMakanan: $riwayatAlergiMakanan,
    riwayatAlergiMakananKet: $riwayatAlergiMakananKet,
    riwayatAlergiPantangan: $riwayatAlergiPantangan,
    nilaiIMT: $nilaiIMT,
    keluhanNyeri: $keluhanNyeri,
    nips: $nips,
    nipsEkspresiWajah: $nipsEkspresiWajah,
    nipsEkspresiWajahHasil: $nipsEkspresiWajahHasil,
    nipsMenangis: $nipsMenangis,
    nipsMenangisHasil: $nipsMenangisHasil,
    nipsPernafasan: $nipsPernafasan,
    nipsPernafasanHasil: $nipsPernafasanHasil,
    nipsPergelanganTangan: $nipsPergelanganTangan,
    nipsPergelanganTanganHasil: $nipsPergelanganTanganHasil,
    nipsKaki: $nipsKaki,
    nipsKakiHasil: $nipsKakiHasil,
    nipsKesadaran: $nipsKesadaran,
    nipsKesadaranHasil: $nipsKesadaranHasil,
    flacc: $flacc,
    flaccWajah: $flaccWajah,
    flaccWajahHasil: $flaccWajahHasil,
    flaccKaki: $flaccKaki,
    flaccKakiHasil: $flaccKakiHasil,
    flaccAktifitas: $flaccAktifitas,
    flaccAktifitasHasil: $flaccAktifitasHasil,
    flaccMenangis: $flaccMenangis,
    flaccMenangisHasil: $flaccMenangisHasil,
    flaccBersuara: $flaccBersuara,
    flaccBersuaraHasil: $flaccBersuaraHasil,
    faces: $faces,
    nyeri: $nyeri,
    skalaNyeri: $skalaNyeri,
    penyebabNyeri: $penyebabNyeri,
    mulaiNyeri: $mulaiNyeri,
    tipeNyeri: $tipeNyeri,
    karakteristikNyeri: $karakteristikNyeri,
    karakteristikNyeriLainnya: $karakteristikNyeriLainnya,
    nyeriMenyebar: $nyeriMenyebar,
    nyeriMenyebarHasil: $nyeriMenyebarHasil,
    frekuensiNyeri: $frekuensiNyeri,
    pengobatanNyeri: $pengobatanNyeri,
    pengobatanNyeriAkibat: $pengobatanNyeriAkibat,
    pengobatanNyeriAkibatLainnya: $pengobatanNyeriAkibatLainnya,
    kemampuanAktifitas: $kemampuanAktifitas,
    statusPsikologis: $statusPsikologis,
    statusPsikologisLainnya: $statusPsikologisLainnya,
    statusMental: $statusMental,
    statusMentalDisorientasi: $statusMentalDisorientasi,
    resikoA1: $resikoA1,
    resikoA2: $resikoA2,
    resikoB1: $resikoB1,
    resiko1Ket: $resiko1Ket,
    resiko2Ket: $resiko2Ket,
    resiko3Ket: $resiko3Ket,
    gizi1: $gizi1,
    gizi2: $gizi2,
    gizi3: $gizi3,
    gizi4: $gizi4,
    flagKaji:$flagKaji
    ){

      no_kaji
      no_medrec
      keluhan
      td
      td1
      td2
      nadi
      pernafasan
      pernafasan1
      pernafasan2
      suhu
      tb
      bb
      riwayatAlergiObat
      riwayatAlergiObatKet
      riwayatAlergiMakanan
      riwayatAlergiMakananKet
      riwayatAlergiPantangan
      nilaiIMT
      keluhanNyeri
      nips
      nipsEkspresiWajah
      nipsEkspresiWajahHasil
      nipsMenangis
      nipsMenangisHasil
      nipsPernafasan
      nipsPernafasanHasil
      nipsPergelanganTangan
      nipsPergelanganTanganHasil
      nipsKaki
      nipsKakiHasil
      nipsKesadaran
      nipsKesadaranHasil
      flacc
      flaccWajah
      flaccWajahHasil
      flaccKaki
      flaccKakiHasil
      flaccAktifitas
      flaccAktifitasHasil
      flaccMenangis
      flaccMenangisHasil
      flaccBersuara
      flaccBersuaraHasil
      faces
      nyeri
      skalaNyeri
      penyebabNyeri
      mulaiNyeri
      tipeNyeri
      karakteristikNyeri
      karakteristikNyeriLainnya
      nyeriMenyebar
      nyeriMenyebarHasil
      frekuensiNyeri
      pengobatanNyeri
      pengobatanNyeriAkibat
      pengobatanNyeriAkibatLainnya
      kemampuanAktifitas
      statusPsikologis
      statusPsikologisLainnya
      statusMental
      statusMentalDisorientasi
      resikoA1
      resikoA2
      resikoB1
      resiko1Ket
      resiko2Ket
      resiko3Ket
      gizi1
      gizi2
      gizi3
      gizi4
    }
}
`;

export const updateKidReviewQuery = gql`
mutation updateKidReviewQuery(
  $no_kaji: String,
  $no_medrec: String,
  $keluhan: String,
  $td: String,
  $td1: Int,
  $td2: Int,
  $nadi: String,
  $nadi1: Int,
  $nadi2: Int,
  $pernafasan: String,
  $pernafasan1: Int,
  $pernafasan2: Int,
  $suhu: Int,
  $tb: Int,
  $bb: Int,
  $riwayatAlergiObat: String,
  $riwayatAlergiObatKet: String,
  $riwayatAlergiMakanan: String,
  $riwayatAlergiMakananKet: String,
  $riwayatAlergiPantangan: String,
  $nilaiIMT: String,
  $keluhanNyeri: String,
  $nips: String,
  $nipsEkspresiWajah: String,
  $nipsEkspresiWajahHasil: String,
  $nipsMenangis: String,
  $nipsMenangisHasil: String,
  $nipsPernafasan: String,
  $nipsPernafasanHasil: String,
  $nipsPergelanganTangan: String,
  $nipsPergelanganTanganHasil: String,
  $nipsKaki: String,
  $nipsKakiHasil: String,
  $nipsKesadaran: String,
  $nipsKesadaranHasil: String,
  $flacc: String,
  $flaccWajah: String,
  $flaccWajahHasil: String,
  $flaccKaki: String,
  $flaccKakiHasil: String,
  $flaccAktifitas: String,
  $flaccAktifitasHasil: String,
  $flaccMenangis: String,
  $flaccMenangisHasil: String,
  $flaccBersuara: String,
  $flaccBersuaraHasil: String,
  $faces: String,
  $nyeri: String,
  $skalaNyeri: String,
  $penyebabNyeri: String,
  $mulaiNyeri: String,
  $tipeNyeri: String,
  $karakteristikNyeri: String,
  $karakteristikNyeriLainnya: String,
  $nyeriMenyebar: String,
  $nyeriMenyebarHasil: String,
  $frekuensiNyeri: String,
  $pengobatanNyeri: String,
  $pengobatanNyeriAkibat: String,
  $pengobatanNyeriAkibatLainnya: String,
  $kemampuanAktifitas: String,
  $statusPsikologis: String,
  $statusPsikologisLainnya: String,
  $statusMental: String,
  $statusMentalDisorientasi: String,
  $resikoA1: String,
  $resikoA2: String,
  $resikoB1: String,
  $resiko1Ket: String,
  $resiko2Ket: String,
  $resiko3Ket: String,
  $gizi1: String,
  $gizi2: String,
  $gizi3: String,
  $gizi4: String) {

  updateKidReview(
    no_kaji: $no_kaji,
    no_medrec: $no_medrec,
    keluhan: $keluhan, 
    td: $td,
    td1: $td1,
    td2: $td2,
    nadi: $nadi,
    pernafasan: $pernafasan,
    pernafasan1: $pernafasan1,
    pernafasan2: $pernafasan2,
    suhu: $suhu,
    tb: $tb,
    bb: $bb,
    riwayatAlergiObat: $riwayatAlergiObat,
    riwayatAlergiObatKet: $riwayatAlergiObatKet,
    riwayatAlergiMakanan: $riwayatAlergiMakanan,
    riwayatAlergiMakananKet: $riwayatAlergiMakananKet,
    riwayatAlergiPantangan: $riwayatAlergiPantangan,
    nilaiIMT: $nilaiIMT,
    keluhanNyeri: $keluhanNyeri,
    nips: $nips,
    nipsEkspresiWajah: $nipsEkspresiWajah,
    nipsEkspresiWajahHasil: $nipsEkspresiWajahHasil,
    nipsMenangis: $nipsMenangis,
    nipsMenangisHasil: $nipsMenangisHasil,
    nipsPernafasan: $nipsPernafasan,
    nipsPernafasanHasil: $nipsPernafasanHasil,
    nipsPergelanganTangan: $nipsPergelanganTangan,
    nipsPergelanganTanganHasil: $nipsPergelanganTanganHasil,
    nipsKaki: $nipsKaki,
    nipsKakiHasil: $nipsKakiHasil,
    nipsKesadaran: $nipsKesadaran,
    nipsKesadaranHasil: $nipsKesadaranHasil,
    flacc: $flacc,
    flaccWajah: $flaccWajah,
    flaccWajahHasil: $flaccWajahHasil,
    flaccKaki: $flaccKaki,
    flaccKakiHasil: $flaccKakiHasil,
    flaccAktifitas: $flaccAktifitas,
    flaccAktifitasHasil: $flaccAktifitasHasil,
    flaccMenangis: $flaccMenangis,
    flaccMenangisHasil: $flaccMenangisHasil,
    flaccBersuara: $flaccBersuara,
    flaccBersuaraHasil: $flaccBersuaraHasil,
    faces: $faces,
    nyeri: $nyeri,
    skalaNyeri: $skalaNyeri,
    penyebabNyeri: $penyebabNyeri,
    mulaiNyeri: $mulaiNyeri,
    tipeNyeri: $tipeNyeri,
    karakteristikNyeri: $karakteristikNyeri,
    karakteristikNyeriLainnya: $karakteristikNyeriLainnya,
    nyeriMenyebar: $nyeriMenyebar,
    nyeriMenyebarHasil: $nyeriMenyebarHasil,
    frekuensiNyeri: $frekuensiNyeri,
    pengobatanNyeri: $pengobatanNyeri,
    pengobatanNyeriAkibat: $pengobatanNyeriAkibat,
    pengobatanNyeriAkibatLainnya: $pengobatanNyeriAkibatLainnya,
    kemampuanAktifitas: $kemampuanAktifitas,
    statusPsikologis: $statusPsikologis,
    statusPsikologisLainnya: $statusPsikologisLainnya,
    statusMental: $statusMental,
    statusMentalDisorientasi: $statusMentalDisorientasi,
    resikoA1: $resikoA1,
    resikoA2: $resikoA2,
    resikoB1: $resikoB1,
    resiko1Ket: $resiko1Ket,
    resiko2Ket: $resiko2Ket,
    resiko3Ket: $resiko3Ket,
    gizi1: $gizi1,
    gizi2: $gizi2,
    gizi3: $gizi3,
    gizi4: $gizi4
    ){

      no_kaji
      no_medrec
      keluhan
      td
      td1
      td2
      nadi
      pernafasan
      pernafasan1
      pernafasan2
      suhu
      tb
      bb
      riwayatAlergiObat
      riwayatAlergiObatKet
      riwayatAlergiMakanan
      riwayatAlergiMakananKet
      riwayatAlergiPantangan
      nilaiIMT
      keluhanNyeri
      nips
      nipsEkspresiWajah
      nipsEkspresiWajahHasil
      nipsMenangis
      nipsMenangisHasil
      nipsPernafasan
      nipsPernafasanHasil
      nipsPergelanganTangan
      nipsPergelanganTanganHasil
      nipsKaki
      nipsKakiHasil
      nipsKesadaran
      nipsKesadaranHasil
      flacc
      flaccWajah
      flaccWajahHasil
      flaccKaki
      flaccKakiHasil
      flaccAktifitas
      flaccAktifitasHasil
      flaccMenangis
      flaccMenangisHasil
      flaccBersuara
      flaccBersuaraHasil
      faces
      nyeri
      skalaNyeri
      penyebabNyeri
      mulaiNyeri
      tipeNyeri
      karakteristikNyeri
      karakteristikNyeriLainnya
      nyeriMenyebar
      nyeriMenyebarHasil
      frekuensiNyeri
      pengobatanNyeri
      pengobatanNyeriAkibat
      pengobatanNyeriAkibatLainnya
      kemampuanAktifitas
      statusPsikologis
      statusPsikologisLainnya
      statusMental
      statusMentalDisorientasi
      resikoA1
      resikoA2
      resikoB1
      resiko1Ket
      resiko2Ket
      resiko3Ket
      gizi1
      gizi2
      gizi3
      gizi4
    }
}
`;

export const kidReviewByMedrecQuery = gql`
query kidReviewByMedrecQuery($no_medrec: String!) {

    kidReviewByMedrec(no_medrec: $no_medrec){
      no_kaji
      no_medrec
      keluhan
      td
      td1
      td2
      nadi
      pernafasan
      pernafasan1
      pernafasan2
      suhu
      tb
      bb
      riwayatAlergiObat
      riwayatAlergiObatKet
      riwayatAlergiMakanan
      riwayatAlergiMakananKet
      riwayatAlergiPantangan
      nilaiIMT
      keluhanNyeri
      nips
      nipsEkspresiWajah
      nipsEkspresiWajahHasil
      nipsMenangis
      nipsMenangisHasil
      nipsPernafasan
      nipsPernafasanHasil
      nipsPergelanganTangan
      nipsPergelanganTanganHasil
      nipsKaki
      nipsKakiHasil
      nipsKesadaran
      nipsKesadaranHasil
      flacc
      flaccWajah
      flaccWajahHasil
      flaccKaki
      flaccKakiHasil
      flaccAktifitas
      flaccAktifitasHasil
      flaccMenangis
      flaccMenangisHasil
      flaccBersuara
      flaccBersuaraHasil
      faces
      nyeri
      skalaNyeri
      penyebabNyeri
      mulaiNyeri
      tipeNyeri
      karakteristikNyeri
      karakteristikNyeriLainnya
      nyeriMenyebar
      nyeriMenyebarHasil
      frekuensiNyeri
      pengobatanNyeri
      pengobatanNyeriAkibat
      pengobatanNyeriAkibatLainnya
      kemampuanAktifitas
      statusPsikologis
      statusPsikologisLainnya
      statusMental
      statusMentalDisorientasi
      resikoA1
      resikoA2
      resikoB1
      resiko1Ket
      resiko2Ket
      resiko3Ket
      gizi1
      gizi2
      gizi3
      gizi4
    }
}
`;

export const kidReviewByMedrec0Query = gql`
query kidReviewByMedrec0Query($no_medrec: String!) {

    kidReviewByMedrec0(no_medrec: $no_medrec){
      no_kaji
      no_medrec
      keluhan
      td
      td1
      td2
      nadi
      pernafasan
      pernafasan1
      pernafasan2
      suhu
      tb
      bb
      riwayatAlergiObat
      riwayatAlergiObatKet
      riwayatAlergiMakanan
      riwayatAlergiMakananKet
      riwayatAlergiPantangan
      nilaiIMT
      keluhanNyeri
      nips
      nipsEkspresiWajah
      nipsEkspresiWajahHasil
      nipsMenangis
      nipsMenangisHasil
      nipsPernafasan
      nipsPernafasanHasil
      nipsPergelanganTangan
      nipsPergelanganTanganHasil
      nipsKaki
      nipsKakiHasil
      nipsKesadaran
      nipsKesadaranHasil
      flacc
      flaccWajah
      flaccWajahHasil
      flaccKaki
      flaccKakiHasil
      flaccAktifitas
      flaccAktifitasHasil
      flaccMenangis
      flaccMenangisHasil
      flaccBersuara
      flaccBersuaraHasil
      faces
      nyeri
      skalaNyeri
      penyebabNyeri
      mulaiNyeri
      tipeNyeri
      karakteristikNyeri
      karakteristikNyeriLainnya
      nyeriMenyebar
      nyeriMenyebarHasil
      frekuensiNyeri
      pengobatanNyeri
      pengobatanNyeriAkibat
      pengobatanNyeriAkibatLainnya
      kemampuanAktifitas
      statusPsikologis
      statusPsikologisLainnya
      statusMental
      statusMentalDisorientasi
      resikoA1
      resikoA2
      resikoB1
      resiko1Ket
      resiko2Ket
      resiko3Ket
      gizi1
      gizi2
      gizi3
      gizi4
    }
}
`;

export const setExaminationFlagQuery = gql`
mutation setExaminationFlagQuery($no_medrec: String!, $flag: Int!) {
  setExaminationFlag(no_medrec: $no_medrec, flag: $flag) {
  no_periksa,
  poli,
  nama_pasien,
  no_medrec,
  nodok,
  no_antrian,
  jaminan,
  umur,
  kelamin,
  status_pasien,
  kaji,
  flag
}
}
`;

export const EmployeeByNikQuery = gql`
query EmployeeByNikQuery($nik: String!) {
  karyawanByNIK(nik: $nik) {
    nik
    nama
    panggilan
    jnsktp
    ktp
    jenkel
    tempat
    tgllhr
    blnlhr
    thnlhr
    sts
    agama
    kp
    jln
    desa
    kecamatan
    no
    kota
    rt
    rw
    areahp1
    hp1
    areahp2
    hp2
    area
    telp
    areapager
    pager1
    pager2
    photo
    bpk
    tglmsk
    blnmsk
    thnmsk
    tglklr
    thnklr
    blnklr
    kd_bagian
    kd_jabatan
    shift
    status
    kd_unit
    no_urut
    kd_golongan
    kd_ptkp
    absensi
    flag_keluar
    nilai
    kd_divisi
    nm_divisi
    flag_paramedis
    nm_subdivisi
    kd_subdivisi
    aktifbekerja
    tgl_masuk
    sts_kerja
    sts_hutang
    sts_prg
    id_prg
    pass_prg
    kd_ruang
    nm_ruang
    level
  }
}
`;

export const getMWReviewById0Query = gql`
query getMWReviewById0Query($no_medrec: String!) {
  MWReviewByMedrec0(no_medrec: $no_medrec) {
    no_kaji,
      no_medrec,
      tanggal,
      jam,
      sumberData,
      rujukan,
      rujukan_ket,
      diagnosa_rujukan,
      keluhan,
      riwayatPenyakit,
      riwayatPenyakit_ket,
      dirawat,
      dirawat_diagnosa,
      dirawat_kapan,
      dirawat_di,
      dioprasi,
      dioprasi_jenis,
      dioprasi_kapan,
      pengobatan,
      pengobatan_obat,
      penyakitKeluarga,
      ketergantunganZat,
      pekerjaan,
      pekerjaan_ket,
      alergi,
      alergi_obat,
      alergi_obat_reaksi,
      alergi_makanan,
      alergi_makanan_reaksi,
      alergi_lainnya,
      alergi_lainnya_reaksi,
      kontrasepsi,
      kontrasepsi_jenis,
      kontrasepsi_lama,
      kontrasepsi_keluhan,
      pernikahan,
      menikah_kali,
      menikah_umur,
      menikah1,
      menikah2,
      menarche,
      siklus,
      siklus_teratur,
      siklus_teratur_ket,
      haid_volume,
      haid_keluhan,
      haid_hpht,
      haid_taksiran,
      ginekologi,
      riwayat_kehamilan,
      status_ekosos,
      bicara,
      bicara_ket,
      penerjemah,
      penerjemah_ket,
      isyarat,
      hambatan,
      hambatan_ket,
      resiko_a,
      resiko_b,
      resiko_hasil,
      beritahuDokter,
      beritahuDokter_pukul,
      aktivitas,
      aktivitas_ket,
      alatBantu,
      kronis_lokasi,
      kronis_frek,
      kronis_durasi,
      akut_lokasi,
      akut_frek,
      akut_durasi,
      skor_nyeri,
      nyeriHilang,
  }
}
`;

export const MidwifeReviewQuery = gql`
mutation MidwifeReviewQuery(
  $no_kaji: String,
  $no_medrec: String,
  $tanggal: String,
  $jam: String,
  $sumberData: String,
  $rujukan: String,
  $rujukan_ket: String,
  $diagnosa_rujukan:String,
  $keluhan: String,
  $riwayatPenyakit: String,
  $riwayatPenyakit_ket: String,
  $dirawat: String,
  $dirawat_diagnosa: String,
  $dirawat_kapan: String,
  $dirawat_di: String,
  $dioprasi: String,
  $dioprasi_jenis: String,
  $dioprasi_kapan: String,
  $pengobatan: String,
  $pengobatan_obat: String,
  $penyakitKeluarga: String,
  $ketergantunganZat: String,
  $pekerjaan: String,
  $pekerjaan_ket: String,
  $alergi: String,
  $alergi_obat: String,
  $alergi_obat_reaksi: String,
  $alergi_makanan: String,
  $alergi_makanan_reaksi: String,
  $alergi_lainnya: String,
  $alergi_lainnya_reaksi: String,
  $kontrasepsi: String,
  $kontrasepsi_jenis: String,
  $kontrasepsi_lama: String,
  $kontrasepsi_keluhan: String,
  $pernikahan: String,
  $menikah_kali: Int,
  $menikah_umur: Int,
  $menikah1: Int,
  $menikah2: Int,
  $menarche: Int,
  $siklus: Int,
  $siklus_teratur: String,
  $siklus_teratur_ket: Int,
  $haid_volume: Int,
  $haid_keluhan: String,
  $haid_hpht: String,
  $haid_taksiran: String,
  $ginekologi: String,
  $riwayat_kehamilan: String,
  $status_ekosos: String,
  $bicara: String,
  $bicara_ket: String,
  $penerjemah: String,
  $penerjemah_ket: String,
  $isyarat: String,
  $hambatan: String,
  $hambatan_ket: String,
  $resiko_a: String,
  $resiko_b: String,
  $resiko_hasil: String,
  $beritahuDokter: String,
  $beritahuDokter_pukul: String,
  $aktivitas: String,
  $aktivitas_ket: String,
  $alatBantu: String,
  $kronis_lokasi: String,
  $kronis_frek: String,
  $kronis_durasi: String,
  $akut_lokasi: String,
  $akut_frek: String,
  $akut_durasi: String,
  $skor_nyeri: Int,
  $nyeriHilang: String,
  $flagKaji: Int) {

  addMWReview(
    no_kaji: $no_kaji,
    no_medrec: $no_medrec,
    tanggal: $tanggal,
    jam: $jam,
    sumberData: $sumberData,
    rujukan: $rujukan,
    rujukan_ket: $rujukan_ket,
    diagnosa_rujukan: $diagnosa_rujukan,
    keluhan: $keluhan,
    riwayatPenyakit: $riwayatPenyakit,
    riwayatPenyakit_ket: $riwayatPenyakit_ket,
    dirawat: $dirawat,
    dirawat_diagnosa: $dirawat_diagnosa,
    dirawat_kapan: $dirawat_kapan,
    dirawat_di: $dirawat_di,
    dioprasi: $dioprasi,
    dioprasi_jenis: $dioprasi_jenis,
    dioprasi_kapan: $dioprasi_kapan,
    pengobatan: $pengobatan,
    pengobatan_obat: $pengobatan_obat,
    penyakitKeluarga: $penyakitKeluarga,
    ketergantunganZat: $ketergantunganZat,
    pekerjaan: $pekerjaan,
    pekerjaan_ket: $pekerjaan_ket,
    alergi: $alergi,
    alergi_obat: $alergi_obat,
    alergi_obat_reaksi: $alergi_obat_reaksi,
    alergi_makanan: $alergi_makanan,
    alergi_makanan_reaksi: $alergi_makanan_reaksi,
    alergi_lainnya: $alergi_lainnya,
    alergi_lainnya_reaksi: $alergi_lainnya_reaksi,
    kontrasepsi: $kontrasepsi,
    kontrasepsi_jenis: $kontrasepsi_jenis,
    kontrasepsi_lama:  $kontrasepsi_lama,
    kontrasepsi_keluhan: $kontrasepsi_keluhan,
    pernikahan: $pernikahan,
    menikah_kali: $menikah_kali,
    menikah_umur: $menikah_umur,
    menikah1: $menikah1,
    menikah2: $menikah2,
    menarche: $menarche,
    siklus: $siklus,
    siklus_teratur: $siklus_teratur,
    siklus_teratur_ket: $siklus_teratur_ket,
    haid_volume: $haid_volume,
    haid_keluhan: $haid_keluhan,
    haid_hpht: $haid_hpht,
    haid_taksiran: $haid_taksiran,
    ginekologi: $ginekologi,
    riwayat_kehamilan: $riwayat_kehamilan,
    status_ekosos: $status_ekosos,
    bicara: $bicara,
    bicara_ket: $bicara_ket,
    penerjemah: $penerjemah,
    penerjemah_ket: $penerjemah_ket,
    isyarat: $isyarat,
    hambatan: $hambatan,
    hambatan_ket: $hambatan_ket,
    resiko_a: $resiko_a,
    resiko_b: $resiko_b,
    resiko_hasil: $resiko_hasil,
    beritahuDokter: $beritahuDokter,
    beritahuDokter_pukul: $beritahuDokter_pukul,
    aktivitas: $aktivitas,
    aktivitas_ket: $aktivitas_ket,
    alatBantu: $alatBantu,
    kronis_lokasi: $kronis_lokasi,
    kronis_frek: $kronis_frek,
    kronis_durasi: $kronis_durasi,
    akut_lokasi: $akut_lokasi,
    akut_frek: $akut_frek,
    akut_durasi: $akut_durasi,
    skor_nyeri: $skor_nyeri,
    nyeriHilang: $nyeriHilang,
    flagKaji: $flagKaji){

      no_kaji,
      no_medrec,
      tanggal,
      jam,
      sumberData,
      rujukan,
      rujukan_ket,
      diagnosa_rujukan,
      keluhan,
      riwayatPenyakit,
      riwayatPenyakit_ket,
      dirawat,
      dirawat_diagnosa,
      dirawat_kapan,
      dirawat_di,
      dioprasi,
      dioprasi_jenis,
      dioprasi_kapan,
      pengobatan,
      pengobatan_obat,
      penyakitKeluarga,
      ketergantunganZat,
      pekerjaan,
      pekerjaan_ket,
      alergi,
      alergi_obat,
      alergi_obat_reaksi,
      alergi_makanan,
      alergi_makanan_reaksi,
      alergi_lainnya,
      alergi_lainnya_reaksi,
      kontrasepsi,
      kontrasepsi_jenis,
      kontrasepsi_lama,
      kontrasepsi_keluhan,
      pernikahan,
      menikah_kali,
      menikah_umur,
      menikah1,
      menikah2,
      menarche,
      siklus,
      siklus_teratur,
      siklus_teratur_ket,
      haid_volume,
      haid_keluhan,
      haid_hpht,
      haid_taksiran,
      ginekologi,
      riwayat_kehamilan,
      status_ekosos,
      bicara,
      bicara_ket,
      penerjemah,
      penerjemah_ket,
      isyarat,
      hambatan,
      hambatan_ket,
      resiko_a,
      resiko_b,
      resiko_hasil,
      beritahuDokter,
      beritahuDokter_pukul,
      aktivitas,
      aktivitas_ket,
      alatBantu,
      kronis_lokasi,
      kronis_frek,
      kronis_durasi,
      akut_lokasi,
      akut_frek,
      akut_durasi,
      skor_nyeri,
      nyeriHilang,
    }
}
`;

export const updateMWReviewQuery = gql`
mutation updateMWReviewQuery(
  $no_kaji: String,
  $no_medrec: String,
  $tanggal: String,
  $jam: String,
  $sumberData: String,
  $rujukan: String,
  $rujukan_ket: String,
  $diagnosa_rujukan:String,
  $keluhan: String,
  $riwayatPenyakit: String,
  $riwayatPenyakit_ket: String,
  $dirawat: String,
  $dirawat_diagnosa: String,
  $dirawat_kapan: String,
  $dirawat_di: String,
  $dioprasi: String,
  $dioprasi_jenis: String,
  $dioprasi_kapan: String,
  $pengobatan: String,
  $pengobatan_obat: String,
  $penyakitKeluarga: String,
  $ketergantunganZat: String,
  $pekerjaan: String,
  $pekerjaan_ket: String,
  $alergi: String,
  $alergi_obat: String,
  $alergi_obat_reaksi: String,
  $alergi_makanan: String,
  $alergi_makanan_reaksi: String,
  $alergi_lainnya: String,
  $alergi_lainnya_reaksi: String,
  $kontrasepsi: String,
  $kontrasepsi_jenis: String,
  $kontrasepsi_lama: String,
  $kontrasepsi_keluhan: String,
  $pernikahan: String,
  $menikah_kali: Int,
  $menikah_umur: Int,
  $menikah1: Int,
  $menikah2: Int,
  $menarche: Int,
  $siklus: Int,
  $siklus_teratur: String,
  $siklus_teratur_ket: Int,
  $haid_volume: Int,
  $haid_keluhan: String,
  $haid_hpht: String,
  $haid_taksiran: String,
  $ginekologi: String,
  $riwayat_kehamilan: String,
  $status_ekosos: String,
  $bicara: String,
  $bicara_ket: String,
  $penerjemah: String,
  $penerjemah_ket: String,
  $isyarat: String,
  $hambatan: String,
  $hambatan_ket: String,
  $resiko_a: String,
  $resiko_b: String,
  $resiko_hasil: String,
  $beritahuDokter: String,
  $beritahuDokter_pukul: String,
  $aktivitas: String,
  $aktivitas_ket: String,
  $alatBantu: String,
  $kronis_lokasi: String,
  $kronis_frek: String,
  $kronis_durasi: String,
  $akut_lokasi: String,
  $akut_frek: String,
  $akut_durasi: String,
  $skor_nyeri: Int,
  $nyeriHilang: String) {

  updateMWReview(
    no_kaji: $no_kaji,
    no_medrec: $no_medrec,
    tanggal: $tanggal,
    jam: $jam,
    sumberData: $sumberData,
    rujukan: $rujukan,
    rujukan_ket: $rujukan_ket,
    diagnosa_rujukan: $diagnosa_rujukan,
    keluhan: $keluhan,
    riwayatPenyakit: $riwayatPenyakit,
    riwayatPenyakit_ket: $riwayatPenyakit_ket,
    dirawat: $dirawat,
    dirawat_diagnosa: $dirawat_diagnosa,
    dirawat_kapan: $dirawat_kapan,
    dirawat_di: $dirawat_di,
    dioprasi: $dioprasi,
    dioprasi_jenis: $dioprasi_jenis,
    dioprasi_kapan: $dioprasi_kapan,
    pengobatan: $pengobatan,
    pengobatan_obat: $pengobatan_obat,
    penyakitKeluarga: $penyakitKeluarga,
    ketergantunganZat: $ketergantunganZat,
    pekerjaan: $pekerjaan,
    pekerjaan_ket: $pekerjaan_ket,
    alergi: $alergi,
    alergi_obat: $alergi_obat,
    alergi_obat_reaksi: $alergi_obat_reaksi,
    alergi_makanan: $alergi_makanan,
    alergi_makanan_reaksi: $alergi_makanan_reaksi,
    alergi_lainnya: $alergi_lainnya,
    alergi_lainnya_reaksi: $alergi_lainnya_reaksi,
    kontrasepsi: $kontrasepsi,
    kontrasepsi_jenis: $kontrasepsi_jenis,
    kontrasepsi_lama:  $kontrasepsi_lama,
    kontrasepsi_keluhan: $kontrasepsi_keluhan,
    pernikahan: $pernikahan,
    menikah_kali: $menikah_kali,
    menikah_umur: $menikah_umur,
    menikah1: $menikah1,
    menikah2: $menikah2,
    menarche: $menarche,
    siklus: $siklus,
    siklus_teratur: $siklus_teratur,
    siklus_teratur_ket: $siklus_teratur_ket,
    haid_volume: $haid_volume,
    haid_keluhan: $haid_keluhan,
    haid_hpht: $haid_hpht,
    haid_taksiran: $haid_taksiran,
    ginekologi: $ginekologi,
    riwayat_kehamilan: $riwayat_kehamilan,
    status_ekosos: $status_ekosos,
    bicara: $bicara,
    bicara_ket: $bicara_ket,
    penerjemah: $penerjemah,
    penerjemah_ket: $penerjemah_ket,
    isyarat: $isyarat,
    hambatan: $hambatan,
    hambatan_ket: $hambatan_ket,
    resiko_a: $resiko_a,
    resiko_b: $resiko_b,
    resiko_hasil: $resiko_hasil,
    beritahuDokter: $beritahuDokter,
    beritahuDokter_pukul: $beritahuDokter_pukul,
    aktivitas: $aktivitas,
    aktivitas_ket: $aktivitas_ket,
    alatBantu: $alatBantu,
    kronis_lokasi: $kronis_lokasi,
    kronis_frek: $kronis_frek,
    kronis_durasi: $kronis_durasi,
    akut_lokasi: $akut_lokasi,
    akut_frek: $akut_frek,
    akut_durasi: $akut_durasi,
    skor_nyeri: $skor_nyeri,
    nyeriHilang: $nyeriHilang){

      no_kaji,
      no_medrec,
      tanggal,
      jam,
      sumberData,
      rujukan,
      rujukan_ket,
      diagnosa_rujukan,
      keluhan,
      riwayatPenyakit,
      riwayatPenyakit_ket,
      dirawat,
      dirawat_diagnosa,
      dirawat_kapan,
      dirawat_di,
      dioprasi,
      dioprasi_jenis,
      dioprasi_kapan,
      pengobatan,
      pengobatan_obat,
      penyakitKeluarga,
      ketergantunganZat,
      pekerjaan,
      pekerjaan_ket,
      alergi,
      alergi_obat,
      alergi_obat_reaksi,
      alergi_makanan,
      alergi_makanan_reaksi,
      alergi_lainnya,
      alergi_lainnya_reaksi,
      kontrasepsi,
      kontrasepsi_jenis,
      kontrasepsi_lama,
      kontrasepsi_keluhan,
      pernikahan,
      menikah_kali,
      menikah_umur,
      menikah1,
      menikah2,
      menarche,
      siklus,
      siklus_teratur,
      siklus_teratur_ket,
      haid_volume,
      haid_keluhan,
      haid_hpht,
      haid_taksiran,
      ginekologi,
      riwayat_kehamilan,
      status_ekosos,
      bicara,
      bicara_ket,
      penerjemah,
      penerjemah_ket,
      isyarat,
      hambatan,
      hambatan_ket,
      resiko_a,
      resiko_b,
      resiko_hasil,
      beritahuDokter,
      beritahuDokter_pukul,
      aktivitas,
      aktivitas_ket,
      alatBantu,
      kronis_lokasi,
      kronis_frek,
      kronis_durasi,
      akut_lokasi,
      akut_frek,
      akut_durasi,
      skor_nyeri,
      nyeriHilang
    }
}
`;