import { Storage } from '@ionic/storage';
import { AuthService } from './../../providers/auth-service';
import { AntrianService } from '../../providers/antrian-service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

@IonicPage()
@Component({
  selector: 'page-doctor-poli',
  templateUrl: 'doctor-poli.html',
})
export class DoctorPoliPage implements OnInit {

  poli = "";
  doctorName = "";
  no_doctor = "";
  dataPoli: Array<any>;
  dataDoctor: Array<any>;

  /**
   * Creates an instance of DoctorPoliPage.
   * @param {NavController} navCtrl 
   * @param {NavParams} navParams 
   * @param {Storage} storage 
   * @param {AuthService} auth 
   * @param {AntrianService} antrianService 
   * @memberof DoctorPoliPage
   */
  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    private storage: Storage,
    private auth: AuthService,
    private antrianService: AntrianService,
    private menu: MenuController) { }


  ngOnInit() {
    this.antrianService.groupByPoli().subscribe(({ data }) => {
      this.dataPoli = [...data.groupByPoli];
    });
  }

  ionViewCanEnter() {
    return this.storage.get('token');
  }

  /**
   * get Doctor by Poli
   * @param poli
   * @memberof DoctorPoliPage
   */
  getDoctorByPoli() {
    this.antrianService.groupByDoctor(this.poli).subscribe(({ data }) => {
      this.dataDoctor = [...data.groupByDoctor];
    });
  }

  /**
   * 
   * 
   * @memberof DoctorPoliPage
   */
  getDoctorById() {
    this.antrianService.getDoctorById(this.no_doctor).subscribe(({ data }) => {
      const docData = data.doctorByNodok;
      this.doctorName = docData.nama_dok;
    });    
  }

  /**
   * go to Dashboard Queue Patient
   * @memberof DoctorPoliPage
   */
  openQueue() {
    if (this.doctorName != "" && this.poli != "") { 
      this.storage.set('poli', this.poli);
      this.storage.set('nama_dokter', this.doctorName);
      this.storage.set('nodok',this.no_doctor);
      this.menu.enable(true);
      this.navCtrl.setRoot('HomePage', { paramPoli: this.poli, paramDokter: this.doctorName, paramNodok: this.no_doctor });
    }
    else {
      this.alertMessage("Poli dan Dokter tidak boleh kosong");
    }


  }

  /**
   * logout account and redirect to Login page
   * @memberof DoctorPoliPage
   */
  logoutAccount() {
    this.auth.logout().subscribe(out => {
      this.navCtrl.setRoot('AuthenticationPage');
    });
  }
  /**
   * 
   * 
   * @param {string} message 
   * @memberof DoctorPoliPage
   */
  alertMessage(message: string) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

}
