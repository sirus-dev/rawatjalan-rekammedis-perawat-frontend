import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorPoliPage } from './doctor-poli';

@NgModule({
  declarations: [
    DoctorPoliPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorPoliPage),
  ],
})
export class DoctorPoliPageModule {}
