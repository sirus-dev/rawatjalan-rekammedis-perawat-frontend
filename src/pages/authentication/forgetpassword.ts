import { IonicPage, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { Loading, NavController } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-forget',
    templateUrl: 'forgetpassword.html'
})

export class ForgetPasswordPage implements OnInit {
    loading: Loading;
    forgetCredentials = { email: '' };
    
    constructor(public nav:NavController, private auth: AuthService, public storage:Storage, private alertCtrl: AlertController ) {}

    ngOnInit() { }

    public forgetPassword(){
        this.auth.forgetPass(this.forgetCredentials).subscribe(
            data => this.isSendEmail(data),
            error => console.log(error)
          );
    }

    isSendEmail(data){
        if(data.success){
            let alert = this.alertCtrl.create({
                title: 'Berhasil Kirim Email',
                subTitle: 'Mohon untuk cek email anda untuk proses selanjutnya',
                buttons: [{
                    text: 'OK',
                    role: 'OK',
                    handler: () => {
                        this.goToLogin();
                    }
                }]
            });
            alert.present();
        }else{
            this.auth.showError('Gagal Kirim Email','Email yang anda masukkan tidak terdaftar');
        }
    }

    goToLogin(){
        this.nav.push('AuthenticationPage');
    }
}