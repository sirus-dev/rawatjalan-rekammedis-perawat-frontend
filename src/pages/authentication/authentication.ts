import { AuthService } from './../../providers/auth-service';
import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams, Loading, AlertController, LoadingController } from 'ionic-angular';
import { IonicPage, NavController, Loading, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-authentication',
  templateUrl: 'authentication.html',
})
export class AuthenticationPage {

  loading: Loading;
  registerCredentials = { email: '', password: '' };
  
  /**
   * Creates an instance of AuthenticationPage.
   * @param {NavController} nav 
   * @param {AuthService} auth 
   * @param {AlertController} alertCtrl 
   * @param {LoadingController} loadingCtrl 
   * @param {Storage} storage 
   * @memberof AuthenticationPage
   */
  constructor(public nav:NavController, 
              private auth: AuthService, 
            //   private alertCtrl: AlertController, 
              public loadingCtrl:LoadingController, 
              public storage:Storage ) {}


  login(){
      let auth = this.auth.login(this.registerCredentials).subscribe(
        data => this.isLogin(data),
        error => this.logError(error)
    );

  }

  isLogin(data){
    if (data.success) {
        this.auth.saveToken(data, this.registerCredentials).subscribe(
            data => this.checkRole(),
            error => this.auth.showError("Gagal Login","Anda bukan perawat")
        );
    } else {
        return this.auth.showError("Gagal Login","Silakan cek kembali email dan password anda");
    }
  }

  goToForgetPage(){
      this.nav.push('ForgetPasswordPage');
  }

  checkRole(){
    this.storage.get('token').then((data)=>{
        if(data){
            this.storage.get('role').then((data)=>{
                console.log(data);
                if(data == "perawat"){
                    return this.nav.setRoot('DoctorPoliPage');
                }else{
                    this.auth.showError("Gagal Login","Anda bukan perawat");
                }
            });
        }else{
            console.log("TOKEN " + data);
            return this.auth.showError("Gagal Login","Silakan cek kembali email dan password anda");
        }
                            
    });
}
async logError(data) {
    return this.auth.showError("Gagal Login","Silakan cek kembali email dan password anda.");
}

}