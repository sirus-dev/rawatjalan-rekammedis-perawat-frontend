import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-midwife-pregnancy',
  templateUrl: 'midwife-pregnancy.html',
})
export class MidwifePregnancy {

  private pregnancyForm: FormGroup;
  
  birthDate: any;
  patientName: any;
  no_medrec: any;

  riwayat_kehamilan: Array<any>;
  formData: any;
  index: any;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, public navParams: NavParams, public viewCtrl: ViewController) {



    this.no_medrec = this.navParams.get('no_medrec');
    this.patientName = this.navParams.get('nama_pasien');
    this.birthDate = this.navParams.get('birthDate');

    let data = this.navParams.get('riwayat');
    this.index = this.navParams.get('index');


    console.log(this.index);
    if(this.index == 'new'){
      this.riwayat_kehamilan = [{
        tanggal:'',
        tempat: '',
        umur: '',
        jenis: '',
        penolong: '',
        penyulit: '',
        anak_jk: '',
        anak_bb: '',
        anak_pb: '',
        nifas: '',
        keadaan: ''
      }];
    }
    else{
      this.riwayat_kehamilan  = [data[this.index]];
    }
    
    this.pregnancyForm = this.formBuilder.group({
      'tanggal': [''],
      'tempat': [''],
      'umur': [''],
      'jenis': [''],
      'penolong': [''],
      'penyulit': [''],
      'anak_jk': [''],
      'anak_bb': [''],
      'anak_pb': [''],
      'nifas': [''],
      'keadaan': ['']
  });
  }

  submitPengkajian(formData) {
    if (this.pregnancyForm.valid) {
      this.viewCtrl.dismiss(formData);
  } else {
      // var excText = "";
      // if (formData.tanggal == "") { excText = "Tanggal" }
      // else if (formData.jam == "") { excText = "Jam" }
      // else if (formData.sumberData == "") { excText = "Sumber Data" }


      // let alert = this.alertCtrl.create({
      //     subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excText.bold() + ' tidak boleh kosong',
      //     buttons: ['OK'],
      //     enableBackdropDismiss: false
      // });
      // alert.present();
  }
    
}


  backPage() {
    this.viewCtrl.dismiss();
  }
}
