import { IonicPage, NavController, AlertController, NavParams, ViewController, ModalOptions } from 'ionic-angular';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AntrianService } from '../../providers/antrian-service';
import { StatusBar } from '@ionic-native/status-bar';
import { ModalController } from 'ionic-angular';
import { MidwifePregnancy } from './midwife-pregnancy';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'midwife-review-page',
    templateUrl: 'midwife-review.html'
})


export class MidwifeReview implements OnInit {

    myModalOptions: ModalOptions = {
        enableBackdropDismiss: false,
        cssClass: 'pregnancy'
    };

    myDate: String = moment().toISOString();
    weekDay: Array<any> = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];
    dt: any;
    month: any;
    year: any;
    hour: any;
    day: any;
    thisdate: any;

    reviewMenu: string = "bidan";

    private midwifeReviewForm: FormGroup;

    datareview: Array<any> = [];

    patientData: any;
    no_medrec: any;
    nama_pasien: any;
    age: "";
    birthDate: any;

    poli: any;
    dokter: any;
    nik: any;
    nurse: any;

    nokaji: any;
    sumberData: any = '';
    sumberData_lainnya: any = '';
    rujukan: any = '';
    rujukan_ket: any = '';
    rujukan_ket_rs: any = '';
    diagnosa_rujukan: any;
    keluhan: any;
    riwayatPenyakit: any = '';
    riwayatPenyakit_ket: any;
    dirawat: any = '';
    dirawat_diagnosa: any;
    dirawat_kapan: any;
    dirawat_di: any;
    dioprasi: any = '';
    dioprasi_jenis: any;
    dioprasi_kapan: any;
    pengobatan: any = '';
    pengobatan_obat: any;
    penyakitKeluarga: any = '';
    penyakitKeluarga_select: any = '';
    penyakitKeluarga_lainnya: any = '';
    ketergantunganZat: any = '';
    ketergantunganZat_select: any = '';
    ketergantunganZat_lainnya: any = '';
    pekerjaan: any = '';
    pekerjaan_ket: any;
    alergi: any = '';
    alergi_obat: any;
    alergi_obat_reaksi: any;
    alergi_makanan: any;
    alergi_makanan_reaksi: any;
    alergi_lainnya: any;
    alergi_lainnya_reaksi: any;
    kontrasepsi: any = '';
    kontrasepsi_jenis: any;
    kontrasepsi_lama: any;
    kontrasepsi_keluhan: any;
    pernikahan: any = '';
    menikah_kali: any;
    menikah_umur: any;
    menikah1: any;
    menikah2: any;
    menarche: any;
    siklus: any;
    siklus_teratur: any = '';
    siklus_teratur_ket: any;
    haid_volume: any;
    haid_keluhan: any = '';
    haid_hpht: any;
    haid_taksiran: any;
    ginekologi: any = '';
    ginekologi_select: any = '';
    ginekologi_lainnya: any = '';
    riwayat_kehamilan: any = [];
    status_ekosos: any = '';
    status_ekosos_lainnya: any = '';
    bicara: any = '';
    bicara_ket: any;
    penerjemah: any = '';
    penerjemah_ket: any;
    isyarat: any = '';
    hambatan: any = '';
    hambatan_ket: any;
    resiko_a: any = '';
    resiko_b: any = '';
    resiko_hasil: any = '';
    beritahuDokter: any = '';
    beritahuDokter_pukul: any;
    aktivitas: any = '';
    aktivitas_ket: any;
    alatBantu: any;
    kronis_lokasi: any;
    kronis_frek: any;
    kronis_durasi: any;
    akut_lokasi: any;
    akut_frek: any;
    akut_durasi: any;
    skor_nyeri: any = '';
    nyeriHilang: any = '';
    nyeriHilang_lainnya: any = '';

    tab_riwayatKesehatan: boolean = false;
    tab_riwayatKehamilan: boolean = false;
    tab_statusEkonomi: boolean = false;
    tab_kebutuhan: boolean = false;
    tab_resiko: boolean = false;
    tab_statusFungsional: boolean = false;
    tab_skala: boolean = false;
    tab_nutrisi: boolean = false;
    tab_komentar: boolean = false;

    constructor(private navCtrl: NavController, private formBuilder: FormBuilder, private alertCtrl: AlertController, private modalCtrl: ModalController,
        private navParams: NavParams, private antrianService: AntrianService, public viewCtrl: ViewController, public statusBar: StatusBar) {
        statusBar.overlaysWebView(false);
        statusBar.backgroundColorByHexString('#27394b');

        var salt = Math.floor(Math.random() * 10000);
        this.patientData = this.navParams.get('data');
        this.poli = this.navParams.get('poli');
        this.dokter = this.navParams.get('dokter')
        this.nik = this.navParams.get('nik');

        this.no_medrec = this.patientData.no_medrec;
        this.nama_pasien = this.patientData.nama_pasien;
        this.birthDate = this.patientData.tgl_lahir;

        var no_kaji = salt + this.patientData.no_medrec;
        this.setTimeNow();

        this.midwifeReviewForm = this.formBuilder.group({
            'no_kaji': [no_kaji, Validators.required],
            'no_medrec': [this.no_medrec, Validators.required],
            'tanggal': [this.thisdate],
            'jam': [this.myDate, Validators.required],
            'sumberData': ['', Validators.required],
            'sumberData_lainnya': [''],
            'rujukan': [''],
            'rujukan_ket': [''],
            'rujukan_ket_rs': [''],
            'diagnosa_rujukan': [''],
            'keluhan': [''],
            'riwayatPenyakit': [''],
            'riwayatPenyakit_ket': [''],
            'dirawat': [''],
            'dirawat_diagnosa': [''],
            'dirawat_kapan': [''],
            'dirawat_di': [''],
            'dioprasi': [''],
            'dioprasi_jenis': [''],
            'dioprasi_kapan': [''],
            'pengobatan': [''],
            'pengobatan_obat': [''],
            'penyakitKeluarga': [''],
            'penyakitKeluarga_select': [''],
            'penyakitKeluarga_lainnya': [''],
            'ketergantunganZat': [''],
            'ketergantunganZat_select': [''],
            'ketergantunganZat_lainnya': [''],
            'pekerjaan': [''],
            'pekerjaan_ket': [''],
            'alergi': [''],
            'alergi_obat': [''],
            'alergi_obat_reaksi': [''],
            'alergi_makanan': [''],
            'alergi_makanan_reaksi': [''],
            'alergi_lainnya': [''],
            'alergi_lainnya_reaksi': [''],
            'kontrasepsi': [''],
            'kontrasepsi_jenis': [''],
            'kontrasepsi_lama': [''],
            'kontrasepsi_keluhan': [''],
            'pernikahan': [''],
            'menikah_kali': [null],
            'menikah_umur': [null],
            'menikah1': [null],
            'menikah2': [null],
            'menarche': [null],
            'siklus': [null],
            'siklus_teratur': [''],
            'siklus_teratur_ket': [null],
            'haid_volume': [null],
            'haid_keluhan': [''],
            'haid_hpht': [''],
            'haid_taksiran': [''],
            'ginekologi': [''],
            'ginekologi_select': [''],
            'ginekologi_lainnya': [''],
            'riwayat_kehamilan': [''],
            'status_ekosos': [''],
            'status_ekosos_lainnya': [''],
            'bicara': [''],
            'bicara_ket': [''],
            'penerjemah': [''],
            'penerjemah_ket': [''],
            'isyarat': [''],
            'hambatan': [''],
            'hambatan_ket': [''],
            'resiko_a': [''],
            'resiko_b': [''],
            'resiko_hasil': [''],
            'beritahuDokter': [''],
            'beritahuDokter_pukul': [''],
            'aktivitas': [''],
            'aktivitas_ket': [''],
            'alatBantu': [''],
            'kronis_lokasi': [''],
            'kronis_frek': [''],
            'kronis_durasi': [''],
            'akut_lokasi': [''],
            'akut_frek': [''],
            'akut_durasi': [''],
            'skor_nyeri': [null],
            'nyeriHilang': [''],
            'nyeriHilang_lainnya': [''],
        });

        this.antrianService.getMWReviewByMedrecZero(this.no_medrec)
            .subscribe(({ data }) => {
                var reviewstr = JSON.stringify(data);
                var parsreview = JSON.parse(reviewstr);
                let reviewdata = parsreview.MWReviewByMedrec0[0];
                console.log(reviewdata);
                if (reviewdata == null) {
                    this.datareview = [{
                        no_kaji: '',
                        no_medrec: '',
                        tanggal: '',
                        jam: '',
                        sumberData: '',
                        rujukan: '',
                        rujukan_ket: '',
                        diagnosa_rujukan: '',
                        keluhan: '',
                        riwayatPenyakit: '',
                        riwayatPenyakit_ket: '',
                        dirawat: '',
                        dirawat_diagnosa: '',
                        dirawat_kapan: '',
                        dirawat_di: '',
                        dioprasi: '',
                        dioprasi_jenis: '',
                        dioprasi_kapan: '',
                        pengobatan: '',
                        pengobatan_obat: '',
                        penyakitKeluarga: '',
                        ketergantunganZat: '',
                        pekerjaan: '',
                        pekerjaan_ket: '',
                        alergi: '',
                        alergi_obat: '',
                        alergi_obat_reaksi: '',
                        alergi_makanan: '',
                        alergi_makanan_reaksi: '',
                        alergi_lainnya: '',
                        alergi_lainnya_reaksi: '',
                        kontrasepsi: '',
                        kontrasepsi_jenis: '',
                        kontrasepsi_lama: '',
                        kontrasepsi_keluhan: '',
                        pernikahan: '',
                        menikah_kali: '',
                        menikah_umur: '',
                        menikah1: '',
                        menikah2: '',
                        menarche: '',
                        siklus: '',
                        siklus_teratur: '',
                        siklus_teratur_ket: '',
                        haid_volume: '',
                        haid_keluhan: '',
                        haid_hpht: '',
                        haid_taksiran: '',
                        ginekologi: '',
                        riwayat_kehamilan: '',
                        status_ekosos: '',
                        bicara: '',
                        bicara_ket: '',
                        penerjemah: '',
                        penerjemah_ket: '',
                        isyarat: '',
                        hambatan: '',
                        hambatan_ket: '',
                        resiko_a: '',
                        resiko_b: '',
                        resiko_hasil: '',
                        beritahuDokter: '',
                        beritahuDokter_pukul: '',
                        aktivitas: '',
                        aktivitas_ket: '',
                        alatBantu: '',
                        kronis_lokasi: '',
                        kronis_frek: '',
                        kronis_durasi: '',
                        akut_lokasi: '',
                        akut_frek: '',
                        akut_durasi: '',
                        skor_nyeri: '',
                        nyeriHilang: ''
                    }];

                } else {
                    this.nokaji = reviewdata.no_kaji;
                    //this.tanggal = reviewdata.tanggal;
                    //this.jam: reviewdata.jam;
                    if (reviewdata.sumberData === 'pasien' || reviewdata.sumberData === 'keluarga' || reviewdata.sumberData === '') {
                        this.sumberData = reviewdata.sumberData;
                    }
                    else {
                        this.sumberData_lainnya = reviewdata.sumberData;
                        this.sumberData = 'lainnya';
                    }

                    this.rujukan = reviewdata.rujukan;

                    //this.rujukan_ket = reviewdata.rujukan_ket;
                    if (reviewdata.rujukan_ket === 'puskesmas' || reviewdata.rujukan_ket === 'dokter' || reviewdata.rujukan_ket === 'bidan' || reviewdata.rujukan_ket === '') {
                        this.rujukan_ket = reviewdata.rujukan_ket;
                    }
                    else {
                        this.rujukan_ket_rs = reviewdata.rujukan_ket;
                        this.rujukan_ket = 'rs';
                    }

                    this.diagnosa_rujukan = reviewdata.diagnosa_rujukan
                    this.keluhan = reviewdata.keluhan;
                    this.riwayatPenyakit = reviewdata.riwayatPenyakit;
                    this.riwayatPenyakit_ket = reviewdata.riwayatPenyakit_ket;
                    this.dirawat = reviewdata.dirawat;
                    this.dirawat_diagnosa = reviewdata.dirawat_diagnosa;
                    this.dirawat_kapan = reviewdata.dirawat_kapan;
                    this.dirawat_di = reviewdata.dirawat_di;
                    this.dioprasi = reviewdata.dioprasi;
                    this.dioprasi_jenis = reviewdata.dioprasi_jenis;
                    this.dioprasi_kapan = reviewdata.dioprasi_kapan;
                    this.pengobatan = reviewdata.pengobatan;
                    this.pengobatan_obat = reviewdata.pengobatan_obat;

                    //this.penyakitKeluarga = reviewdata.penyakitKeluarga;
                    if (reviewdata.penyakitKeluarga === 'tidak') {
                        this.penyakitKeluarga = reviewdata.penyakitKeluarga;
                    }
                    else {
                        this.penyakitKeluarga = 'ada';
                        if (reviewdata.penyakitKeluarga === 'hipertensik' || reviewdata.penyakitKeluarga === 'jantung' || reviewdata.penyakitKeluarga === 'paru' ||
                            reviewdata.penyakitKeluarga === 'dm' || reviewdata.penyakitKeluarga === 'ginjal' || reviewdata.penyakitKeluarga === "") {
                            this.penyakitKeluarga_select = reviewdata.penyakitKeluarga;

                        } else {
                            this.penyakitKeluarga_select = 'lainnya';
                            this.penyakitKeluarga_lainnya = reviewdata.penyakitKeluarga;
                        }
                    }

                    //this.ketergantunganZat = reviewdata.ketergantunganZat;
                    if (reviewdata.ketergantunganZat === 'tidak') {
                        this.ketergantunganZat = reviewdata.ketergantunganZat;
                    }
                    else {
                        this.ketergantunganZat = 'ya';
                        if (reviewdata.ketergantunganZat === 'obat-obatan' || reviewdata.ketergantunganZat === 'rokok' ||
                            reviewdata.ketergantunganZat === 'alkohol' || reviewdata.ketergantunganZat === "") {
                            this.ketergantunganZat_select = reviewdata.ketergantunganZat;
                        } else {
                            this.ketergantunganZat_select = 'lainnya';
                            this.ketergantunganZat_lainnya = reviewdata.ketergantunganZat;

                        }
                    }

                    this.pekerjaan = reviewdata.pekerjaan;
                    this.pekerjaan_ket = reviewdata.pekerjaan_ket;
                    this.alergi = reviewdata.alergi;
                    this.alergi_obat = reviewdata.alergi_obat;
                    this.alergi_obat_reaksi = reviewdata.alergi_obat_reaksi;
                    this.alergi_makanan = reviewdata.alergi_makanan;
                    this.alergi_makanan_reaksi = reviewdata.alergi_makanan_reaksi;
                    this.alergi_lainnya = reviewdata.alergi_lainnya;
                    this.alergi_lainnya_reaksi = reviewdata.alergi_lainnya_reaksi;
                    this.kontrasepsi = reviewdata.kontrasepsi;
                    this.kontrasepsi_jenis = reviewdata.kontrasepsi_jenis;
                    this.kontrasepsi_lama = reviewdata.kontrasepsi_lama;
                    this.kontrasepsi_keluhan = reviewdata.kontrasepsi_keluhan;
                    this.pernikahan = reviewdata.pernikahan
                    this.menikah_kali = reviewdata.menikah_kali;
                    this.menikah_umur = reviewdata.menikah_umur;
                    this.menikah1 = reviewdata.menikah1;
                    this.menikah2 = reviewdata.menikah2;
                    this.menarche = reviewdata.menarche;
                    this.siklus = reviewdata.siklus;
                    this.siklus_teratur = reviewdata.siklus_teratur;
                    this.siklus_teratur_ket = reviewdata.siklus_teratur_ket;
                    this.haid_volume = reviewdata.haid_volume;
                    this.haid_keluhan = reviewdata.haid_keluhan;
                    this.haid_hpht = reviewdata.haid_hpht
                    this.haid_taksiran = reviewdata.haid_taksiran;

                    //this.ginekologi = reviewdata.ginekologi;
                    if (reviewdata.ginekologi === 'tidak') {
                        this.ginekologi = reviewdata.ginekologi;
                    }
                    else {
                        this.ginekologi = 'ada';
                        if (reviewdata.ginekologi === 'infertilitas' || reviewdata.ginekologi === 'inveksi virus' || reviewdata.ginekologi === 'pms' ||
                            reviewdata.ginekologi === 'endometnosis' || reviewdata.ginekologi === 'myoma' || reviewdata.ginekologi === 'polyp cervix' ||
                            reviewdata.ginekologi === 'kanker' || reviewdata.ginekologi === "") {
                            this.ginekologi_select = reviewdata.ginekologi;
                        } else {
                            this.ginekologi_select = 'lainnya';
                            this.ginekologi_lainnya = reviewdata.ginekologi;

                        }
                    }

                    this.riwayat_kehamilan = JSON.parse(reviewdata.riwayat_kehamilan);

                    //this.status_ekosos = reviewdata.status_ekosos;
                    if (reviewdata.status_ekosos === 'asuransi' || reviewdata.status_ekosos === 'jaminan' ||
                        reviewdata.status_ekosos === 'sendiri' || reviewdata.status_ekosos === '') {
                        this.status_ekosos = reviewdata.status_ekosos;
                    }
                    else {
                        this.status_ekosos_lainnya = reviewdata.status_ekosos;
                        this.status_ekosos = 'lainnya';
                    }

                    this.bicara = reviewdata.bicara;
                    this.bicara_ket = reviewdata.bicara_ket;
                    this.penerjemah = reviewdata.penerjemah;
                    this.penerjemah_ket = reviewdata.penerjemah_ket;
                    this.isyarat = reviewdata.isyarat;
                    this.hambatan = reviewdata.hambatan;
                    this.hambatan_ket = reviewdata.hambatan_ket;
                    this.resiko_a = reviewdata.resiko_a;
                    this.resiko_b = reviewdata.resiko_b;
                    this.resiko_hasil = reviewdata.resiko_hasil;
                    this.beritahuDokter = reviewdata.beritahuDokter;
                    this.beritahuDokter_pukul = reviewdata.beritahuDokter_pukul;
                    this.aktivitas = reviewdata.aktivitas;
                    this.aktivitas_ket = reviewdata.aktivitas_ket;
                    this.alatBantu = reviewdata.alatBantu;
                    this.kronis_lokasi = reviewdata.kronis_lokasi;
                    this.kronis_frek = reviewdata.kronis_frek;
                    this.kronis_durasi = reviewdata.kronis_durasi;
                    this.akut_lokasi = reviewdata.akut_lokasi;
                    this.akut_frek = reviewdata.akut_frek;
                    this.akut_durasi = reviewdata.akut_durasi;
                    this.skor_nyeri = reviewdata.skor_nyeri;

                    //this.nyeriHilang = reviewdata.nyeriHilang;
                    if (reviewdata.nyeriHilang === 'minum obat' || reviewdata.nyeriHilang === 'istirahat' ||
                        reviewdata.nyeriHilang === 'mendengar musik' || reviewdata.nyeriHilang === 'berubah posisi tidur' ||
                        reviewdata.nyeriHilang === '') {
                        this.nyeriHilang = reviewdata.nyeriHilang;
                    }
                    else {
                        this.nyeriHilang_lainnya = reviewdata.nyeriHilang;
                        this.nyeriHilang = 'lainnya';
                    }

                    // this.riwayat_kehamilan = [{
                    //     tanggal: "01-01-01",
                    //     tempat: "Bandung",
                    //     umur: "5 Bulan",
                    //     jenis: "Cesar",
                    //     penolong: "Something",
                    //     penyulit: "Another",
                    //     anak_jk: "Perempuan",
                    //     anak_bb: "3.5 Kg",
                    //     anak_pb: "30cm",
                    //     nifas: "1",
                    //     keadaan: "Sehat"
                    // }, {
                    //     tanggal: "02-02-02",
                    //     tempat: "Cimahi",
                    //     umur: "2 Bulan",
                    //     jenis: "Normal",
                    //     penolong: "Something1",
                    //     penyulit: "Another1",
                    //     anak_jk: "Laki-laki",
                    //     anak_bb: "2.8 Kg",
                    //     anak_pb: "35cm",
                    //     nifas: "2",
                    //     keadaan: "Incubator"
                    // }];
                    // reviewdata.riwayat_kehamilan = this.riwayat_kehamilan;

                    this.datareview = [reviewdata];
                }
            });
    }

    setTimeNow() {
        this.myDate = moment().format();
        let datenow = new Date(moment().format());
        this.year = datenow.getFullYear();
        this.month = datenow.getMonth() + 1;
        this.dt = datenow.getDate();
        this.day = datenow.getDay();

        if (this.month < 10) {
            this.month = '0' + this.month;
        }
        if (this.dt < 10) {
            this.dt = '0' + this.dt;
        }
        this.thisdate = this.dt + '-' + this.month + '-' + this.year;
        console.log(this.myDate);
        console.log(this.thisdate);
    }

    ngOnInit() {

    }

    submitPengkajian(formData) {
        
        //formData.tanggal = this.thisdate;
        if (formData.sumberData == "lainnya") { formData.sumberData = formData.sumberData_lainnya; }
        if (formData.rujukan_ket == "rs") { formData.rujukan_ket = formData.rujukan_ket_rs; }
        if (formData.penyakitKeluarga == "ada") {
            formData.penyakitKeluarga = formData.penyakitKeluarga_select;
            if (formData.penyakitKeluarga_select == "lainnya") { formData.penyakitKeluarga = formData.penyakitKeluarga_lainnya; }
        }
        if (formData.ketergantunganZat == "ya") {
            formData.ketergantunganZat = formData.ketergantunganZat_select;
            if (formData.ketergantunganZat_select == "lainnya") { formData.ketergantunganZat = formData.ketergantunganZat_lainnya; }
        }
        if (formData.ginekologi == "ada") {
            formData.ginekologi = formData.ginekologi_select;
            if (formData.ginekologi_select == "lainnya") { formData.ginekologi = formData.ginekologi_lainnya; }
        }
        if (formData.status_ekosos == "lainnya") { formData.status_ekosos = formData.status_ekosos_lainnya; }
        if (formData.nyeriHilang == "lainnya") { formData.nyeriHilang = formData.nyeriHilang_lainnya; }
        
        if (this.midwifeReviewForm.valid) {
            let riwayat = JSON.stringify(this.riwayat_kehamilan);
            formData.riwayat_kehamilan = riwayat;
            this.antrianService.saveMidwifeReview(formData).subscribe(({ data }) => {
                var str = JSON.stringify(data);
                var pars = JSON.parse(str);
                let status = pars.addMWReview;
                this.antrianService.setStatusReview(status.no_medrec, 'Sudah Dikaji', this.poli, this.dokter).subscribe(({ data }) => {
                    var str = JSON.stringify(data);
                    var pars = JSON.parse(str);
                    let datapasien = pars.setReviewStatus;
                    if (datapasien.kaji == "Belum Dikaji" && datapasien.flag !== 1) {

                    } else {
                        let alert = this.alertCtrl.create({
                            subTitle: 'Pengkajian Pasien berhasil di input',
                            buttons: [{
                                text: 'OK',
                                role: 'OK',
                                handler: () => {
                                    this.navCtrl.setRoot('HomePage', { paramStatus: status.no_medrec });
                                }
                            },]
                        });
                        alert.present();
                    }
                });

            }, (error) => {

            });
        } else {
            var excText = "";
            if (formData.tanggal == "") { excText = "Tanggal" }
            else if (formData.jam == "") { excText = "Jam" }
            else if (formData.sumberData == "") { excText = "Sumber Data" }


            let alert = this.alertCtrl.create({
                subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excText.bold() + ' tidak boleh kosong',
                buttons: ['OK'],
                enableBackdropDismiss: false
            });
            alert.present();
        }
    }

    updateSubmitPengkajian(formData) {
        if (formData.sumberData == "lainnya") { formData.sumberData = formData.sumberData_lainnya; }
        if (formData.rujukan_ket == "rs") { formData.rujukan_ket = formData.rujukan_ket_rs; }
        if (formData.penyakitKeluarga == "ada") {
            formData.penyakitKeluarga = formData.penyakitKeluarga_select;
            if (formData.penyakitKeluarga_select == "lainnya") { formData.penyakitKeluarga = formData.penyakitKeluarga_lainnya; }
        }
        if (formData.ketergantunganZat == "ya") {
            formData.ketergantunganZat = formData.ketergantunganZat_select;
            if (formData.ketergantunganZat_select == "lainnya") { formData.ketergantunganZat = formData.ketergantunganZat_lainnya; }
        }
        if (formData.ginekologi == "ada") {
            formData.ginekologi = formData.ginekologi_select;
            if (formData.ginekologi_select == "lainnya") { formData.ginekologi = formData.ginekologi_lainnya; }
        }
        if (formData.status_ekosos == "lainnya") { formData.status_ekosos = formData.status_ekosos_lainnya; }
        if (formData.nyeriHilang == "lainnya") { formData.nyeriHilang = formData.nyeriHilang_lainnya; }

        if (this.midwifeReviewForm.valid) {
            let no_kaji_lama = this.datareview[0].no_kaji;
            formData.no_kaji = no_kaji_lama;
            let riwayat = JSON.stringify(this.riwayat_kehamilan);
            formData.riwayat_kehamilan = riwayat;
            // console.log(formData);
            this.antrianService.updateMidwifeReview(formData).subscribe(({ data }) => {
                var str = JSON.stringify(data);
                var pars = JSON.parse(str);
                let status = pars.updateMWReview;

                if (status.no_kaji == no_kaji_lama) {
                    let alert = this.alertCtrl.create({
                        subTitle: 'Pengkajian Pasien berhasil di input',
                        buttons: [{
                            text: 'OK',
                            role: 'OK',
                            handler: () => {
                                this.navCtrl.setRoot('HomePage', { paramStatus: status.no_medrec });
                            }
                        },]
                    });
                    alert.present();
                }
            });
        } else {
            var excText = "";
            if (formData.tanggal == "") { excText = "Tanggal" }
            else if (formData.jam == "") { excText = "Jam" }
            else if (formData.sumberData == "") { excText = "Sumber Data" }

            let alert = this.alertCtrl.create({
                subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excText.bold() + ' tidak boleh kosong',
                buttons: ['OK']
            });
            alert.present();
        }
    }

    openRiwayat(index: any) {
        let modal = this.modalCtrl.create('MidwifePregnancy', {
            riwayat: this.riwayat_kehamilan,
            index: index,
            no_medrec: this.no_medrec,
            nama_pasien: this.nama_pasien,
            birthDate: this.birthDate
        }, this.myModalOptions);

        if (index == 'new') {
            modal.onDidDismiss(data => {
                if (data) {
                    if (!this.riwayat_kehamilan) {
                        this.riwayat_kehamilan = [];
                    }
                    this.riwayat_kehamilan.push(data);
                }
            });
        }else{
            modal.onDidDismiss(data => {
                if (data) {
                    this.riwayat_kehamilan[index] = data;
                }
            });
        }

        modal.present();
    }

    deleteRiwayat(index: any){
        let alert = this.alertCtrl.create({
            subTitle: "Anda yakin riwayat ini akan dihapus?",
            buttons: [
                {
                    text: 'YA',
                    role: 'OKE',
                    handler: () => {
                        this.riwayat_kehamilan.splice(index,1);
                    }
                },
                {
                    text: 'TIDAK',
                    role: 'CANCEL',
                }
            ]
        });
        alert.present();
    }

    createModalWithParams() {
        let modal = this.modalCtrl.create('Calendar', { enableBackdropDismiss: false });
        modal.onDidDismiss(data => {
            if (data) {
                let date = data.toISOString();
                let realdate = moment(date).format();
                this.myDate = realdate;
                let datenow = new Date(realdate);
                this.year = datenow.getFullYear();
                this.month = datenow.getMonth() + 1;
                this.dt = datenow.getDate();
                this.day = datenow.getDay();

                if (this.month < 10) {
                    this.month = '0' + this.month;
                }
                if (this.dt < 10) {
                    this.dt = '0' + this.dt;
                }
                this.thisdate = this.dt + '-' + this.month + '-' + this.year;
            }
        });
        modal.present();
    }

    showForm(sub: any) {
        switch (sub) {

            case "tab_riwayatKesehatan":
                return this.tab_riwayatKesehatan = !this.tab_riwayatKesehatan;

            case "tab_riwayatKehamilan":
                return this.tab_riwayatKehamilan = !this.tab_riwayatKehamilan;

            case "tab_statusEkonomi":
                return this.tab_statusEkonomi = !this.tab_statusEkonomi;

            case "tab_kebutuhan":
                return this.tab_kebutuhan = !this.tab_kebutuhan;

            case "tab_resiko":
                return this.tab_resiko = !this.tab_resiko;

            case "tab_statusFungsional":
                return this.tab_statusFungsional = !this.tab_statusFungsional;

            case "tab_skala":
                return this.tab_skala = !this.tab_skala;

            case "tab_nutrisi":
                return this.tab_nutrisi = !this.tab_nutrisi;

            case "tab_komentar":
                return this.tab_komentar = !this.tab_komentar;
        }
    }

    backPage() {
        this.navCtrl.setRoot("HomePage");
    }
}
