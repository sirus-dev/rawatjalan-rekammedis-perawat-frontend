import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MidwifePregnancy } from './midwife-pregnancy';

@NgModule({
  declarations: [
    MidwifePregnancy,
  ],
  imports: [
    IonicPageModule.forChild(MidwifePregnancy),
  ],
})
export class MidwifePregnancyPageModule {}
