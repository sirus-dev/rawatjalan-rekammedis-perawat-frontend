import { AdultReview } from './adult-review';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KidReview } from './kid-review';
import { MidwifeReview } from './midwife-review';
import { SirusStylusModule } from '@sirus/stylus';
import { Stylus } from './stylus';

@NgModule({
  declarations: [
    AdultReview,
    KidReview,
    // MidwifeReview,
    Stylus
  ],
  imports: [
    SirusStylusModule.forRoot(
      '22eda92c-10af-40d8-abea-fd4093c17d81',
      'a1fa759f-b3ce-4091-9fd4-d34bb870c601',
       'webdemoapi.myscript.com'
    ),
    IonicPageModule.forChild(AdultReview)
  ],
  exports: [
    Stylus,
  ],
  entryComponents: [
    Stylus
  ]
})
export class AdultReviewPageModule {}
