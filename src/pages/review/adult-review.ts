import { IonicPage } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AntrianService } from '../../providers/antrian-service';
import { StylusComponent, InkChangeData, PenParametersInput } from '@sirus/stylus';

@IonicPage()
@Component({
    selector: 'adult-review-page',
    templateUrl: 'adult-review.html'
})

export class AdultReview {

    reviewMenu: string = "dewasa";

    home = 'HomePage';
    pen: PenParametersInput = {
        color: '#000',
        width: 2
    }

    private reviewForm: FormGroup;

    event = {
        date: new Date().toISOString()
    }

    patientData: any;
    no_medrec: any;
    nama_pasien: any;
    age: any;
    birthDate: any;

    poli: any;
    dokter: any;
    nik: any;
    nurse: any;

    //totalScore = 0;
    score1 = 0;
    score2 = 0;
    score3 = 0;

    datareview: Array<any> = [];

    nokaji: any;

    stylusSignature: any;


    resikoA1: any;
    resikoA2: any;
    resikoB1: any;
    resikoTindakan1action: any;
    resikoTindakan2action: any;
    resikoTindakan3action: any;
    nyeri: any;
    nyeri_score: any;
    resiko_jatuh_a: any;
    resiko_jatuh_b: any;
    gizi1: any;
    gizi2: any;
    gizi3: any;
    gizi3_lainnya: any;
    status_fungsional: any;
    status_fungsional_ket: any;
    tt_perawat: any;
    // anamnesa_keluhan: any;
    // anamnesa_riwayat_penyakit: any;
    // fisik_kepala: any;
    // fisik_leher: any;
    // fisik_mulut: any;
    // fisik_paru: any;
    // fisik_genitalia: any;
    // fisik_mata: any;
    // fisik_tht: any;
    // fisik_jantung: any;
    // fisik_abdomen: any;
    // fisik_ekstremitas: any;
    // status_gizi: any;
    // pemeriksaan_gigi: any;
    // pemeriksaan_gigi_ket: any;
    // status_lokalis: any;
    // pemeriksaan_penunjang: any;
    // diagnosis: any;
    // rencana: any;
    // dirujuk: any;
    // dirujuk_lainnya: any;

    resiko_jatuh: boolean = false;
    s_nyeri: boolean = false;
    s_resiko_jatuh: boolean = false;
    s_gizi: boolean = false;
    s_fungsional: boolean = false;
    as_dokter: boolean = false;



    constructor(private navCtrl: NavController, private formBuilder: FormBuilder, private alertCtrl: AlertController, private navParams: NavParams, private antrianService: AntrianService, public viewCtrl: ViewController, public statusBar: StatusBar) {

        statusBar.overlaysWebView(false);
        statusBar.backgroundColorByHexString('#27394b');

        var salt = Math.floor(Math.random() * 10000);
        this.patientData = this.navParams.get('data');
        this.poli = this.navParams.get('poli');
        this.dokter = this.navParams.get('dokter')
        this.nik = this.navParams.get('nik');
        this.getNurseData(this.nik);
        this.stylusSignature = "";
        var no_kaji = salt + this.patientData.no_medrec
        this.reviewForm = this.formBuilder.group({
            'no_kaji': [no_kaji, Validators.required],
            'no_medrec': [this.patientData.no_medrec, Validators.required],
            'keluhan': ['', Validators.required],
            'resikoA1': [''],
            'resikoA2': [''],
            'resikoB1': [''],
            'resikoHasil1': [''],
            'resikoHasil2': [''],
            'resikoHasil3': [''],
            'resikoTindakan1action': [''],
            'resikoTindakan2action': [''],
            'resikoTindakan3action': [''],
            'resikoTindakan1': [''],
            'resikoTindakan2': [''],
            'resikoTindakan3': [''],
            'td1': ['', Validators.required],
            'td2': ['', Validators.required],
            'nadi1': ['', Validators.required],
            'nadi2': ['menit'],
            'pernafasan1': ['', Validators.required],
            'pernafasan2': ['menit'],
            'suhu': ['', Validators.required],
            'tb': ['', Validators.required],
            'bb': ['', Validators.required],
            'nyeri': [''],
            'resiko_jatuh_a': [''],
            'resiko_jatuh_b': [''],
            'gizi1': [''],
            'gizi2': [''],
            'gizi3': [''],
            'gizi3_lainnya': [''],
            'status_fungsional': [''],
            'status_fungsional_ket': [''],
            'tt_perawat': [''],
            // 'anamnesa_keluhan': [''],
            // 'anamnesa_riwayat_penyakit': [''],
            // 'fisik_kepala': [''],
            // 'fisik_leher': [''],
            // 'fisik_mulut': [''],
            // 'fisik_paru': [''],
            // 'fisik_genitalia': [''],
            // 'fisik_mata': [''],
            // 'fisik_tht': [''],
            // 'fisik_jantung': [''],
            // 'fisik_abdomen': [''],
            // 'fisik_ekstremitas': [''],
            // 'status_gizi': [''],
            // 'pemeriksaan_gigi': [''],
            // 'pemeriksaan_gigi_ket': [''],
            // 'status_lokalis': [''],
            // 'pemeriksaan_penunjang': [''],
            // 'diagnosis': [''],
            // 'rencana': [''],
            // 'dirujuk': [''],
            // 'dirujuk_lainnya': [''],
        });

        this.no_medrec = this.patientData.no_medrec;
        this.nama_pasien = this.patientData.nama_pasien;
        this.birthDate = this.patientData.tgl_lahir;
        this.getFullBirthDate();

        this.antrianService.getReviewByMedrecZero(this.no_medrec)
            .subscribe(({ data }) => {
                var reviewstr = JSON.stringify(data);
                var parsreview = JSON.parse(reviewstr);
                let reviewdata = parsreview.reviewByMedrec0;
                if (reviewdata == null) {
                    this.datareview = [{
                        no_kaji: '',
                        no_medrec: '',
                        keluhan: '',
                        resikoA1: '',
                        resikoA2: '',
                        resikoB1: '',
                        resikoHasil1: '',
                        resikoHasil2: '',
                        resikoHasil3: '',
                        resikoTindakan1action: '',
                        resikoTindakan2action: '',
                        resikoTindakan3action: '',
                        resikoTindakan1: '',
                        resikoTindakan2: '',
                        resikoTindakan3: '',
                        td: '',
                        nadi: '',
                        pernafasan: '',
                        suhu: '',
                        tb: '',
                        bb: '',
                        nyeri: '',
                        resiko_jatuh_a: '',
                        resiko_jatuh_b: '',
                        gizi1: '',
                        gizi2: '',
                        gizi3: '',
                        gizi3_lainnya: '',
                        status_fungsional: '',
                        status_fungsional_ket: '',
                        tt_perawat: ''
                        // anamnesa_keluhan: '',
                        // anamnesa_riwayat_penyakit: '',
                        // fisik_kepala: '',
                        // fisik_leher: '',
                        // fisik_mulut: '',
                        // fisik_paru: '',
                        // fisik_genitalia: '',
                        // fisik_mata: '',
                        // fisik_tht: '',
                        // fisik_jantung: '',
                        // fisik_abdomen: '',
                        // fisik_ekstremitas: '',
                        // status_gizi: '',
                        // pemeriksaan_gigi: '',
                        // pemeriksaan_gigi_ket: '',
                        // status_lokalis: '',
                        // pemeriksaan_penunjang: '',
                        // diagnosis: '',
                        // rencana: '',
                        // dirujuk: '',
                        // dirujuk_lainnya: ''
                    }];

                } else {
                    this.nokaji = reviewdata.no_kaji;
                    this.resikoA1 = reviewdata.resikoA1;
                    this.resikoA2 = reviewdata.resikoA2;
                    this.resikoB1 = reviewdata.resikoB1;
                    this.resikoTindakan1action = reviewdata.resikoTindakan1action;
                    this.resikoTindakan2action = reviewdata.resikoTindakan2action;
                    this.resikoTindakan3action = reviewdata.resikoTindakan3action;
                    if (reviewdata.nyeri == "0" || reviewdata.nyeri == "") {
                        this.nyeri = reviewdata.nyeri;
                    } else {
                        this.nyeri = 'ada';
                        this.nyeri_score = reviewdata.nyeri;
                    }
                    this.resiko_jatuh_a = reviewdata.resiko_jatuh_a;
                    this.resiko_jatuh_b = reviewdata.resiko_jatuh_b;
                    this.gizi1 = reviewdata.gizi1;
                    this.gizi2 = reviewdata.gizi2;
                    this.gizi3 = reviewdata.gizi3;
                    this.gizi3_lainnya = reviewdata.gizi3_lainnya;
                    this.status_fungsional = reviewdata.status_fungsional;
                    this.status_fungsional_ket = reviewdata.status_fungsional_ket;
                    // this.tt_perawat = reviewdata.tt_perawat;
                    // this.anamnesa_keluhan = reviewdata.anamnesa_keluhan;
                    // this.anamnesa_riwayat_penyakit = reviewdata.anamnesa_riwayat_penyakit;
                    // this.fisik_kepala = reviewdata.fisik_kepala;
                    // this.fisik_leher = reviewdata.fisik_leher;
                    // this.fisik_mulut = reviewdata.fisik_mulut;
                    // this.fisik_paru = reviewdata.fisik_paru;
                    // this.fisik_genitalia = reviewdata.fisik_genitalia;
                    // this.fisik_mata = reviewdata.fisik_mata;
                    // this.fisik_tht = reviewdata.fisik_tht;
                    // this.fisik_jantung = reviewdata.fisik_jantung;
                    // this.fisik_abdomen = reviewdata.fisik_abdomen;
                    // this.fisik_ekstremitas = reviewdata.fisik_ekstremitas;
                    // this.status_gizi = reviewdata.status_gizi;
                    // this.pemeriksaan_gigi = reviewdata.pemeriksaan_gigi;
                    // this.pemeriksaan_gigi_ket = reviewdata.pemeriksaan_gigi_ket;
                    // this.status_lokalis = reviewdata.status_lokalis;
                    // this.pemeriksaan_penunjang = reviewdata.pemeriksaan_penunjang;
                    // this.diagnosis = reviewdata.diagnosis;
                    // this.rencana = reviewdata.rencana;
                    // this.dirujuk = reviewdata.dirujuk;
                    // this.dirujuk_lainnya = reviewdata.dirujuk_lainnya;

                    this.datareview = [reviewdata];
                }
            });
    }

    onChecked($event, $value) {
        if ($event == "ya" && $value == "gizi1") {
            this.score1 = 1;
        }
        if ($event == "tidak" && $value == "gizi1") {
            this.score1 = 0;
        }
        if ($event == "ya" && $value == "gizi2") {
            this.score2 = 1;
        }
        if ($event == "tidak" && $value == "gizi2") {
            this.score2 = 0;
        }
        if ($event == "ya" && $value == "gizi3") {
            this.score3 = 1;
        }
        if ($event == "tidak" && $value == "gizi3") {
            this.score3 = 0;
        }

        var totalScore = this.score1 + this.score2 + this.score3;

        if (totalScore > 2) {
            this.antrianService.setStatusPatient(this.patientData.no_medrec, "pending", 3, this.poli, this.dokter).subscribe(({ data }) => {
                if (data) {
                    let alert = this.alertCtrl.create({
                        title: 'Notifikasi',
                        subTitle: 'Skor Pasien telah mencapai lebih dari 2, Pasien perlu konsultasi ke ahli gizi',
                        buttons: [{
                            text: 'OK',
                            role: 'OK',
                            handler: () => {
                                this.navCtrl.setRoot(this.home);
                            }
                        },]
                    });
                    alert.present();
                }
            });
        }
    }

    backPage() {
        this.navCtrl.setRoot("HomePage");
    }

    stylusChange($event: any) {
        this.stylusSignature = $event;
    }

    getNurseData(nik_nurse: any) {
        this.antrianService.getEmployeeByNik(nik_nurse).subscribe(({ data }) => {
            var str = JSON.stringify(data);
            var pars = JSON.parse(str);
            let nurse = pars.karyawanByNIK;
            this.nurse = nurse.nama;
        }, (error) => {

        });
    }

    getFullBirthDate() {
        let now = new Date();

        let year = this.birthDate.slice(0, 4);
        let month = this.birthDate.slice(5, 7);
        let date = this.birthDate.slice(8, 10);

        let leap_year = 0;
        for (let i = year; i < now.getFullYear(); i++) {
            if (i % 4 == 0) {
                leap_year++;
            }
        }

        let past = new Date(year, month - 1, date);
        let diff = Math.floor(now.getTime() - past.getTime());
        let day = (1000 * 60 * 60 * 24);

        let days = Math.floor(diff / day);
        let age_day = Math.round(days % 30.4167) - leap_year;
        let age_month = Math.floor(days / 30.4167) % 12;
        let age_year = Math.floor(Math.floor(days / 30.4167) / 12);

        if (age_year > 17) {
            this.age = age_year + " Tahun";
        }
        else {
            this.age = age_year + " Tahun " + age_month + " Bulan " + age_day + " Hari ";
        }
    }

    submitPengkajian(formData) {
        console.log(formData);
        formData.tt_perawat = this.stylusSignature;

        if (formData.tt_perawat == "") { 
            var excTxt = "Tanda Tangan";
            let alert = this.alertCtrl.create({
                subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excTxt.bold() + ' tidak boleh kosong. Pastikan untuk tekan tombol "Simpan Stylus" terlebih dahulu untuk menyimpan hasil tanda tangan',
                buttons: ['OK'],
                enableBackdropDismiss: false
            });
            alert.present(); 
        }else{
            if (this.reviewForm.valid) {
                formData.nadi2 = 1;
                formData.pernafasan2 = 1;
                this.antrianService.saveAdultReview(formData).subscribe(({ data }) => {
                    var str = JSON.stringify(data);
                    var pars = JSON.parse(str);
                    let status = pars.adultReview;
                    this.antrianService.setStatusReview(status.no_medrec, 'Sudah Dikaji', this.poli, this.dokter).subscribe(({ data }) => {
                        var str = JSON.stringify(data);
                        var pars = JSON.parse(str);
                        let datapasien = pars.setReviewStatus;
                        if (datapasien.kaji == "Belum Dikaji" && datapasien.flag !== 1) {
    
                        } else {
                            let alert = this.alertCtrl.create({
                                subTitle: 'Pengkajian Pasien berhasil di input',
                                buttons: [{
                                    text: 'OK',
                                    role: 'OK',
                                    handler: () => {
                                        this.navCtrl.setRoot(this.home, { paramStatus: status.no_medrec });
                                    }
                                },]
                            });
                            alert.present();
                        }
                    });
    
                }, (error) => {
    
                });
            } else {
                var excText = "";
                if (formData.keluhan == "") { excText = "Keluhan" }
                else if (formData.td1 == "") { excText = "TD Sistole" }
                else if (formData.td2 == "") { excText = "TD Diastole" }
                else if (formData.nadi1 == "") { excText = "Nadi (x)" }
                else if (formData.pernafasan1 == "") { excText = "Pernafasan (x)" }
                else if (formData.suhu == "") { excText = "Suhu" }
                else if (formData.tb == "") { excText = "TB" }
                else if (formData.bb == "") { excText = "BB" }
                
    
                let alert = this.alertCtrl.create({
                    subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excText.bold() + ' tidak boleh kosong',
                    buttons: ['OK'],
                    enableBackdropDismiss: false
                });
                alert.present();
            }
        }
    }

    updateSubmitPengkajian(formData) {
        formData.tt_perawat = this.stylusSignature;

        if (formData.tt_perawat == "") { 
            var excTxt = "Tanda Tangan";
            let alert = this.alertCtrl.create({
                subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excTxt.bold() + ' tidak boleh kosong. Pastikan untuk tekan tombol "Simpan Stylus" terlebih dahulu untuk menyimpan hasil tanda tangan',
                buttons: ['OK'],
                enableBackdropDismiss: false
            });
            alert.present(); 
        }else{
            if (this.reviewForm.valid) {
                let no_kaji_lama = this.datareview[0].no_kaji;
                formData.no_kaji = no_kaji_lama;
                formData.nadi2 = 1;
                formData.pernafasan2 = 1;
                console.log(formData);
                this.antrianService.updateAdultReview(formData).subscribe(({ data }) => {
                    var str = JSON.stringify(data);
                    var pars = JSON.parse(str);
                    let status = pars.updateReview;
    
                    if (status.no_kaji == no_kaji_lama) {
                        let alert = this.alertCtrl.create({
                            subTitle: 'Pengkajian Pasien berhasil di input',
                            buttons: [{
                                text: 'OK',
                                role: 'OK',
                                handler: () => {
                                    this.navCtrl.setRoot(this.home, { paramStatus: status.no_medrec });
                                }
                            },]
                        });
                        alert.present();
                    }
                });
            } else {
                var excText = "";
                if (formData.keluhan == "") { excText = "Keluhan" }
                else if (formData.td1 == "") { excText = "TD Sistole" }
                else if (formData.td2 == "") { excText = "TD Diastole" }
                else if (formData.nadi1 == "") { excText = "Nadi (x)" }
                else if (formData.pernafasan1 == "") { excText = "Pernafasan (x)" }
                else if (formData.suhu == "") { excText = "Suhu" }
                else if (formData.tb == "") { excText = "TB" }
                else if (formData.bb == "") { excText = "BB" }
                else if (formData.tt_perawat == "") { excText = "Tanda Tangan" }
    
                let alert = this.alertCtrl.create({
                    subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap. Form ' + excText.bold() + ' tidak boleh kosong',
                    buttons: ['OK']
                });
                alert.present();
            }
        }
    }

    showForm(sub: any) {
        switch (sub) {

            case "resiko_jatuh":
                return this.resiko_jatuh = !this.resiko_jatuh;

            case "s_nyeri":
                return this.s_nyeri = !this.s_nyeri;

            case "s_resiko_jatuh":
                return this.s_resiko_jatuh = !this.s_resiko_jatuh;

            case "s_gizi":
                return this.s_gizi = !this.s_gizi;

            case "s_fungsional":
                return this.s_fungsional = !this.s_fungsional;

            case "as_dokter":
                return this.as_dokter = !this.as_dokter;
        }
    }

}