import { AntrianService } from './../../providers/antrian-service';
import { ViewController, NavParams, AlertController, NavController, IonicPage } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'kid-review-page',
    templateUrl: 'kid-review.html'
})

export class KidReview implements OnInit {

    home = 'HomePage';

    private childReviewForm: FormGroup;

    datareview: Array<any> = [];

    @Input('nama-pasien')
    nama_pasien: string;

    @Input('no-medrec')
    no_medrec: string;

    @Input('age')
    age: string;

    @Input('birthDate')
    birthDate: string;

    nokaji:any;

    poli:any;
    dokter:any;

    riwayatAlergiObat:any;
    riwayatAlergiMakanan:any;
    nilaiIMT:any;
    keluhanNyeri:any;
    nips:any;
    nipsEkspresiWajah:any;
    nipsMenangis:any;
    nipsPernafasan:any;
    nipsPergelanganTangan:any;
    nipsKaki:any;
    nipsKesadaran:any;
    flacc:any;
    flaccWajah:any;
    flaccKaki:any;
    flaccAktifitas:any;
    flaccMenangis:any;
    flaccBersuara:any;
    faces:any;
    nyeri:any;
    tipeNyeri:any;
    karakteristikNyeri:any;
    nyeriMenyebar:any;
    frekuensiNyeri:any;
    pengobatanNyeri:any;
    pengobatanNyeriAkibat:any;
    kemampuanAktifitas:any;
    statusPsikologis:any;
    statusMental:any;
    statusMentalDisorientasi:any;
    resikoA1:any;
    resikoA2:any;
    resikoB1:any;

    constructor(private navCtrl: NavController, private formBuilder: FormBuilder, 
        private alertCtrl: AlertController, private navParams: NavParams, 
        private antrianService: AntrianService, public viewCtrl: ViewController) {

        var salt = Math.floor(Math.random() * 10000);
        var no_kaji = salt + this.navParams.get('paramId');

        this.childReviewForm = this.formBuilder.group({
            'no_kaji': [no_kaji, Validators.required],
            'no_medrec': [this.navParams.get('paramId'), Validators.required],
            'keluhan': [''],
            'td1': ['', Validators.required],
            'td2': ['', Validators.required],
            'nadi': ['', Validators.required],
            'penafasan': ['', Validators.required],
            'suhu': ['', Validators.required],
            'tb': ['', Validators.required],
            'bb': ['', Validators.required],
            'riwayatAlergiObat': [''],
            'riwayatAlergiObatKet': [''],
            'riwayatAlergiMakanan': [''],
            'riwayatAlergiMakananKet': [''],
            'riwayatAlergiPantangan': [''],
            'nilaiIMT': [''],
            'keluhanNyeri': [''],
            'nips': [''],
            'nipsEkspresiWajah': [''],
            'nipsEkspresiWajahHasil': [''],
            'nipsMenangis': [''],
            'nipsMenangisHasil': [''],
            'nipsPernafasan': [''],
            'nipsPernafasanHasil': [''],
            'nipsPergelanganTangan': [''],
            'nipsPergelanganTanganHasil': [''],
            'nipsKaki': [''],
            'nipsKakiHasil': [''],
            'nipsKesadaran': [''],
            'nipsKesadaranHasil': [''],
            'flacc': [''],
            'flaccWajah': [''],
            'flaccWajahHasil': [''],
            'flaccKaki': [''],
            'flaccKakiHasil': [''],
            'flaccAktifitas': [''],
            'flaccAktifitasHasil': [''],
            'flaccMenangis': [''],
            'flaccMenangisHasil': [''],
            'flaccBersuara': [''],
            'flaccBersuaraHasil': [''],
            'faces': [''],
            'nyeri': [''],
            'skalaNyeri': [''],
            'penyebabNyeri': [''],
            'mulaiNyeri': [''],
            'tipeNyeri': [''],
            'karakteristikNyeri': [''],
            'karakteristikNyeriLainnya': [''],
            'nyeriMenyebar': [''],
            'nyeriMenyebarHasil': [''],
            'frekuensiNyeri': [''],
            'pengobatanNyeri': [''],
            'pengobatanNyeriAkibat': [''],
            'pengobatanNyeriAkibatLainnya': [''],
            'kemampuanAktifitas': [''],
            'statusPsikologis': [''],
            'statusPsikologisLainnya': [''],
            'statusMental': [''],
            'statusMentalDisorientasi': [''],
            'resikoA1': [''],
            'resikoA2': [''],
            'resikoB1': [''],
            'resiko1Ket': [''],
            'resiko2Ket': [''],
            'resiko3Ket': ['']

        });

    }

    ngOnChanges(){}

    ngOnInit() {
        this.antrianService.getKidReviewByMedrecZero(this.no_medrec)
        .subscribe(({data}) => {
          var reviewstr = JSON.stringify(data);
          var parsreview = JSON.parse(reviewstr);
          let reviewdata = parsreview.kidReviewByMedrec0;
          if(reviewdata == null){
            this.datareview = 
              [{no_kaji: '',
              no_medrec: '',
              keluhan: '',
              td1: '',
              td2: '',
              nadi: '',
              penafasan: '',
              suhu: '',
              tb: '',
              bb: '',
              riwayatAlergiObat: '',
              riwayatAlergiObatKet: '',
              riwayatAlergiMakanan: '',
              riwayatAlergiMakananKet: '',
              riwayatAlergiPantangan: '',
              nilaiIMT: '',
              keluhanNyeri: '',
              nips: '',
              nipsEkspresiWajah: '',
              nipsEkspresiWajahHasil: '',
              nipsMenangis: '',
              nipsMenangisHasil: '',
              nipsPernafasan: '',
              nipsPernafasanHasil: '',
              nipsPergelanganTangan: '',
              nipsPergelanganTanganHasil: '',
              nipsKaki: '',
              nipsKakiHasil: '',
              nipsKesadaran: '',
              nipsKesadaranHasil: '',
              flacc: '',
              flaccWajah: '',
              flaccWajahHasil: '',
              flaccKaki: '',
              flaccKakiHasil: '',
              flaccAktifitas: '',
              flaccAktifitasHasil: '',
              flaccMenangis: '',
              flaccMenangisHasil: '',
              flaccBersuara: '',
              flaccBersuaraHasil: '',
              faces: '',
              nyeri: '',
              skalaNyeri: '',
              penyebabNyeri: '',
              mulaiNyeri: '',
              tipeNyeri: '',
              karakteristikNyeri: '',
              karakteristikNyeriLainnya: '',
              nyeriMenyebar: '',
              nyeriMenyebarHasil: '',
              frekuensiNyeri: '',
              pengobatanNyeri: '',
              pengobatanNyeriAkibat: '',
              pengobatanNyeriAkibatLainnya: '',
              kemampuanAktifitas: '',
              statusPsikologis: '',
              statusPsikologisLainnya: '',
              statusMental: '',
              statusMentalDisorientasi: '',
              resikoA1: '',
              resikoA2: '',
              resikoB1: '',
              resiko1Ket: '',
              resiko2Ket: '',
              resiko3Ket: ''}];
          }else{
            this.nokaji = reviewdata.no_kaji;
            this.resikoA1 = reviewdata.resikoA1;
            this.resikoA2 = reviewdata.resikoA2;
            this.resikoB1 = reviewdata.resikoB1;
            this.riwayatAlergiObat = reviewdata.riwayatAlergiObat;
            this.riwayatAlergiMakanan = reviewdata.riwayatAlergiMakanan;
            this.nilaiIMT = reviewdata.nilaiIMT;
            this.keluhanNyeri = reviewdata.keluhanNyeri;
            this.nips = reviewdata.nips;
            this.nipsEkspresiWajah = reviewdata.nipsEkspresiWajah;
            this.nipsMenangis = reviewdata.nipsMenangis;
            this.nipsPernafasan = reviewdata.nipsPernafasan;
            this.nipsPergelanganTangan = reviewdata.nipsPergelanganTangan;
            this.nipsKaki = reviewdata.nipsKaki;
            this.nipsKesadaran = reviewdata.nipsKesadaran;
            this.flacc = reviewdata.flacc;
            this.flaccWajah = reviewdata.flaccWajah;
            this.flaccKaki = reviewdata.flaccKaki;
            this.flaccAktifitas = reviewdata.flaccAktifitas;
            this.flaccMenangis = reviewdata.flaccMenangis;
            this.flaccBersuara = reviewdata.flaccBersuara;
            this.faces = reviewdata.faces;
            this.nyeri = reviewdata.nyeri;
            this.tipeNyeri = reviewdata.tipeNyeri;
            this.karakteristikNyeri = reviewdata.karakteristikNyeri;
            this.nyeriMenyebar = reviewdata.nyeriMenyebar;
            this.frekuensiNyeri = reviewdata.frekuensiNyeri;
            this.pengobatanNyeri = reviewdata.pengobatanNyeri;
            this.pengobatanNyeriAkibat = reviewdata.pengobatanNyeriAkibat;
            this.kemampuanAktifitas = reviewdata.kemampuanAktifitas;
            this.statusPsikologis = reviewdata.statusPsikologis;
            this.statusMental = reviewdata.statusMental;
            this.statusMentalDisorientasi = reviewdata.statusMentalDisorientasi;

            this.datareview = [reviewdata];
          }
        }); 

     }

    submitPengkajianAnak(formData) {

        if (this.childReviewForm.valid) {

            this.antrianService.saveKidReview(formData).subscribe(({ data }) => {

                var str = JSON.stringify(data);
                var pars = JSON.parse(str);
                let status = pars.addKidReview;
                this.antrianService.setStatusReview(status.no_medrec, 'Sudah Dikaji', this.poli, this.dokter).subscribe(({ data }) => {
                    var str = JSON.stringify(data);
                    var pars = JSON.parse(str);
                    let datapasien = pars.setReviewStatus;
                    if (datapasien.kaji == "Belum Dikaji" && datapasien.flag !== 1) {


                    } else {

                        let alert = this.alertCtrl.create({
                            subTitle: 'Pengkajian Pasien berhasil di input',
                            buttons: [{
                                text: 'OK',
                                role: 'OK',
                                handler: () => {
                                    this.navCtrl.setRoot(this.home, { paramStatus: status.no_medrec });
                                }
                            },]
                        });
                        alert.present();
                    }
                });

            }, (error) => {

            });

        } else {

            let alert = this.alertCtrl.create({
                subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap',
                buttons: ['OK']
            });
            alert.present();

        }

    }

    updateSubmitPengkajianAnak(formData) {

        if (this.childReviewForm.valid) {
            let no_kaji_lama = this.datareview[0].no_kaji;
            formData.no_kaji = no_kaji_lama;

            this.antrianService.updateKidReview(formData).subscribe(({ data }) => {
                var str = JSON.stringify(data);
                var pars = JSON.parse(str);
                let status = pars.updateKidReview;
                
                    if(status.no_kaji == no_kaji_lama){

                        let alert = this.alertCtrl.create({
                            subTitle: 'Pengkajian Pasien berhasil di input',
                            buttons: [{
                                text: 'OK',
                                role: 'OK',
                                handler: () => {
                                    this.navCtrl.setRoot(this.home, { paramStatus: status.no_medrec });
                                }
                            },]
                        });
                        alert.present();
                    }

            }, (error) => {

            });

        } else {

            let alert = this.alertCtrl.create({
                subTitle: 'Maaf, harap isi Pengkajian Pemeriksaan Fisik dengan lengkap',
                buttons: ['OK']
            });
            alert.present();

        }
    }
}