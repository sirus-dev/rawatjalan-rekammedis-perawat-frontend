import { Calendar } from './calendar';
import { CalendarModule } from 'ion2-calendar';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Calendar
  ],
  imports: [
    CalendarModule,
    IonicPageModule.forChild(Calendar),
  ],
})
export class CalendarPageModule {}
