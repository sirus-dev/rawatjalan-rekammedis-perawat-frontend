import { FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, ModalController } from 'ionic-angular';
import { AntrianService } from '../../providers/antrian-service';
import { Component, OnInit } from '@angular/core';
import { ViewController, ToastController, NavParams } from 'ionic-angular';
import { Printer, PrintOptions } from '@ionic-native/printer';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'controlletter-modal',
  templateUrl: 'controlletter.html'
})

export class ControlLetterPage implements OnInit {
  datapersonal: Array<any>;
  myDate: String = moment().toISOString();
  weekDay: Array<any> = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];
  dt: any;
  month: any;
  year: any;
  hour: any;
  day: any;
  message: string;
  thisdate: any;

  dataPatient: any;

  private formSMS: FormGroup;

  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController, private antrianService: AntrianService, public params: NavParams, private formBuilder: FormBuilder, private printer: Printer, public modalCtrl: ModalController) {

    this.dataPatient = params.get('personal');

    this.formSMS = this.formBuilder.group({
      'dayControl': [''],
      'dateControl': [''],
      'hourControl': [''],
      'description': [''],
    });
  }

  ionViewDidLoad() {

    if (this.dataPatient) {
      this.datapersonal = [this.dataPatient];

    } else {
      this.datapersonal = [{
        nama_pasien: '',
        jaminan: '',
        tgl_lahir: '',
        no_medrec: '',
        umur: '-',
        kelamin: ''

      }];
    }
  }

  ngOnInit() {
    this.myDate = moment().format();
    let datenow = new Date(moment().format());
    this.year = datenow.getFullYear();
    this.month = datenow.getMonth() + 1;
    this.dt = datenow.getDate();
    this.day = datenow.getDay();

    if (this.month < 10) {
      this.month = '0' + this.month;
    }
    if (this.dt < 10) {
      this.dt = '0' + this.dt;
    }
    this.thisdate = this.dt + '-' + this.month + '-' + this.year;
  }

  smsSubmit(formData) {

    this.antrianService.getDataPatientByMedrec(this.dataPatient.no_medrec).subscribe(({ data }) => {

      var str = JSON.stringify(data);
      var pars = JSON.parse(str);
      let datapatient = pars.patientByNorm;

      if (data) {
        let date = new Date(formData.hourControl);
        this.day = date.getDay();
        this.hour = date.getHours();

        if (this.hour < 10) {
          this.hour = '0' + this.hour;
        }

        let dateFormat = moment(date).format('DD MMMM YYYY');

        this.message = "SURAT KONTROL ELEKTRONIK" + "%0A" + "Kami dari RS Mary Cileungsi memberitahukan kepada Bapak,Ibu,Saudara,i dengan Identitas dibawah ini " + "%0A" + "Nama : " + datapatient.nama + "%0A" + "No Medrec : " + datapatient.no_medrec + "%0A" + "Diagnosa : " + formData.description + "%0A" + "Pada Hari : " + this.weekDay[this.day] + "%0A" + "Tanggal : " + dateFormat + "%0A" + "Jam : " + this.hour + ".00 WIB\n\n -Pesan ini hanya uji coba, mohon diabaikan-";

        this.antrianService.sendSMS(datapatient.telp, this.message);

        let toast = this.toastCtrl.create({
          message: 'Pengiriman Surat Kontrol via SMS Berhasil',
          duration: 3000,
          position: 'top'
        });
        toast.present();
        this.viewCtrl.dismiss();

      } else {

      }
    });
  }

  dismiss() {

    this.viewCtrl.dismiss();

  }

  printSubmit(formData) {

      if (this.dataPatient) {
        let date = new Date(formData.hourControl);
        this.day = date.getDay();
        this.hour = date.getHours();

        if (this.hour < 10) {
          this.hour = '0' + this.hour;
        }

        let dateFormat = moment(date).format('DD MMMM YYYY');

        let options: PrintOptions = {
          name: 'MyDocuments',
          landscape: true,
          grayscale: true
        };

        this.printer.isAvailable().then((onsuccess: any) => {

          this.printer.print("<center><h1>Surat Kontrol Elektronik</h1></center><hr><br>Kami dari RS Mary Cileungsi memberitahukan kepada Bapak,Ibu,Saudara,i dengan Identitas dibawah ini<br><br><table border=0><tr><td>Nama</td><td>:</td><td>" + this.dataPatient.nama_pasien + "</td></tr><tr><td>No Medrec</td><td>:</td><td>" + this.dataPatient.no_medrec + "</td></tr><tr><td>Diagnosa</td><td>:</td><td>" + formData.description + "</td></tr><tr><td>Pada Hari</td><td>:</td><td>" + this.weekDay[this.day] + "</td></tr><tr><td>Tanggal </td><td>:</td><td>" + dateFormat + "</td></tr><tr><td>Jam</td><td>:</td><td>" + this.hour + ".00 WIB</td></tr></table>", options).then((value: any) => {

            let toast = this.toastCtrl.create({
              message: 'Cetak Surat Kontrol Berhasil',
              duration: 3000,
              position: 'top'
            });

            toast.present();

            this.viewCtrl.dismiss();
          }, (error) => {

            let toast = this.toastCtrl.create({
              message: 'Gagal Cetak Surat Kontrol',
              duration: 3000,
              position: 'top'
            });

            toast.present();

            this.viewCtrl.dismiss();
          });

        }
          , (err) => {
            alert('err:' + err)
          });

      } else {

      }
  }

  createModalWithParams() {
    let modal = this.modalCtrl.create('Calendar', { enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data) {
        let date = data.toISOString();
        let realdate = moment(date).format();
        this.myDate = realdate;
        let datenow = new Date(realdate);
        this.year = datenow.getFullYear();
        this.month = datenow.getMonth() + 1;
        this.dt = datenow.getDate();
        this.day = datenow.getDay();

        if (this.month < 10) {
          this.month = '0' + this.month;
        }
        if (this.dt < 10) {
          this.dt = '0' + this.dt;
        }
        this.thisdate = this.dt + '-' + this.month + '-' + this.year;
      }
    });
    modal.present();
  }


}
