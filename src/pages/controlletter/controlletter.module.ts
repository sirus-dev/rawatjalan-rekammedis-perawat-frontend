import { ControlLetterPage } from './controlletter';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    ControlLetterPage
  ],
  imports: [
    IonicPageModule.forChild(ControlLetterPage),
  ],
})
export class ControlLetterPageModule {}
