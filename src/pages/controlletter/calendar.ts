import { ViewController, IonicPage } from 'ionic-angular';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'calendar-modal',
    templateUrl: 'calendar.html'
})

export class Calendar implements OnInit {
    date: string;
    type: 'string'; 
    dt:any;
    month:any;
    year:any;
    hour:any;
    day:any;
    message:string;
    data:any;

    constructor(public viewCtrl: ViewController) { }

    ngOnInit(){
        this.date = moment().format();
    }
  
    onChange($event) {
        let date = new Date($event._d);
        this.data  = date;
        this.viewCtrl.dismiss(this.data);
    }

    dismiss() {
        this.data = new Date();
        this.viewCtrl.dismiss(this.data);

  }
}