import { IonicPage } from 'ionic-angular';
import { AntrianService } from '../../providers/antrian-service';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import {
  AlertController,
  LoadingController,
  ModalController,
  NavController,
  NavParams,
  ToastController,
} from 'ionic-angular';
import { examineComponent } from './examine/examine';
import { Subscription } from "rxjs/Subscription";
import { Loading } from 'ionic-angular/components/loading/loading';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage implements OnInit {

  examine: examineComponent;

  dataExamination: Boolean = false;
  dataExaminationIsActive: Boolean = false;
  examinedone: Boolean = false;
  doneExamination: Boolean = false;

  review = 'AdultReview';

  dataPatient: Array<any> = [];
  dataWaitingList: Array<any> = [];
  dataPendingPatient: Array<any> = [];
  dataReviewing: Array<any> = [];
  dataReviewingDone: Array<any> = [];

  dataReviewingAge: string;
  dataReviewingDoneAge: string;

  showQueue: boolean = false;

  countOfCall: string = "3";

  counterCountOfQueue: any;
  showTimeNow: any;
  dataByNoRM: string;

  poliname: any;
  doctorname: any;
  no_doctor: any;
  nik_nurse: any = "0";

  ngSubscribe: Subscription = new Subscription();
  loading: Loading;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private antrianService: AntrianService
  ) {

    this.initializeItems();
    this.storage.get('token').then((data) => {

    });

    this.storage.get('nik').then((data) => {
      this.nik_nurse = data;
    });
  }

  initializeItems() {

    const MILLISECONDS = 1000;

    setInterval(() => {
      let datetime = new Date();
      let hour = datetime.getHours();
      let minutes = datetime.getMinutes();
      this.showTimeNow = hour + ":" + (minutes < 10 ? "0" + minutes : minutes);
    }, MILLISECONDS);

  }

  ngOnInit() {
    this.loadData();
    this.loadSubscription();
    this.loadingMessage("Mohon Tunggu . . .",2000);
    setTimeout(() => {
      this.loadPatientExamination("0");
      this.loadPatientExamination("1");
    }, 2000);
  }

  ngOnDestroy() {
    this.ngSubscribe.unsubscribe();
  }

  loadSubscription() {
    setTimeout(() => {
      this.ngSubscribe.add(this.antrianService.loadQueueSubscription(this.no_doctor, 0, this.poliname, this.doctorname)
        .subscribe(({ data }) => {
          this.loadData();
        }));

      this.ngSubscribe.add(this.antrianService.loadQueueSubscription(this.no_doctor, 1, this.poliname, this.doctorname)
        .subscribe(({ data }) => {
          this.loadData();
        }));

      this.ngSubscribe.add(this.antrianService.loadQueueSubscription(this.no_doctor, 3, this.poliname, this.doctorname)
        .subscribe(({ data }) => {
          this.loadData();
        }));

      this.ngSubscribe.add(this.antrianService.loadExamineSubscription(this.no_doctor, 1, this.poliname, this.doctorname)
        .subscribe(({ data }) => {

          this.antrianService.loadPatientExamination(this.no_doctor).subscribe(({ data }) => {
            let datapatient = [data.examineLast];
            if (datapatient[0] == null) {
              this.dataExamination = false;
              this.dataReviewing = [
                {
                  no_antrian: '', nama_pasien: '',
                  no_medrec: '',
                  jaminan: '',
                  umur: '',
                  status_pasien: '',
                  kelamin: '',
                  kaji: ''
                }];
                this.dataReviewingAge = '';
            } else {
              if (datapatient[0].flag == 0) {
                this.dataExaminationIsActive = false;
              } else if (datapatient[0].flag == 1) {
                this.dataExaminationIsActive = false;
              } else {
                this.dataExaminationIsActive = true;
              }
              this.dataExamination = true;
              this.dataReviewing = datapatient;
              this.dataReviewingAge = this.getFullBirthDate(this.dataReviewing[0].tgl_lahir);
            }
          });

          this.antrianService.loadPatientExamination1(this.no_doctor).subscribe(({ data }) => {
            let datapatient = [data.examineLast1];
            if (datapatient[0] == null) {
              this.doneExamination = false;
              this.dataReviewingDone = [
                {
                  no_antrian: '', nama_pasien: '',
                  no_medrec: '',
                  jaminan: '',
                  umur: '',
                  status_pasien: '',
                  kelamin: '',
                  kaji: ''
                }];
                this.dataReviewingDoneAge = '';
            } else {
              this.doneExamination = true;
              this.dataReviewingDone = datapatient;
              this.dataReviewingDoneAge = this.getFullBirthDate(this.dataReviewingDone[0].tgl_lahir);
            }
          });
        }));
    }, 2000);
  }

  loadData() {
    this.storage.get('poli').then((data) => {
      if (data) {
        let poli = data;
        this.storage.get('nama_dokter').then((data) => {
          if (data) {
            this.storage.get('nodok').then((data) => {
              this.no_doctor = data;
            });

            let doctorNameData = data;
            this.poliname = poli;
            this.doctorname = doctorNameData;
            this.antrianService.loadQueue(poli, doctorNameData)
              .subscribe(({ data }) => {

                let patientqueueAvailable = [...data.available];
                let patientqueueWaiting = [...data.waiting];
                let patientqueuePending = [...data.pending];
                if (patientqueueWaiting) {
                  this.showQueue = true;
                }
                this.dataPatient = patientqueueAvailable;
                this.dataWaitingList = patientqueueWaiting;
                this.dataPendingPatient = patientqueuePending; 
                setTimeout(() => {
                  this.counterCountOfQueue = this.dataPatient.length;
                  // this.loadPatientExamination("0");
                  // this.loadPatientExamination("1");
                }, 2000);
                //console.log(this.dataPatient);
              });
          }
        });
      } else {
        this.no_doctor = this.navParams.get('nodok');
        this.antrianService.loadQueue(this.navParams.get('paramPoli'), this.navParams.get('paramDokter'))
          .subscribe(({ data }) => {
            let patientqueueAvailable = [...data.available];
            let patientqueueWaiting = [...data.waiting];
            let patientqueuePending = [...data.pending];
            if (patientqueueWaiting) {
              this.showQueue = true;
            }
            this.dataPatient = patientqueueAvailable;
            this.dataWaitingList = patientqueueWaiting;
            this.dataPendingPatient = patientqueuePending;
            setTimeout(() => {
              this.counterCountOfQueue = this.dataPatient.length;
              // this.loadPatientExamination("0");
              // this.loadPatientExamination("1");
            }, 2000);
          });
      }
    });
  }

  loadPatientExamination(status) {
    if (this.dataReviewing.length == 0) {
      this.storage.get('poli').then((data) => {
        let poli = data;
        this.storage.get('nama_dokter').then((data) => {
          if (data) {
            let doctorNameData = data;
            this.poliname = poli;
            this.doctorname = doctorNameData;
            switch (status) {
              case "0":
                this.antrianService.loadPatientExamination(this.no_doctor).subscribe(({ data }) => {
                  let datapatient = [data.examineLast];
                  if (datapatient[0] == null) {
                    this.dataExamination = false;
                    this.dataReviewing = [
                      {
                        no_antrian: '', nama_pasien: '',
                        no_medrec: '',
                        jaminan: '',
                        umur: '',
                        status_pasien: '',
                        kelamin: '',
                        kaji: ''
                      }];
                      this.dataReviewingAge = '';
                  } else {
                    if (datapatient[0].flag == 0) {
                      this.dataExaminationIsActive = false;
                    } else if (datapatient[0].flag == 1) {
                      this.dataExaminationIsActive = false;
                    } else {
                      this.dataExaminationIsActive = true;
                    }
                    this.dataExamination = true;
                    this.dataReviewing = datapatient;
                    this.dataReviewingAge = this.getFullBirthDate(this.dataReviewing[0].tgl_lahir);
                  }
                });
                break;

              case "1":
                this.antrianService.loadPatientExamination1(this.no_doctor).subscribe(({ data }) => {
                  let datapatient = [data.examineLast1];
                  if (datapatient[0] == null) {
                    this.doneExamination = false;
                    this.dataReviewingDone = [
                      {
                        no_antrian: '', nama_pasien: '',
                        no_medrec: '',
                        jaminan: '',
                        umur: '',
                        status_pasien: '',
                        kelamin: '',
                        kaji: ''
                      }];
                      this.dataReviewingDoneAge = '';
                  } else {
                    this.doneExamination = true;
                    this.dataReviewingDone = datapatient;
                    this.dataReviewingDoneAge = this.getFullBirthDate(this.dataReviewingDone[0].tgl_lahir);
                  }
                });
                break;
            }
          }
        });
      });

    }
  }

  getFullBirthDate(birthDate:any): string{
    let now = new Date();
    
    let year = birthDate.slice(0, 4);
    let month = birthDate.slice(5, 7);
    let date = birthDate.slice(8,10);

    let leap_year = 0;
    for(let i=year;i<now.getFullYear();i++){
        if(i%4 == 0){
            leap_year++;
        }
    }

    let past = new Date(year,month-1,date);
    let diff = Math.floor(now.getTime() - past.getTime());
    let day = (1000 * 60 * 60 * 24);

    let days = Math.floor(diff/day);        
    let age_day =  Math.round(days%30.4167)-leap_year;
    let age_month =  Math.floor(days/30.4167)%12;
    let age_year =  Math.floor(Math.floor(days/30.4167)/12);

    if(age_year > 17){
        return age_year + " Tahun";
    }
    else{
        return age_year + " Tahun " + age_month + " Bulan " + age_day + " Hari ";
    }
}

  examineDoneChange(event) {
    this.examinedone = event;
    if (event == true) {
      this.loadPatientExamination("1");
    }
    else if (event == false) {
      this.loadPatientExamination("0");
    }
  }

  changePoli() {
    this.navCtrl.setRoot('DoctorPoliPage');
  }

  refreshAntrian() {
    //this.loadData();
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  ionViewCanEnter() {
    return this.storage.get('token');
  }

  ionViewDidEnter() {
    if (this.navParams.get('paramStatus')) {
    }
  }

  loadingMessage(message: string, duration: number) {

    this.loadingCtrl.create({
        content: message,
        duration: duration,
        dismissOnPageChange: true
    }).present();
  }

}
