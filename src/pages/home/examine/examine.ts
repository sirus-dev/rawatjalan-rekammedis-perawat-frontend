import { IonicPage } from 'ionic-angular';
import { AntrianService } from '../../../providers/antrian-service';
import { Storage } from '@ionic/storage';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
    AlertController,
    LoadingController,
    ModalController,
    NavController,
    NavParams,
    ToastController,
} from 'ionic-angular';

@Component({
    selector: 'examine',
    templateUrl: 'examine.html'
})

export class examineComponent implements OnInit {

    public examinedone: Boolean = false;
    public segment_examine: String = "SEDANG";
    /**
     * 
     * 
     * @type {Array<any>}
     * @memberof examineComponent
     */
    @Input('dataPatient') dataPatient: Array<any> = [];
    /**
     * 
     * 
     * @type {Array<any>}
     * @memberof examineComponent
     */
    @Input('dataReviewing') dataReviewing: Array<any> = [];
    /**
     * 
     * 
     * @type {string}
     * @memberof examineComponent
     */
    @Input('dataReviewingAge') dataReviewingAge: string;
    /**
     * 
     * 
     * @type {Array<any>}
     * @memberof examineComponent
     */

    @Input('dataReviewingDone') dataReviewingDone: Array<any> = [];

    /**
     * 
     * 
     * @type {string}
     * @memberof examineComponent
     */
    @Input('dataReviewingDoneAge') dataReviewingDoneAge: string;
    /**
     * 
     * 
     * @type {Boolean}
     * @memberof examineComponent
     */
    @Input('dataExamination') dataExamination: Boolean;
    /**
     * 
     * 
     * @type {Boolean}
     * @memberof examineComponent
     */
    @Input('dataExaminationIsActive') dataExaminationIsActive: Boolean;
    /**
     * 
     * 
     * @type {Boolean}
     * @memberof examineComponent
     */
    @Input('doneExamination') doneExamination: Boolean;
    /**
     * 
     * 
     * @type {*}
     * @memberof examineComponent
     */
    @Input('poliname') poliname: any;
    /**
     * 
     * 
     * @type {*}
     * @memberof examineComponent
     */
    @Input('doctorname') doctorname: any;
    /**
     * 
     * 
     * @type {*}
     * @memberof examineComponent
     */
    @Input('no_doctor') no_doctor: any;
    /**
     * 
     * 
     * @type {*}
     * @memberof examineComponent
     */
    @Input('counterCountOfQueue') counterCountOfQueue: any;
    /**
     * 
     * 
     * @type {Array<any>}
     * @memberof examineComponent
     */
    @Input('dataWaitingList') dataWaitingList: Array<any> = [];
    /**
     * 
     * 
     * @type {EventEmitter<boolean>}
     * @memberof examineComponent
     */
    @Output('examineDone') examineDone: EventEmitter<boolean> = new EventEmitter<boolean>();



    constructor(public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        private storage: Storage,
        public modalCtrl: ModalController,
        private antrianService: AntrianService) {

        if (this.dataReviewing == null) {
            this.dataReviewing = [{
                no_antrian: '', nama_pasien: '',
                no_medrec: '',
                jaminan: '',
                umur: '',
                status_pasien: '',
                kelamin: '',
                kaji: ''
            }];
        }
        else {
            //this.dataReviewing[0]["detail_umur"] = this.getFullBirthDate(this.dataReviewing[0].tgl_lahir);
        }
    }

    ngOnInit() { }

    /**
     * 
     * 
     * @memberof examineComponent
     */
    setCallQueue() {
        this.loadingMessage("Please Wait.....", 2000);
        if (this.dataPatient.length > 0) {
            if (this.dataWaitingList.length > 0) {

                this.antrianService.getDataPatientById(this.dataWaitingList[0].no_medrec)
                    .subscribe(({ data }) => {
                        var strbyId = JSON.stringify(data);
                        var parsbyId = JSON.parse(strbyId);
                        let datapatientById = parsbyId.examineById;
                        let visit = "";
                        if (datapatientById) {
                            visit = "Lama";
                        } else {
                            visit = "Baru";
                        }
                        this.antrianService.getDoctorById(this.dataWaitingList[0].nodok).subscribe(({ data }) => {
                            var str = JSON.stringify(data);
                            var pars = JSON.parse(str);
                            let datadoctor = pars.doctorByNodok
                            this.antrianService.addDataPatient(this.dataWaitingList[0], visit, datadoctor.specialis, this.dataWaitingList[0].nodok)
                                .subscribe(({ data }) => {

                                    if (data) {
                                        this.antrianService.setFlagPatient(this.dataWaitingList[0].no_medrec, 2, this.poliname, this.doctorname).subscribe(({ data }) => {

                                            if (this.dataPatient[0].kaji == "Belum Dikaji") {


                                            } else {

                                                this.antrianService.setFlagPatient(this.dataPatient[0].no_medrec, 1, this.poliname, this.doctorname).subscribe(({ data }) => {
                                                    var str = JSON.stringify(data);
                                                    var pars = JSON.parse(str);
                                                    let status = pars.setQueueFlag;

                                                    this.dataWaitingList.push(status);
                                                    this.dataPatient.splice(0, 1);
                                                });

                                            }

                                            setTimeout(() => {
                                                this.dataWaitingList.sort((a, b) => {
                                                    return Number(a.no_antrian) - Number(b.no_antrian);
                                                });
                                                this.counterCountOfQueue = this.dataPatient.length;
                                            }, 1000);
                                        });
                                    }

                                });
                        });

                    });

            } else {
                this.alertMessage('Belum memanggil pasien, Silahkan panggil pasien');
            }
        } else {
            if (this.dataWaitingList.length > 0) {
                this.antrianService.setFlagPatient(this.dataWaitingList[0].no_medrec, 2, this.poliname, this.doctorname).subscribe(({ data }) => {

                    this.antrianService.getDataPatientById(this.dataWaitingList[0].no_medrec)
                        .subscribe(({ data }) => {

                            var strbyId2 = JSON.stringify(data);
                            var parsbyId2 = JSON.parse(strbyId2);
                            let datapatientById2 = parsbyId2.examineById;
                            let visit = "";
                            if (datapatientById2) {
                                visit = "Lama";
                            } else {
                                visit = "Baru";
                            }
                            this.antrianService.getDoctorById(this.dataWaitingList[0].nodok).subscribe(({ data }) => {
                                var str = JSON.stringify(data);
                                var pars = JSON.parse(str);
                                let datadoctor = pars.doctorByNodok
                                this.antrianService.addDataPatient(this.dataWaitingList[0], visit, datadoctor.specialis, this.no_doctor)
                                    .subscribe(({ data }) => {

                                    });
                            });

                        });
                });
            } else {
                this.alertMessageAction('Antrian panggil sudah habis');
            }
        }


    }

    /**
     * 
     * 
     * @memberof examineComponent
     */
    setFlagExamine() {
        this.loadingMessage('Please wait...', 2000);
        if (this.dataReviewingDone[0].no_medrec != "") {
            this.antrianService.setFlagPatient(this.dataReviewingDone[0].no_medrec, 5, this.poliname, this.doctorname).subscribe(({ data }) => {

                this.antrianService.setFlagExamination(this.dataReviewingDone[0].no_medrec, 2, this.no_doctor).subscribe(({ data }) => {
                    if (data) {
                        this.dataReviewingDone = [
                            {
                                no_antrian: '', nama_pasien: '',
                                no_medrec: '',
                                jaminan: '',
                                umur: '',
                                status_pasien: '',
                                kelamin: '',
                                kaji: ''
                            }];
                    }
                });
            });
        } else {

        }
    }

    /**
     * 
     * 
     * @param {*} page 
     * @memberof examineComponent
     */
    openControlMessage(page: any) {
        if (page == 'ControlLetterPage') {
            let modal = this.modalCtrl.create('ControlLetterPage', { personal: this.dataReviewingDone[0] });

            modal.present();
        }

    }

    /**
     * 
     * 
     * @param {any} status 
     * @memberof examineComponent
     */
    examineIsDone(status) {
        switch (status) {
            case 'SELESAI':
                this.examineDone.emit(true);
                this.examinedone = true;
                break;

            case 'SEDANG':
                this.examineDone.emit(false);
                this.examinedone = false;
                break;
        }
    }

    loadingMessage(message: string, duration: number) {

        this.loadingCtrl.create({
            content: message,
            duration: duration,
            dismissOnPageChange: true
        }).present();

    }

    toastMessage(message: string, duration: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    }

    alertMessage(message: string) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    alertMessageAction(message: string) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: [
                {
                    text: 'OKE',
                    role: 'OKE',
                    handler: () => {
                        this.navCtrl.setRoot(this.navCtrl.getActive().component);
                    }
                }
            ]
        });
        alert.present();
    }

    presentToast() {
        this.toastMessage('Anda dapat melihat status pasien yang sedang diperiksa oleh Dokter, klik SELESAI PERIKSA untuk antrian masuk selanjutnya', 10000);
    }

}