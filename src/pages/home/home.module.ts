import { FilterPipe } from './../../utils/filterpipe';
import { HomePage } from './home';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { queueComponent } from '../home/queue/queue';
import { waitingListComponent } from '../home/waiting-list/waiting-list'
import { examineComponent } from '../home/examine/examine'

@NgModule({
  declarations: [
    HomePage,
    FilterPipe,
    queueComponent,
    waitingListComponent,
    examineComponent
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomeModule {}
