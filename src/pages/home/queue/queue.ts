import { IonicPage } from 'ionic-angular';
import { AntrianService } from '../../../providers/antrian-service';
import { Storage } from '@ionic/storage';
import { Component, OnInit, Input, Output } from '@angular/core';
import {
    AlertController,
    LoadingController,
    ModalController,
    NavController,
    NavParams,
    ToastController,
} from 'ionic-angular';



@Component({
    selector: 'queue',
    templateUrl: 'queue.html'
})

export class queueComponent implements OnInit {

    // public review = 'AdultReview';
    // public review = 'MidwifeReview';
    public review: any;
    public segment_queue = 'available';
    public counterCountOfQueuePending: number;
    public edit_reorder: boolean = false;
    public editButton: string = 'Atur Urutan';
    public editing: boolean = false;

    /**
         * dataPatient
         * 
         * @type {Array<any>}
         * @memberof queueComponent
         */
    @Input('dataPatient') dataPatient: Array<any> = [];

    /**
      * dataPendingPatient
      * 
      * @type {Array<any>}
      * @memberof queueComponent
      */
    @Input('dataPendingPatient') dataPendingPatient: Array<any> = [];

    /**
      * counterCountOfQueue
      * 
      * @type {any}
      * @memberof queueComponent
      */
    @Input('counterCountOfQueue') counterCountOfQueue: any;

    /**
    * poliname
    * 
    * @type {any}
    * @memberof queueComponent
    */
    @Input('poliname') poliname: any;

    /**
    * doctorname
    * 
    * @type {any}
    * @memberof queueComponent
    */
    @Input('doctorname') doctorname: any;
    /**
     * 
     * 
     * @type {*}
     * @memberof queueComponent
     */
    @Input('no_doctor') no_doctor: any;

    /**
     * 
     * 
     * @type {*}
     * @memberof queueComponent
     */
    @Input('nik_nurse') nik_nurse: any;

    constructor(public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        private antrianService: AntrianService) {

    }

    ngOnInit() { }

    /**
     * 
     * 
     * @param {*} no_medrec 
     * @param {number} key 
     * @memberof queueComponent
     */
    goToPengkajian(no_medrec: any, key: number) {
        //console.log(this.poliname);
        if (this.poliname == "KEBIDANAN") {
            this.review = 'MidwifeReview';
        }
        else {
            this.review = 'AdultReview';
        }

        this.antrianService.getPatientById(no_medrec)
            .subscribe(({ data }) => {
                let statusreview = data.queueById.kaji;
                let statusflag = data.queueById.flag;
                if (statusreview == "Belum Dikaji" && statusflag !== 1) {
                    this.navCtrl.setRoot(this.review, { data: data.queueById, poli: this.poliname, dokter: this.doctorname, nik: this.nik_nurse });
                } else {
                    this.alertMessageReview('Anda sudah mengkaji pasien ini. Apakah anda ingin memperbaruinya ? ', data.queueById);
                }
            });

    }

    /**
     * 
     * 
     * @param {*} status 
     * @memberof queueComponent
     */
    showPatient(status: any) {
        this.segment_queue = status;
        this.counterCountOfQueuePending = this.dataPendingPatient.length;
    }

    /**
     * 
     * 
     * @param {*} index 
     * @param {string} status_pasien 
     * @memberof queueComponent
     */
    changeStatusPatient(index: any, status_pasien: string) {
        this.loadingMessage('Mohon Tunggu...', 2000);
        let no_medrec = this.dataPatient[index].no_medrec;
        this.antrianService.setStatusPatient(no_medrec, status_pasien, 3, this.poliname, this.doctorname).subscribe(({ data }) => {

            var str = JSON.stringify(data);
            var pars = JSON.parse(str);
            let status = pars.setQueueStatus;

            if (status.status_pasien == "lab" || status.status_pasien == "radiologi" || status.status_pasien == "pending" || status.status_pasien == "cancel") {
                this.dataPatient.splice(index, 1);
                this.dataPendingPatient.push(status);
                setTimeout(() => {
                    this.counterCountOfQueuePending = this.dataPendingPatient.length;
                    this.dataPendingPatient.sort((a, b) => {
                        return Number(a.no_antrian) - Number(b.no_antrian);
                    });
                }, 1000);
            }

        }, (error) => {

        });

    }

    /**
     * 
     * 
     * @param {*} index 
     * @param {number} no_antrian 
     * @param {string} status_pasien 
     * @memberof queueComponent
     */
    changeStatusPatientPending(index: any, no_antrian: number, status_pasien: string) {
        this.loadingMessage('Mohon Tunggu...', 2000);
        let no_medrec = this.dataPendingPatient[index].no_medrec;
        let numberFlag = 0;
        if (status_pasien == "lab" || status_pasien == "radiologi" || status_pasien == "pending" || status_pasien == "cancel") {
            numberFlag = 3;
        }

        this.antrianService.setStatusPatient(no_medrec, status_pasien, numberFlag, this.poliname, this.doctorname).subscribe(({ data }) => {
            var str = JSON.stringify(data);
            var pars = JSON.parse(str);
            let status = pars.setQueueStatus;

            let id = no_antrian - 1;


            if (status.status_pasien == "lab" || status.status_pasien == "radiologi" || status.status_pasien == "pending" || status.status_pasien == "cancel") {
                this.dataPendingPatient[index] = status;
            } else {
                this.dataPendingPatient.splice(index, 1);
                this.dataPatient.splice(id, 0, status);
                setTimeout(() => {
                    this.counterCountOfQueuePending = this.dataPendingPatient.length;
                    this.dataPatient.sort((a, b) => {
                        return Number(a.no_antrian) - Number(b.no_antrian);
                    });
                }, 1000);
            }

        }, (error) => {

        });
    }

    reorder() {
        this.editing = !this.editing;
        if (this.editing) {
            this.editButton = 'Selesai';
        } else {
            this.editButton = 'Atur Urutan';
        }
    }

    reorderItems(indexes) {
        let element = this.dataPatient[indexes.from];
        this.dataPatient.splice(indexes.from, 1);
        this.dataPatient.splice(indexes.to, 0, element);
    }

    alertMessageReview(message: string, data: any) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: [
                {
                    text: 'OKE',
                    role: 'OKE',
                    handler: () => {
                        this.navCtrl.setRoot(this.review, { data: data, poli: this.poliname, dokter: this.doctorname, nik: this.nik_nurse });
                    }
                },
                {
                    text: 'CANCEL',
                    role: 'CANCEL',
                }
            ]
        });
        alert.present();
    }


    presentToast() {
        this.toastMessage('Anda dapat melihat antrian, klik pasien untuk pengkajian, geser ke kiri untuk status pasien, dan mencari pasien', 8000);
    }


    loadingMessage(message: string, duration: number) {

        this.loadingCtrl.create({
            content: message,
            duration: duration,
            dismissOnPageChange: true
        }).present();

    }

    toastMessage(message: string, duration: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    }
}