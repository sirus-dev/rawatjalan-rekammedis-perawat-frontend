import { IonicPage } from 'ionic-angular';
import { AntrianService } from '../../../providers/antrian-service';
import { Storage } from '@ionic/storage';
import { Component, OnInit, Input, Output } from '@angular/core';
import {
    AlertController,
    LoadingController,
    ModalController,
    NavController,
    NavParams,
    ToastController,
} from 'ionic-angular';

@Component({
    selector: 'waiting-list',
    templateUrl: 'waiting-list.html'
})

export class waitingListComponent implements OnInit {

    public countOfCall: string = "3";

    /**
     * dataWaitingList
     * 
     * @type {Array<any>}
     * @memberof waitingListComponent
     */
    @Input('dataWaitingList') dataWaitingList: Array<any> = [];

    /**
     * 
     * 
     * @type {Array<any>}
     * @memberof waitingListComponent
     */
    @Input('dataPatient') dataPatient: Array<any> = [];

    /**
     * 
     * 
     * @type {*}
     * @memberof waitingListComponent
     */
    @Input('poliname') poliname: any;

    /**
     * 
     * 
     * @type {*}
     * @memberof waitingListComponent
     */
    @Input('doctorname') doctorname: any;

    /**
     * 
     * 
     * @type {*}
     * @memberof waitingListComponent
     */
    @Input('counterCountOfQueue') counterCountOfQueue: any;

    /**
     * 
     * 
     * @type {boolean}
     * @memberof waitingListComponent
     */
    @Input('showQueue') showQueue: boolean;



    constructor(public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        private antrianService: AntrianService) {
    }

    ngOnInit() {}

    /**
     * 
     * 
     * @param {*} n 
     * @memberof waitingListComponent
     */
    doCallPatient(n: any) {

        if (n == null) {
            this.alertMessage('Set Panggil Terlebih Dahulu');
        }
        //switching number for calling patient
        switch (n) {
            //switch if number is 3
            case "3":
                //check if data patient available
                if (this.dataPatient.length > 0 && this.dataPatient.length >= 3) {
                    if (this.dataWaitingList.length == 0) { //data waiting is 0
                        //check 3 of data patient if the review has reviewed
                        if (this.dataPatient[0].kaji == "Sudah Dikaji" && this.dataPatient[1].kaji == "Sudah Dikaji" && this.dataPatient[2].kaji == "Sudah Dikaji") {

                            this.loadingMessage('Please wait...', 2000);
                            for (var i = 0; i < 3; i++) {
                                this.setCallPatient(i);
                            }
                            setTimeout(() => {
                                this.dataWaitingList.sort((a, b) => {
                                    return Number(a.no_antrian) - Number(b.no_antrian);
                                });
                            }, 1000);

                        } else if (this.dataPatient[0].kaji == "Sudah Dikaji" && this.dataPatient[1].kaji == "Sudah Dikaji") { //check 2 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            for (var j = 0; j < 2; j++) {
                                this.setCallPatient(j);
                            }
                            setTimeout(() => {
                                this.dataWaitingList.sort((a, b) => {
                                    return Number(a.no_antrian) - Number(b.no_antrian);
                                });
                                this.alertMessage('Hanya memanggil 2 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else if (this.dataPatient[1].kaji == "Sudah Dikaji" && this.dataPatient[2].kaji == "Sudah Dikaji") { //check 2 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            for (var k = 1; k < 2; k++) {
                                this.setCallPatient(k);
                            }
                            setTimeout(() => {
                                this.dataWaitingList.sort((a, b) => {
                                    return Number(a.no_antrian) - Number(b.no_antrian);
                                });
                                this.alertMessage('Hanya memanggil 2 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else if (this.dataPatient[0].kaji == "Sudah Dikaji" && this.dataPatient[2].kaji == "Sudah Dikaji") { //check 2 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);
                            this.setCallPatient(2);
                            setTimeout(() => {
                                this.dataWaitingList.sort((a, b) => {
                                    return Number(a.no_antrian) - Number(b.no_antrian);
                                });
                                this.alertMessage('Hanya memanggil 2 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                            setTimeout(() => {
                                this.alertMessage('Hanya memanggil 1 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else if (this.dataPatient[1].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(1);

                            setTimeout(() => {
                                this.alertMessage('Hanya memanggil 1 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else if (this.dataPatient[2].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(2);

                            setTimeout(() => {
                                this.alertMessage('Hanya memanggil 1 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }

                    } else if (this.dataWaitingList.length == 1) {  //data waiting is 1

                        if (this.dataPatient[0].kaji == "Sudah Dikaji" && this.dataPatient[1].kaji == "Sudah Dikaji") { //check 2 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            for (var l = 0; l < 2; l++) {
                                this.setCallPatient(l);
                            }

                        } else if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }

                    } else if (this.dataWaitingList.length == 2) { //data waiting is 2

                        if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }

                    } else if (this.dataWaitingList.length == 3) { //data waiting is 3

                        this.alertMessage('Anda sudah memanggil 3 orang');

                    } else { //if data waiting more than 3

                    }


                } else if (this.dataPatient.length > 0 && this.dataPatient.length == 2) {
                    if (this.dataWaitingList.length == 0) { //data waiting is 0
                        //check 3 of data patient if the review has reviewed
                        if (this.dataPatient[0].kaji == "Sudah Dikaji" && this.dataPatient[1].kaji == "Sudah Dikaji") { //check 2 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            for (var m = 0; m < 2; m++) {
                                this.setCallPatient(m);
                            }
                            setTimeout(() => {
                                this.dataWaitingList.sort((a, b) => {
                                    return Number(a.no_antrian) - Number(b.no_antrian);
                                });
                                this.alertMessage('Hanya memanggil 2 Pasien. Antrian Pasien sudah habis');
                            }, 1000);

                        } else if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                            setTimeout(() => {
                                this.alertMessage('Hanya memanggil 1 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else if (this.dataPatient[1].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(1);

                            setTimeout(() => {
                                this.alertMessage('Hanya memanggil 1 Pasien. Mohon untuk kaji terlebih dahulu Pasien Selanjutnya');
                            }, 1000);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }

                    } else if (this.dataWaitingList.length == 1) {  //data waiting is 1

                        if (this.dataPatient[0].kaji == "Sudah Dikaji" && this.dataPatient[1].kaji == "Sudah Dikaji") { //check 2 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            for (var o = 0; o < 2; o++) {
                                this.setCallPatient(o);
                            }

                        } else if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }

                    } else if (this.dataWaitingList.length == 2) { //data waiting is 2

                        if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }

                    } else if (this.dataWaitingList.length == 3) { //data waiting is 3

                        this.alertMessage('Anda sudah memanggil 3 orang');

                    } else { //if data waiting more than 3

                    }

                } else if (this.dataPatient.length > 0 && this.dataPatient.length == 1) {
                    if (this.dataPatient[0].kaji == "Sudah Dikaji") { //check 1 of data patient if the review has reviewed

                        this.loadingMessage('Please wait...', 2000);
                        this.setCallPatient(0);

                        setTimeout(() => {
                            this.alertMessage('Hanya memanggil 1 Pasien. Antrian Pasien sudah habis');
                        }, 1000);

                    } else {

                        this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                    }
                } else { //if data patient is none

                    this.alertMessage('Antrian Pasien sudah habis');

                }

                break;

            case "1":

                if (this.dataPatient.length > 0) {
                    if (this.dataWaitingList.length == 3) {

                        this.alertMessage('Anda sudah memanggil 3 orang');

                    } else {
                        if (this.dataPatient[0].kaji == "Sudah Dikaji") {

                            this.loadingMessage('Please wait...', 2000);
                            this.setCallPatient(0);

                        } else {

                            this.alertMessage('Mohon untuk kaji terlebih dahulu Pasien sebelum dipanggil');

                        }
                    }
                } else {

                    this.alertMessage('Antrian Pasien sudah habis');

                }

                break;

        }

    }

    /**
     * 
     * 
     * @param {*} i 
     * @memberof waitingListComponent
     */
    async setCallPatient(i: any) {
        await setTimeout(() => {
            this.showQueue = true;
            this.antrianService.setFlagPatient(this.dataPatient[i].no_medrec, 1, this.poliname, this.doctorname).subscribe(({ data }) => {
                var str = JSON.stringify(data);
                var pars = JSON.parse(str);
                let status = pars.setQueueFlag;

                this.dataWaitingList.push(status);
                this.dataPatient.splice(0, 1);
                setTimeout(() => {
                    this.counterCountOfQueue = this.dataPatient.length;
                }, 1000);
            });
        }, 2000);
    }
    
    /**
     * 
     * 
     * @param {*} index 
     * @param {string} status_pasien 
     * @memberof waitingListComponent
     */
    changeStatusPatientCall(index: any, status_pasien: string) {
        let no_medrec = this.dataWaitingList[index].no_medrec;
        this.antrianService.setStatusPatient(no_medrec, status_pasien, 3, this.poliname, this.doctorname).subscribe(({ data }) => {

            var str = JSON.stringify(data);
            var pars = JSON.parse(str);
            let status = pars.setQueueStatus;

            if (status.status_pasien == "lab" || status.status_pasien == "radiologi" || status.status_pasien == "pending" || status.status_pasien == "cancel") {

                if (this.dataPatient.length > 0 && this.dataPatient[0].kaji == "Sudah Dikaji") {
                    this.antrianService.setFlagPatient(this.dataPatient[0].no_medrec, 1, this.poliname, this.doctorname).subscribe(({ data }) => {
                        var str = JSON.stringify(data);
                        var pars = JSON.parse(str);
                        let status = pars.setQueueFlag;

                        this.dataWaitingList.push(status);
                        this.dataPatient.splice(0, 1);
                    });
                }
            }

        }, (error) => {

        });

    }

    alertMessage(message: string) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    loadingMessage(message: string, duration: number) {

        this.loadingCtrl.create({
            content: message,
            duration: duration,
            dismissOnPageChange: true
        }).present();

    }

    toastMessage(message: string, duration: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    }

    presentToast(value: any) {
        switch (value) {
            case 2:
                return this.toastMessage('Anda dapat mengatur jumlah panggilan lalu klik PANGGIL', 6000);
            case 3:
                return this.toastMessage('Anda dapat melihat antrian yang akan dipanggil berdasarkan jumlah panggilan pasien', 6000);

        }
    }

}