import { Printer } from '@ionic-native/printer';
//service
import { AntrianService } from '../providers/antrian-service';
import { AuthService } from '../providers/auth-service';

//module
import { BrowserModule } from '@angular/platform-browser';
import { IonicStorageModule } from '@ionic/storage';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

//component
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//apollo-client
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import { ApolloModule } from 'apollo-angular';
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';

//Create Apollo Network to Server
const networkInterface = createNetworkInterface(
  { uri: '/api/graphql', 
    opts: { mode: 'cors'} });

// set ws protocol when using http and wss when using https
const protocol = window.location.protocol.replace('http', 'ws');
// get location host
const host = window.location.host;  
//Create WebSocket client
const wsClient = new SubscriptionClient(`ws://104.248.151.31:14113/subscriptions`, {
  reconnect: true
});

//network handler
networkInterface.useAfter([{
  applyAfterware({ response }, next) {
    if (response.status === 401) {
      console.log("Network Error");
    }
    next();
  }
}]);

// Extend the network interface with the WebSocket
const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
  networkInterface,
  wsClient
);

//create Apollo Client
const client = new ApolloClient({
  networkInterface: networkInterfaceWithSubscriptions
});


export function provideClient(): ApolloClient {
  return client;
}

@NgModule({
  declarations: [
    MyApp
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu' ]
    }),
    IonicStorageModule.forRoot(),
    ApolloModule.forRoot(provideClient),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    AntrianService,
    Printer,
    
  ]
})
export class AppModule {}
