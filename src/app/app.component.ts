import { Storage } from '@ionic/storage';
import { AuthService } from './../providers/auth-service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { AlertController, MenuController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, icon: string }>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private auth: AuthService,
    public storage: Storage,
    public alertCtrl: AlertController) {
    
    this.initializeApp();
    this.menuCtrl.open();

    this.pages = [
      { title: 'Home', icon: 'home' },
      { title: 'Settings', icon: 'settings' },
      { title: 'Logout', icon: 'log-out' }
    ];

    statusBar.overlaysWebView(false);
    statusBar.backgroundColorByHexString('#F5F5F5');
    splashScreen.hide();

  }

  ngOnInit() {
    this.storage.get('token')
    .then((token)=>{
      if (token) {
        return this.storage.get('role');
      }
      return false;
    })
    .then((token)=>{
      if (token) {
        return this.storage.get('nik');
      }
      return false;
    })
    .then((role)=>{
      if (role) {
        return this.storage.get('poli');
      }
      return false;
    })
    .then((poli)=>{
      if (poli) {
       return this.storage.get('nama_dokter'); 
      }
      return false;
    })
    .then((nama_dokter)=>{
      this.splashScreen.hide();
      this.menuCtrl.enable(nama_dokter);
      if (nama_dokter) {
        this.nav.setRoot('HomePage');
      } else {
        this.nav.setRoot('AuthenticationPage');
      }
    })
    .catch((err)=>{
      console.log(err);
    });
  }

  initializeApp() {
    // this.platform.ready().then(() => {
    //   this.statusBar.styleDefault();
    //   this.splashScreen.hide();
    // });
  }

  openPage(page) {
    if (page == "Logout") {
      this.logout();
    } else {
      this.nav.setRoot(page.component);
    }
  }

  logout() {
    let alert = this.alertCtrl.create({
      subTitle: "Apakah anda yakin ingin keluar?",
      buttons: [
        {
          text: 'YA',
          role: 'YA',
          handler: () => {
            this.auth.logout().subscribe(out => {
              this.menuCtrl.enable(false);
              this.nav.setRoot('AuthenticationPage');
            });
          }
        },
        {
          text: 'TIDAK',
          role: 'TIDAK',
        }
      ]
    });
    alert.present();
  }
}
